# -*- coding: utf-8 -*-
##############################################################################
#                 @author Esousy
#
##############################################################################

{
    'name': 'VDE CFDI 3.3',
    'version': '10.0',
    'description': ''' Factura Electronica módulo de ventas para Mexico (CFDI 2017)
    ''',
    'category': 'Sales, Accounting',
    'author': 'VDE Suite',
    'website': '',
    'depends': [
        'base',
        'sale',
        'account'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/res_partner_view.xml',
        'views/res_company_view.xml',
        'views/product_view.xml',
        'views/account_invoice_view.xml',
        'views/account_payment_view.xml',
        'views/account_tax_view.xml',
        'views/sale_view.xml',
        'wizard/bulk_invoice_payment.xml',
        'wizard/bulk_invoice_payment_intermoneda.xml',
        'views/res_partner_bank.xml',
        'report/invoice_report.xml',
        'views/widget.xml',
        #'report/payment_report.xml',
        #'report/sale_report_templates.xml',
        #'data/mail_template_data.xml',
    ],
    'qweb': [
    'static/src/xml/*.xml',
    ],
    'application': False,
    'installable': True,
    'price': 0.00,
    'currency': 'USD',
    'license': 'OPL-1',	
}