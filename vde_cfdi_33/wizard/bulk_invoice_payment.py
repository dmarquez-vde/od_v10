# -*- coding: utf-8 -*-
##############################################################################
#
#    @ Devintelle COunstlting Services Pvt Ltd.
#    Copyright (C) 2015 Devintelle Software Solutions (<http://devintellecs.com>).
#
##############################################################################

from openerp import api, fields, models, _
from openerp.exceptions import ValidationError
import datetime
from openerp.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
import itertools
from operator import itemgetter
import operator
import openerp
import base64
from os.path import join
import os

class bulk_invoice(models.TransientModel):
    _name = 'bulk.invoice'

    @api.onchange('full_payment')
    def onchange_full_payment(self):
        if self.full_payment:
            self.to_paid_amount = self.amount
        else:
            self.to_paid_amount = 0.0

    @api.multi
    @api.depends('to_paid_amount')
    def _get_paid_amount(self):
        for line in self:
            if line.currency_id.id == line.invoice_currency_id.id:
                line.paid_amount = line.to_paid_amount
            else:
                line.paid_amount = line.currency_id.with_context(date=line.bulk_invoice_id.payment_date).compute(line.to_paid_amount,self.env.user.partner_id.company_id.currency_id)

    invoice_id = fields.Many2one('account.invoice',string='Invoice')
    partner_id = fields.Many2one('res.partner',string='Partner')
    amount = fields.Float('Amount')
    paid_amount = fields.Float('Pay Amount',compute='_get_paid_amount')
    to_paid_amount = fields.Float('Pay Amount')
    bulk_invoice_id = fields.Many2one('bulk.inv.payment')
    communication = fields.Char(string="Referencia")
    currency_id = fields.Many2one('res.currency')
    invoice_currency_id = fields.Many2one('res.currency', related="invoice_id.currency_id")
    full_payment = fields.Boolean(string="Pago completo?", default=True)

    @api.onchange('to_paid_amount')
    def onchange_to_paid_amount(self):
        if self.to_paid_amount:
            if self.currency_id.id == self.invoice_currency_id.id:
                self.paid_amount = self.to_paid_amount
            else:
                self.paid_amount = self.currency_id.with_context(date=self.bulk_invoice_id.payment_date).compute(self.to_paid_amount,self.env.user.partner_id.company_id.currency_id)
            #self.company_id.currency_id.with_context(date=self.env.context.get('fecha_pago')).compute(dif,currency),
            #self.paid_amount = self.currency_id.with_context(date=self.bulk_invoice_id.payment_date).compute(self.to_paid_amount,self.env.user.partner_id.company_id.currency_id)

class bulk_inv_payment(models.TransientModel):
    _name = 'bulk.inv.payment'

    @api.model
    def default_get(self,fields):
        res = super(bulk_inv_payment, self).default_get(fields)
        inv_ids = self._context.get('active_ids')
        vals=[]
        invoice_ids = self.env['account.invoice'].browse(inv_ids)
        monedas = []
        partners = []
        for invoice in invoice_ids:
            partners.append(invoice.partner_id.id)
        last_partner=partners[0]
        for partner in partners:
            if partner == last_partner:
                last_partner = partner
                continue
            else:
                raise ValidationError('Todas las facturas deben pertener al mismo cliente.')
        for invoice in invoice_ids:
            monedas.append(invoice.currency_id.id)
        last_moneda=monedas[0]
        for moneda in monedas:
            if moneda == last_moneda:
                last_moneda = moneda
                continue
            else:
                raise ValidationError('Todas las facturas deben tener la misma moneda.')
        moneda_rec = self.env['res.currency'].browse(last_moneda)
        if moneda_rec.name == 'MXN':
            last_moneda = False
        inv_type = ''
        for invo in invoice_ids:
            inv_type = invo.type
            break
        for inv in invoice_ids:
            if inv_type != inv.type:
                raise ValidationError('You must select only invoices or refunds.')
            if inv.state != 'open':
                raise ValidationError('Please Select Open Invoices.')
            vals.append((0,0,{
                'invoice_id':inv and inv.id or False,
                'partner_id':inv and inv.partner_id.id or False,
                'amount':inv.residual or 0.0,
                #'paid_amount':inv.residual or 0.0,
                'paid_amount': inv.residual or 0.0,
                'to_paid_amount': inv.residual or 0.0,
                'full_payment': True,
                'invoice_currency_id': inv.currency_id.id or False
            }))
            if inv.type in ('out_invoice','out_refund'):
                res.update({
                    'partner_type':'customer',
                })
            else:
                res.update({
                    'partner_type':'supplier',
                })
        if inv_type in ('out_invoice','in_refund'):
            res.update({
                'payment_type':'inbound'
            })
        else:
            res.update({
                'payment_type':'outbound'
            })

        res.update({
        'invoice_ids': vals,
        'partner_id': last_partner,
        'currency_id': last_moneda
        })
        return res


    name = fields.Char('Name',default='hello')
    payment_type = fields.Selection([('outbound','Send Money'),('inbound','Receive Money'),('transfer','Transfer')],string="Payment Type", required="1")
    payment_date = fields.Date('Payment Date', required="1",default=fields.Datetime.now)
    communication = fields.Char('Memo')
    partner_type = fields.Selection([('customer','Customer'),('supplier','Supplier')],string='Partner Type')
    journal_id = fields.Many2one('account.journal', string='Payment Method', required=True, domain=[('type', 'in', ('bank', 'cash'))])
    invoice_ids = fields.One2many('bulk.invoice','bulk_invoice_id',string='Invoice')
    partner_id = fields.Many2one('res.partner')
    intermoneda = fields.Boolean(string="Es intermoneda?", default=lambda self: self.env.context.get('intermoneda', False))
    currency_id = fields.Many2one('res.currency', string="Moneda")



    @api.onchange('communication')
    def onchange_communication(self):
        if self.communication:
            for line in self.invoice_ids:
                line.communication = self.communication

    @api.onchange('journal_id')
    def onchange_journal_id(self):
        if self.journal_id:
            for line in self.invoice_ids:
                line.currency_id = self.journal_id.currency_id.id or self.env.user.partner_id.company_id.currency_id.id


    @api.multi
    def process_payment(self):
        vals=[]
        for line in self.invoice_ids:
            #print "esto es paid_amount ", line.paid_amount
            if line.paid_amount > 0.0:
                #a_payment_line_ids =  self.env['advance.payment.line'].search([('invoice_id','=',line.invoice_id.id),('state','in',['posted','sent','reconciled'])])
                a_payment_line_ids =  self.env['advance.payment.line'].search([('invoice_id','=',line.invoice_id.id),('state','in',['posted','sent','reconciled']),('folio_fiscal','!=',False)])
                #print "esto es a_payment_line_ids ", a_payment_line_ids
                cont = len(a_payment_line_ids)
                #print "esto es cont ", cont
                cont = cont + 1
                #print "esto es cont ", cont
                vals.append({
                    'invoice_id': line.invoice_id or False,
                    'partner_id': line.partner_id and line.partner_id.id or False,
                    'amount': line.amount or 0.0,
                    'paid_amount': line.paid_amount or 0.0,
                    'to_paid_amount': line.to_paid_amount or 0.0,
                    'currency_id': line.invoice_id.currency_id.id or False,
                    'tipocambio': line.invoice_id.tipocambio or 0,
                    'reference': line.communication or False,
                    'currency_id': line.currency_id.id or False,
                    'no_de_pago': cont or 0,
                })
        new_vals=sorted(vals,key=itemgetter('partner_id'))
        groups = itertools.groupby(new_vals, key=operator.itemgetter('partner_id'))
        result = [{'partner_id':k,'values':[x for x in v]} for k, v in groups]
        new_payment_ids=[]
        for res in result:
            payment_method_id= self.env['account.payment.method'].search([('name','=','Manual')],limit=1)
            if not payment_method_id:
                payment_method_id= self.env['account.payment.method'].search([],limit=1)
            pay_val={
                'payment_type':self.payment_type,
                'payment_date':self.payment_date,
                'partner_type':self.partner_type,
                'partner_id':res.get('partner_id'),
                'journal_id':self.journal_id and self.journal_id.id or False,
                'communication':self.communication,
                'payment_method_id':payment_method_id and payment_method_id.id or False,
                'state':'draft',
                'amount':20,
                'currency_id':res.get('values')[0].get('currency_id'),
            }
            payment_id = self.env['account.payment'].create(pay_val)
            line_list=[]
            paid_amt=0
            inv_ids = []
            for inv_line in res.get('values'):
                invoice  = inv_line.get('invoice_id')
                inv_ids.append(invoice.id)
                full_reco=False
                if invoice.residual == inv_line.get('paid_amount'):
                    full_reco = True
                #print "esto es to_paid_amount ", inv_line.get('to_paid_amount')
                line_list.append((0,0,{
                    'invoice_id': invoice.id,
                    'account_id': invoice.account_id and invoice.account_id.id or False,
                    'date': invoice.date_invoice,
                    'due_date': invoice.date_due,
                    'original_amount': invoice.amount_total,
                    'balance_amount': invoice.residual,
                    'allocation': inv_line.get('paid_amount'),
                    'allocation1': inv_line.get('to_paid_amount'),
                    'full_reconclle': full_reco,
                    'account_payment_id': payment_id and payment_id.id or False,
                    'tipocambio': inv_line.get('tipocambio') or 0,
                    'reference': inv_line.get('reference') or False,
                    'currency_id': inv_line.get('currency_id') or False,
                    'no_de_pago': inv_line.get('no_de_pago') or 0
                }))
                paid_amt += inv_line.get('paid_amount')
            payment_id.write({
                'line_ids':line_list,
                'amount':paid_amt,
                'invoice_ids':[(6,0,inv_ids)]
            })
            sequence_code = payment_id.post()
            nuevo_folio = self.env['ir.sequence'].with_context(ir_sequence_date=payment_id.payment_date).next_by_code(sequence_code)
            payment_id.write({'name':nuevo_folio})
            new_payment_ids.append(payment_id)
        return True


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
