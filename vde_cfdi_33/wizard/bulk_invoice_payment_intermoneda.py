# -*- coding: utf-8 -*-
##############################################################################
#
#    @ Devintelle COunstlting Services Pvt Ltd.
#    Copyright (C) 2015 Devintelle Software Solutions (<http://devintellecs.com>).
#
##############################################################################

from openerp import api, fields, models, _
from openerp.exceptions import ValidationError
import datetime
from openerp.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
import itertools
from operator import itemgetter
import operator
import openerp
import base64
from os.path import join
import os
import odoo.addons.decimal_precision as dp

class bulk_invoice_intermoneda(models.TransientModel):
    _name = 'bulk.invoice.intermoneda'


    @api.onchange('full_payment')
    def onchange_full_payment(self):
        print "entra a onchange_full_payment ", self.env.context
        #if self.env.context.get('intermoneda')==False:
        if self.bulk_invoice_id.intermoneda == False:
            if self.full_payment:
                self.to_paid_amount = self.amount
            else:
                self.to_paid_amount = 0.0
        #elif self.env.context.get('intermoneda')==True:
        if self.bulk_invoice_id.intermoneda == True:
            print "intermoneda"
            if self.invoice_currency_id.name != 'MXN':
                print "factura diferente a pesos"
                if self.full_payment:
                    invoice_date_datetime = datetime.datetime.strptime(self.bulk_invoice_id.payment_date, '%Y-%m-%d')
                    rate_obj = self.env['res.currency.rate']
                    rate_rec = rate_obj.search([
                        ('currency_id', '=', self.invoice_currency_id.id),
                        ('name', '<=', str(invoice_date_datetime))
                        ], order='name desc', limit=1)
                    if rate_rec:
                        rate = rate_obj.browse(rate_rec.id).rate
                    else:
                        rate = 1.0
                    print "el rate ", rate
                    #self.to_paid_amount = (self.amount / (1/rate)) * (1/rate)
                    self.to_paid_amount = self.amount / rate
                else:
                    print "entra else 94"
                    self.to_paid_amount = 0.0
                    self.paid_amount = 0.0
            elif self.invoice_currency_id.name == 'MXN':
                print "factura en pesos"
                if self.full_payment:
                    invoice_date_datetime = datetime.datetime.strptime(self.bulk_invoice_id.payment_date, '%Y-%m-%d')
                    rate_obj = self.env['res.currency.rate']
                    rate_rec = rate_obj.search([
                        ('currency_id', '=', self.currency_id.id),
                        ('name', '<=', str(invoice_date_datetime))
                        ], order='name desc', limit=1)
                    if rate_rec:
                        rate = rate_obj.browse(rate_rec.id).rate
                    else:
                        rate = 1.0
                    print "el rate ", rate
                    self.to_paid_amount = self.amount / (1/rate)
                else:
                    print "entra else 94"
                    self.to_paid_amount = 0.0
                    self.paid_amount = 0.0

    #@api.multi
    #@api.depends('to_paid_amount')
    #def _get_paid_amount(self):
        #for line in self:
            #if line.currency_id.id == line.invoice_currency_id.id:
                #line.paid_amount = line.to_paid_amount
            #else:
                #line.paid_amount = line.currency_id.with_context(date=line.bulk_invoice_id.payment_date).compute(line.to_paid_amount,self.env.user.partner_id.company_id.currency_id)

    @api.multi
    @api.depends('to_paid_amount')
    def _get_paid_amount(self):
        for line in self:
            if line.to_paid_amount:
                if line.currency_id.id == line.invoice_currency_id.id:
                    print "mismas monedas"
                    line.paid_amount = line.to_paid_amount
                else:
                    print "diferentes monedas"
                    #### Cualquier moneda de factura / Pago en Pesos
                    if line.currency_id.name == 'MXN':
                        print "a pagar son pesos"
                        #self.paid_amount = self.currency_id.with_context(date=self.bulk_invoice_id.payment_date).compute(self.to_paid_amount,self.env.user.partner_id.company_id.currency_id)
                        invoice_date_datetime = datetime.datetime.strptime(line.bulk_invoice_id.payment_date, '%Y-%m-%d')
                        print"esta es la fehca que optiene",invoice_date_datetime
                        rate_obj = self.env['res.currency.rate']
                        print "ESTP ES RATE_OBJ",rate_obj
                        print "ESTO ES ID DE MONEDA",line.invoice_currency_id.id
                        rate_rec = rate_obj.search([
                            ('currency_id', '=', line.invoice_currency_id.id),
                            ('name', '<=', str(invoice_date_datetime))
                            ], order='name desc', limit=1)
                        print "Esto es rate_rec",rate_rec
                        if rate_rec:
                            rate = rate_obj.browse(rate_rec.id).rate
                        else:
                            rate = 1.0
                        print "esto es rate ", rate
                        #rate = 1 / rate
                        print "esto es rate ", rate
                        #self.to_paid_amount = (self.amount / (1/rate)) * (1/rate)
                        line.paid_amount = (line.to_paid_amount / (1/ rate))
                    #### Factura en pesos / Pago en USD
                    if line.invoice_currency_id.name == 'MXN':
                        print "a cobrar Pesos"
                        invoice_date_datetime = datetime.datetime.strptime(line.bulk_invoice_id.payment_date, '%Y-%m-%d')
                        rate_obj = self.env['res.currency.rate']
                        rate_rec = rate_obj.search([
                            ('currency_id', '=', line.currency_id.id),
                            ('name', '<=', str(invoice_date_datetime))
                            ], order='name desc', limit=1)
                        if rate_rec:
                            rate = rate_obj.browse(rate_rec.id).rate
                        else:
                            rate = 1.0
                        print "esto es rate ", rate
                        #rate = 1 / rate
                        print "esto es rate ", rate
                        #self.to_paid_amount = self.amount / (1/rate)
                        line.paid_amount = (line.to_paid_amount * (1/rate))



                    if line.invoice_currency_id.name == 'USD' and line.currency_id.name == 'EUR' :
                        print "A saldar factura de Dolares con pago en EUROS"
                        invoice_date_datetime = datetime.datetime.strptime(line.bulk_invoice_id.payment_date, '%Y-%m-%d')
                        rate_obj = self.env['res.currency.rate']
                        rate_rec = rate_obj.search([
                            ('currency_id', '=', line.currency_id.id),
                            ('name', '<=', str(invoice_date_datetime))
                            ], order='name desc', limit=1)
                        if rate_rec:
                            rate = rate_obj.browse(rate_rec.id).rate
                        else:
                            rate = 1.0
                        print "esto es rate ", rate
                        #rate = 1 / rate
                        print "esto es rate ", rate

                        invoice_date_datetime = datetime.datetime.strptime(line.bulk_invoice_id.payment_date, '%Y-%m-%d')
                        print "esto es la fecha que busco ",invoice_date_datetime
                        rate_obj = self.env['res.currency.rate']
                        rate_rec = rate_obj.search([
                            ('currency_id', '=', line.invoice_currency_id.id),
                            ('name', '<=', str(invoice_date_datetime))
                            ], order='name desc', limit=1)
                        if rate_rec:
                            rate1 = rate_obj.browse(rate_rec.id).rate
                        else:
                            rate1 = 1.0

                        print "esto es rate1 ", rate1
                        #rate = 1 / rate
                        print "esto es rate1 ", rate1

                        #self.to_paid_amount = self.amount / (1/rate)
                        y=rate1
                        print "esto es y ",y
                        x=line.to_paid_amount
                        print "esto es x ",x
                        z=line.tipocambiop
                        print "esto es z ",z

                        line.paid_amount = (y/(z/x))


                    if line.invoice_currency_id.name == 'EUR' and line.currency_id.name == 'USD' :


                                    print "A saldar factura de EUROS con pago en DOLARES"
                                    invoice_date_datetime = datetime.datetime.strptime(line.bulk_invoice_id.payment_date, '%Y-%m-%d')
                                    print "esto es la fecha que busco ",invoice_date_datetime
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                        ('currency_id', '=', line.currency_id.id),
                                        ('name', '<=', str(invoice_date_datetime))
                                        ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate = 1.0

                                    invoice_date_datetime = datetime.datetime.strptime(line.bulk_invoice_id.payment_date, '%Y-%m-%d')
                                    print "esto es la fecha que busco ",invoice_date_datetime
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                        ('currency_id', '=', line.invoice_currency_id.id),
                                        ('name', '<=', str(invoice_date_datetime))
                                        ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate1 = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate1 = 1.0

                                    print "esto es rate1 ", rate1
                                    #rate = 1 / rate
                                    print "esto es rate1 ", rate1

                                    #self.to_paid_amount = self.amount / (1/rate)
                                    y=rate1
                                    print "esto es y ",y
                                    x=line.to_paid_amount
                                    z=line.tipocambiop
                                    print "esto es z ",z

                                    line.paid_amount = ((y*x)/z)



    invoice_id = fields.Many2one('account.invoice',string='Invoice')
    partner_id = fields.Many2one('res.partner',string='Partner')
    amount = fields.Float('Amount')
    #paid_amount = fields.Float('Pay Amount',compute='_get_paid_amount')
    paid_amount = fields.Float('Pay Amount', compute='_get_paid_amount')
    to_paid_amount = fields.Float('Pay Amount')
    bulk_invoice_id = fields.Many2one('bulk.inv.payment.intermoneda')
    communication = fields.Char(string="Referencia")
    currency_id = fields.Many2one('res.currency')
    invoice_currency_id = fields.Many2one('res.currency', related="invoice_id.currency_id")
    full_payment = fields.Boolean(string="Pago completo?", default=True)
    #tipocambio = fields.Char(string=_('TipoCambio'))
    #tipocambiop = fields.Char(string=_('TipoCambio'))
    tipocambio = fields.Float(string=_('TipoCambio'), related="invoice_id.tipocambio", digits=dp.get_precision('Currency'))
    tipocambiop = fields.Float(string=_('TipoCambio'), digits=dp.get_precision('Currency'))

    #@api.onchange('to_paid_amount')
    #def onchange_to_paid_amount(self):
        #if self.to_paid_amount:
            #if self.currency_id.id == self.invoice_currency_id.id:
                #print "mismas monedas"
                #self.paid_amount = self.to_paid_amount
            #else:
                #print "diferentes monedas"
                #if self.currency_id.name == 'MXN':
                    #print "a pagar son pesos"
                    ##self.paid_amount = self.currency_id.with_context(date=self.bulk_invoice_id.payment_date).compute(self.to_paid_amount,self.env.user.partner_id.company_id.currency_id)
                    #invoice_date_datetime = datetime.datetime.strptime(self.bulk_invoice_id.payment_date, '%Y-%m-%d')
                    #rate_obj = self.env['res.currency.rate']
                    #rate_rec = rate_obj.search([
                        #('currency_id', '=', self.invoice_currency_id.id),
                        #('name', '<=', str(invoice_date_datetime))
                        #], order='name desc', limit=1)
                    #if rate_rec:
                        #rate = rate_obj.browse(rate_rec.id).rate
                    #else:
                        #rate = 1.0
                    #print "esto es rate ", rate
                    #rate = 1 / rate
                    #print "esto es rate ", rate
                    #self.paid_amount = self.to_paid_amount / rate
            #self.company_id.currency_id.with_context(date=self.env.context.get('fecha_pago')).compute(dif,currency),
            #self.paid_amount = self.currency_id.with_context(date=self.bulk_invoice_id.payment_date).compute(self.to_paid_amount,self.env.user.partner_id.company_id.currency_id)

class bulk_inv_payment_intermoneda(models.TransientModel):
    _name = 'bulk.inv.payment.intermoneda'

    @api.model
    def default_get(self,fields):
        #res={}
        print "entra a default_get ", self.env.context
        res = super(bulk_inv_payment_intermoneda, self).default_get(fields)
        inv_ids = self._context.get('active_ids')
        vals=[]
        invoice_ids = self.env['account.invoice'].browse(inv_ids)
        monedas = []
        partners = []
        for invoice in invoice_ids:
            partners.append(invoice.partner_id.id)
        last_partner=partners[0]
        for partner in partners:
            if partner == last_partner:
                last_partner = partner
                continue
            else:
                raise ValidationError('Todas las facturas deben pertener al mismo cliente.')
        #for invoice in invoice_ids:
            #monedas.append(invoice.currency_id.id)
        #if self.env.context.get('intermoneda')==False:
            #moneda_last = monedas[0]
            #for moneda in monedas:
                #if moneda == moneda_last:
                    #moneda_last = moneda
                    #continue
                #else:
                    #raise ValidationError('Todas las facturas deben tener la misma moneda.')
        #if self.env.context.get('intermoneda')==True:
            #moneda_last = monedas[0]
            #for moneda in monedas:
                #if not moneda == moneda_last:
                    #moneda_last = moneda
                    #continue
                #else:
                    #raise ValidationError('Todas las facturas deben tener la diferente moneda.')
        inv_type = ''
        for invo in invoice_ids:
            inv_type = invo.type
            break
        for inv in invoice_ids:
            if inv_type != inv.type:
                raise ValidationError('Seleccione solo facturas o Notas de credito.')
            if inv.state != 'open':
                raise ValidationError('Seleccione facturas abiertas.')
            vals.append((0,0,{
                'invoice_id':inv and inv.id or False,
                'partner_id':inv and inv.partner_id.id or False,
                'amount':inv.residual or 0.0,
                #'paid_amount':inv.residual or 0.0,
                #'paid_amount': 0.0,
                #'to_paid_amount': 0.0,
                #'full_payment': False,
                'invoice_currency_id': inv.currency_id.id or False,
                #'tipocambio': inv.tipocambio or 0.0,
                #'tipocambiop': self.env['bulk.invoice']._get_tc_pago(),
            }))
            if inv.type in ('out_invoice','out_refund'):
                res.update({
                    'partner_type':'customer',
                })
            else:
                res.update({
                    'partner_type':'supplier',
                })
        if inv_type in ('out_invoice','in_refund'):
            res.update({
                'payment_type':'inbound'
            })
        else:
            res.update({
                'payment_type':'outbound'
            })
        print "esto es vals ", vals
        res.update({
        'invoice_ids': vals,
        'partner_id': last_partner
        })
        return res

    @api.onchange('payment_date')
    def onchange_payment_date(self):
        if self.payment_date:
            #today = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
            #invoice_date_datetime = datetime.datetime.strptime(self.payment_date, '%Y-%m-%d %H:%M:%S')
            invoice_date_datetime = datetime.datetime.strptime(self.payment_date, '%Y-%m-%d')
            print "esto es fecha ", invoice_date_datetime
            rate_obj = self.env['res.currency.rate']
            rate_rec = rate_obj.search([
                ('currency_id', '=', self.journal_id.currency_id.id),
                ('name', '<=', str(invoice_date_datetime))
                # not sure for what purpose 'currency_rate_type_id' field exists in the table, but keep this line just in case
                ], order='name desc', limit=1)
            if rate_rec:
                rate = rate_obj.browse(rate_rec.id).rate
            else:
                rate = 1.0
            print "esto es rate en el onchange", rate
            for line in self.invoice_ids:
                line.tipocambiop = rate
                print "dentro del onchange en el for", line.tipocambiop

    #@api.onchange('payment_date')
    #def onchange_payment_date(self):
        #if self.payment_date:
            ##today = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
            ##invoice_date_datetime = datetime.datetime.strptime(self.payment_date, '%Y-%m-%d %H:%M:%S')
            #invoice_date_datetime = datetime.datetime.strptime(self.payment_date, '%Y-%m-%d')
            ##print "esto es fecha ", invoice_date_datetime
            #rate_obj = self.env['res.currency.rate']
            #rate_rec = rate_obj.search([
                #('currency_id', '=', self.journal_id.currency_id.id),
                #('name', '<=', str(invoice_date_datetime))
                ## not sure for what purpose 'currency_rate_type_id' field exists in the table, but keep this line just in case
                #], order='name desc', limit=1)
            #if rate_rec:
                #rate = rate_obj.browse(rate_rec.id).rate
            #else:
                #rate = 1.0
            #for line in self.invoice_ids:
                #line.tipocambiop = rate
                #if line.currency_id.name == 'MXN':
                    #invoice_date_datetime = datetime.datetime.strptime(self.payment_date, '%Y-%m-%d')
                    ##print "esto es fecha ", invoice_date_datetime
                    #rate_obj = self.env['res.currency.rate']
                    #rate_rec = rate_obj.search([
                        #('currency_id', '=', line.invoice_currency_id.id),
                        #('name', '<=', str(invoice_date_datetime))
                        ## not sure for what purpose 'currency_rate_type_id' field exists in the table, but keep this line just in case
                        #], order='name desc', limit=1)
                    #if rate_rec:
                        #rate = rate_obj.browse(rate_rec.id).rate
                    #else:
                        #rate = 1.0
                    #line.to_paid_amount = line.amount * (1/rate)
            ##self.tipocambiop=rate
            ##return rate



    @api.depends('invoice_ids')
    @api.one
    def _get_total_amount(self):
        print "entra a total_amount"
        if self.invoice_ids:
            suma = 0
            for line in self.invoice_ids:
                suma = suma + line.to_paid_amount
            print "Esto es suma ", suma
            self.total_amount = suma



    name = fields.Char('Name',default='hello')
    payment_type = fields.Selection([('outbound','Send Money'),('inbound','Receive Money'),('transfer','Transfer')],string="Payment Type", required="1")
    payment_date = fields.Date('Payment Date', required="1",default=fields.Datetime.now)
    communication = fields.Char('Memo')
    partner_type = fields.Selection([('customer','Customer'),('supplier','Supplier')],string='Partner Type')
    journal_id = fields.Many2one('account.journal', string='Payment Method', required=True, domain=[('type', 'in', ('bank', 'cash'))])
    invoice_ids = fields.One2many('bulk.invoice.intermoneda','bulk_invoice_id',string='Invoice')
    partner_id = fields.Many2one('res.partner')
    intermoneda = fields.Boolean(string="Es intermoneda?", default=lambda self: self.env.context.get('intermoneda', False))
    total_amount = fields.Float(string="Monto total de pago", compute="_get_total_amount")



    @api.onchange('communication')
    def onchange_communication(self):
        if self.communication:
            for line in self.invoice_ids:
                line.communication = self.communication

    @api.onchange('journal_id')
    def onchange_journal_id(self):
        print "ESTA AQUI cuando cambia la cuenta"
        if self.journal_id:
            for line in self.invoice_ids:
                nueva_moneda = self.journal_id.currency_id.id or self.env.user.partner_id.company_id.currency_id.id
                #if self.env['res.currency'].browse(nueva_moneda).name=='MXN':
                invoice_date_datetime = datetime.datetime.strptime(self.payment_date, '%Y-%m-%d')
                rate_obj = self.env['res.currency.rate']
                print "esto es journal ", self.journal_id.currency_id
                rate_rec = rate_obj.search([
                    ('currency_id', '=', self.journal_id.currency_id.id),
                    ('name', '<=', str(invoice_date_datetime))
                    ], order='name desc', limit=1)
                if rate_rec:
                    rate = rate_obj.browse(rate_rec.id).rate
                else:
                    rate = 1.0
                line.tipocambiop = rate
                line.currency_id = nueva_moneda


    #@api.onchange('journal_id')
    #def onchange_journal_id(self):
        #if self.journal_id:
            #for line in self.invoice_ids:
                #nueva_moneda = self.journal_id.currency_id.id or self.env.user.partner_id.company_id.currency_id.id
                #line.currency_id = nueva_moneda
                #if self.env['res.currency'].browse(nueva_moneda).name=='MXN':
                    #invoice_date_datetime = datetime.datetime.strptime(self.payment_date, '%Y-%m-%d')
                    #rate_obj = self.env['res.currency.rate']
                    #rate_rec = rate_obj.search([
                        #('currency_id', '=', line.invoice_currency_id.id),
                        #('name', '<=', str(invoice_date_datetime))
                        #], order='name desc', limit=1)
                    #if rate_rec:
                        #rate = rate_obj.browse(rate_rec.id).rate
                    #else:
                        #rate = 1.0
                    #print "esto es rate ", rate
                    #rate = 1 / rate
                    #print "esto es rate ", rate
                    #print "esto es line.amount ", line.amount
                    #line.to_paid_amount = line.amount * rate
                    #line.full_payment = True
                    #line.paid_amount = line.amount


    @api.multi
    def process_payment(self):
        print "esto es context desde process_payment ", self.env.context
        #import time
        #time.sleep(30)
        if self.total_amount < 1:
            raise ValidationError('No puede hacer un pago en 0')
        monedas = []
        moneda = 0
        for line in self.invoice_ids:
            print "esto es monedas ", monedas
            #if line.currency_id.id in monedas:
            if line.invoice_currency_id.id == line.currency_id.id:
                #raise ValidationError('Todas las facturas tienen .')
                valida = False
                moneda = line.invoice_currency_id.id
            else:
                #monedas.append(line.currency_id.id)
                valida = True
        print "esto es monedas ", monedas
        print "esto es valida ", valida
        print "esto es moneda ", moneda
        if valida==False and (self.journal_id.currency_id.id == moneda ):
            raise ValidationError('Para hacer pagos en la misma moneda debe seleccionar el menu "Pago Multi Factura Misma moneda"')
        else:
            vals=[]
            for line in self.invoice_ids:
                print "esto es paid_amount ", line.paid_amount
                if line.paid_amount > 0.0:
                    #a_payment_line_ids =  self.env['advance.payment.line'].search([('invoice_id','=',line.invoice_id.id),('state','in',['posted','sent','reconciled'])])
                    a_payment_line_ids =  self.env['advance.payment.line'].search([('invoice_id','=',line.invoice_id.id),('state','in',['posted','sent','reconciled']),('folio_fiscal','!=',False)])
                    print "esto es a_payment_line_ids ", a_payment_line_ids
                    cont = len(a_payment_line_ids)
                    print "esto es cont ", cont
                    cont = cont + 1
                    print "esto es cont ", cont
                    vals.append({
                        'invoice_id': line.invoice_id or False,
                        'partner_id': line.partner_id and line.partner_id.id or False,
                        'amount': line.amount or 0.0,
                        'paid_amount': line.paid_amount or 0.0,
                        'to_paid_amount': line.to_paid_amount or 0.0,
                        'currency_id': line.invoice_id.currency_id.id or False,
                        'tipocambio': line.invoice_id.tipocambio or 0,
                        'reference': line.communication or False,
                        'currency_id': line.currency_id.id or False,
                        'no_de_pago': cont or 0,
                    })
            new_vals=sorted(vals,key=itemgetter('partner_id'))
            groups = itertools.groupby(new_vals, key=operator.itemgetter('partner_id'))
            result = [{'partner_id':k,'values':[x for x in v]} for k, v in groups]
            new_payment_ids=[]
            for res in result:
                payment_method_id= self.env['account.payment.method'].search([('name','=','Manual')],limit=1)
                if not payment_method_id:
                    payment_method_id= self.env['account.payment.method'].search([],limit=1)
                pay_val={
                    'payment_type':self.payment_type,
                    'payment_date':self.payment_date,
                    'partner_type':self.partner_type,
                    'partner_id':res.get('partner_id'),
                    'journal_id':self.journal_id and self.journal_id.id or False,
                    'communication':self.communication,
                    'payment_method_id':payment_method_id and payment_method_id.id or False,
                    'state':'draft',
                    #'amount':20,
                    'amount': self.total_amount,
                    'currency_id':res.get('values')[0].get('currency_id'),
                }
                payment_id = self.env['account.payment'].create(pay_val)
                line_list=[]
                paid_amt=0
                inv_ids = []
                for inv_line in res.get('values'):
                    print "esto es inv_line ", inv_line
                    invoice  = inv_line.get('invoice_id')
                    inv_ids.append(invoice.id)
                    full_reco=False
                    print "esto es paid_amount ", inv_line.get('paid_amount')
                    if invoice.residual == inv_line.get('paid_amount'):
                        full_reco = True
                    line_list.append((0,0,{
                        'invoice_id': invoice.id,
                        'account_id': invoice.account_id and invoice.account_id.id or False,
                        'date': invoice.date_invoice,
                        'due_date': invoice.date_due,
                        'original_amount': invoice.amount_total,
                        'balance_amount': invoice.residual,
                        'allocation': inv_line.get('paid_amount'),
                        'allocation1': inv_line.get('to_paid_amount'),
                        'full_reconclle': full_reco,
                        'account_payment_id': payment_id and payment_id.id or False,
                        'tipocambio': inv_line.get('tipocambio') or 0,
                        'reference': inv_line.get('reference') or False,
                        'currency_id': inv_line.get('currency_id') or False,
                        'no_de_pago': inv_line.get('no_de_pago') or 0
                    }))
                    #paid_amt += inv_line.get('paid_amount')
                payment_id.write({
                    'line_ids':line_list,
                    #'amount':paid_amt,
                    'invoice_ids':[(6,0,inv_ids)]
                })
            sequence_code = payment_id.post()
            nuevo_folio = self.env['ir.sequence'].with_context(ir_sequence_date=payment_id.payment_date).next_by_code(sequence_code)
            payment_id.write({'name':nuevo_folio})
            new_payment_ids.append(payment_id)
        return True


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
