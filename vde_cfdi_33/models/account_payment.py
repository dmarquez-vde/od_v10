#Embedded file name: C:\Program Files (x86)\Odoo 10.0\server\odoo\addons\cdfi_invoice\models\account_payment.py
import base64
import string
import json
import requests
import datetime
from lxml import etree
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from dateutil.relativedelta import relativedelta
import amount_to_text_es_MX
from reportlab.graphics.barcode import createBarcodeDrawing, getCodes
from reportlab.lib.units import mm
from suds.client import Client
import zeep

class account_payment_invoice(models.Model):

    _name='account.payment.line.invoice'

    payment_id = fields.Many2one('account.payment', string="Pago", ondelete='cascade', index=True)
    invoice_id = fields.Many2one('account.invoice', string="Factura", ondelete='restrict', index=True)
    metodo_pago = fields.Many2one('c.metodopago', string="M\xc3\xa9todo de pago", related='invoice_id.methodo_pago')
    moneda_id = fields.Many2one('res.currency', string="Moneda", related='invoice_id.currency_id')
    name = fields.Char(string="UUID", related='invoice_id.folio_fiscal', store=True)
    #by_payment = fields.Boolean(string="Pago de factura?", default=False)

class AccountPayment(models.Model):
    _inherit = 'account.payment'

    line_ids = fields.One2many('advance.payment.line','account_payment_id')

    @api.multi
    def post(self):
        """ Create the journal items for the payment and update the payment's state to 'posted'.
            A journal entry is created containing an item in the source liquidity account (selected journal's default_debit or default_credit)
            and another in the destination reconciliable account (see _compute_destination_account_id).
            If invoice_ids is not empty, there will be one reconciliable move line per invoice to reconcile with.
            If the payment is a transfer, a second journal entry is created in the destination journal to receive money from the transfer account.
        """
        #if self.line_ids:
            #amt=0.0
            #for line in self.line_ids:
                #amt += line.allocation
            #if self.amount < amt:
                #raise ValidationError(("Amount is must be greater or equal '%s'") %(amt))
            #if self.amount > amt:
                #if (not self.env.context.get('from_invoice')) and (not self.env.context.get('intermoneda')):
                    #for line in self.line_ids:
                        #line.allocation = line.allocation + (self.amount - amt)
                        #break

        for rec in self:

            if rec.cancelado == True:
		raise UserError(_("No es posible confirmar un pago que previamente fue cancelado."))

            if rec.state != 'draft':
                raise UserError(_("Only a draft payment can be posted. Trying to post a payment in state %s.") % rec.state)

            if any(inv.state != 'open' for inv in rec.invoice_ids):
                raise ValidationError(_("The payment cannot be processed because the invoice is not open!"))

            # Use the right sequence to set the name
            if rec.payment_type == 'transfer':
                sequence_code = 'account.payment.transfer'
            else:
                if rec.partner_type == 'customer':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.customer.invoice'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.customer.refund'
                if rec.partner_type == 'supplier':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.supplier.refund'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.supplier.invoice'
            #if not rec.name:
            if rec.name == 'Draft Payment':
                #rec.name = self.env['ir.sequence'].with_context(ir_sequence_date=rec.payment_date).next_by_code(sequence_code)
		print "No da folio"
            if not rec.name and rec.payment_type != 'transfer':
                raise UserError(_("You have to define a sequence for %s in your company.") % (sequence_code,))

            # Create the journal entry
            amount = rec.amount * (rec.payment_type in ('outbound', 'transfer') and 1 or -1)
            #print "esto es amoun t", amount
            move = rec._create_payment_entry(amount)
            #print "esto es move ", move
            # In case of a transfer, the first journal entry created debited the source liquidity account and credited
            # the transfer account. Now we debit the transfer account and credit the destination liquidity account.
            if rec.payment_type == 'transfer':
                transfer_credit_aml = move.line_ids.filtered(lambda r: r.account_id == rec.company_id.transfer_account_id)
                transfer_debit_aml = rec._create_transfer_entry(amount)
                (transfer_credit_aml + transfer_debit_aml).reconcile()

            rec.write({'state': 'posted', 'move_name': move.name})
            return sequence_code


    factura_cfdi = fields.Boolean('Factura CFDI', copy=False)
    invoice_line = fields.One2many('account.payment.line.invoice', 'payment_id', string="Facturas relacionadas")
    forma_pago = fields.Many2one('c.formapago', string="Forma de pago")
    forma_pago_code = fields.Char(string='Codigo Forma pago', related='forma_pago.code')
    #forma_pago = fields.Selection(selection=[('01', _('Efectivo')),
     #('02', _('Cheque')),
     #('03', _('Transferencia')),
     #('04', _('Tarjeta de cr\xc3\xa9dito')),
     #('28', _('Tarjeta de d\xc3\xa9bito'))], string=_('Forma de pago'))
    tipo_comprobante = fields.Selection(selection=[('P', 'Pago')], string=_('Tipo de comprobante'), default='P')
    methodo_pago = fields.Selection(selection=[('PUE', _('Pago en una sola exhibici\xc3\xb3n')), ('PPD', _('Pago en parcialidades o diferido'))], string=_('M\xc3\xa9todo de pago'))
    no_de_pago = fields.Integer('No. de pago', readonly=True)
    #saldo_pendiente = fields.Float('Saldo pendiente', readonly=True)
    saldo_pendiente = fields.Monetary('Saldo pendiente', readonly=True)
    #monto_pagar = fields.Float('Monto a pagar', compute='_compute_monto_pagar')
    monto_pagar = fields.Monetary('Monto a pagar', compute='_compute_monto_pagar')
    #saldo_restante = fields.Float('Saldo restante', readonly=True, compute='_get_monto_restante', store=True)
    saldo_restante = fields.Monetary('Saldo restante', readonly=True, compute='_get_monto_restante', store=True)
    #fecha_pago = fields.Datetime('Fecha de pago')
    banco_emisor = fields.Selection(selection=[('BBA940707IE1', 'BANCO DEL BAJIO'),
     ('BII931004P61', 'BANCO INBURSA'),
     ('BIN931011519', 'BANCO INTERACCIONES'),
     ('BMN930209927', 'BANCO MERCANTIL DEL NORTE'),
     ('BMI9704113PA', 'BANCO MONEX'),
     ('BMI061005NY5', 'BANCO MULTIVA'),
     ('BAF950102JP5', 'BANCA AFIRME'),
     ('BBA830831LJ2', 'BBVA BANCOMER'),
     ('HMI950125KG8', 'HSBC'),
     ('IBA950503GTA', 'IXE BANCO'),
     ('SIN9412025I4', 'SCOTIABANK INVERLAT'),
     ('BSM970519DU8', 'BANCO SANTANDER'),
     ('BNM840515VB1', 'BANCO NACIONAL DE MEXICO'),
     ('BNE820901682', 'BANCO NACIONAL DE EJERCITO FUERZA AEREA Y ARMADA'),
     ('BRM940216EQ6', 'BANCO REGIONAL DE MONTERREY'),
     ('BAI0205236Y8', 'BANCO AZTECA')], string=_('Banco emisor'))
    cuenta_emisor = fields.Char('Cuenta del emisor')
    rfc_banco_emisor = fields.Char(_('RFC banco emisor'), compute='_compute_rfc_banco_emisor')
    numero_operacion = fields.Char(_('N\xc3\xbamero de operaci\xc3\xb3n'))
    banco_receptor = fields.Char(_('Banco receptor'), compute='_compute_banco_receptor')
    cuenta_beneficiario = fields.Char(_('Cuenta beneficiario'), compute='_compute_banco_receptor')
    rfc_banco_receptor = fields.Char(_('RFC banco receptor'), compute='_compute_banco_receptor')
    #estado_pago = fields.Selection(selection=[('pago_no_enviado', 'Complemento no enviado'), ('pago_correcto', 'Complemento generado'), ('problemas_pago', 'Problemas con el Complemento'),('pago_cancelado', 'Complemento cancelado')], string=_('Estado de pago'), default='pago_no_enviado', readonly=True)
    #tipo_relacion = fields.Selection(selection=[('04', 'Sustituci\xc3\xb3n de los CFDI previos')], string=_('Tipo relaci\xc3\xb3n'))
    uso_cfdi = fields.Many2one('c.usocfdi', string="Uso de CFDI")
    #cfdi_relacionado = fields.Many2one('account.invoice', string=_('CFDI Relacionado'))
    #uuid_relacionado = fields.Char(string=_('UUID CFDI Relacionado'),related='cfdi_relacionado.folio_fiscal',)
    #metodo_pago_relacionado = fields.Many2one('c.metodopago', string="M\xc3\xa9todo de pago", related='cfdi_relacionado.metodo_pago')
    confirmacion = fields.Char(string=_('Confirmaci\xc3\xb3n'))
    sustitucion = fields.Boolean(string="Es una sustitucion?", default=False)
    tipo_relacion = fields.Many2one('c.tiporelacion', string="Tipo Relacion")
    sustituido = fields.Boolean(string="Ha sido sustituido?", default=False)
    vde_payment = fields.Many2one('account.payment', string="Facturas relacionadas")


    folio_fiscal = fields.Char(string=_('Folio Fiscal'), readonly=True)
    numero_cetificado = fields.Char(string=_('Numero de cetificado'))
    cetificaso_sat = fields.Char(string=_('Cetificao SAT'))
    fecha_certificacion = fields.Char(string=_('Fecha y Hora Certificaci\xc3\xb3n'))
    cadena_origenal = fields.Char(string=_('Cadena Origenal del Complemento digital de SAT'))
    selo_digital_cdfi = fields.Char(string=_('Selo Digital del CDFI'))
    selo_sat = fields.Char(string=_('Selo del SAT'))
    moneda = fields.Char(string=_('Moneda'))
    monedap = fields.Char(string=_('Moneda'))
    tipocambio = fields.Char(string=_('TipoCambio'))
    tipocambiop = fields.Char(string=_('TipoCambio'))
    folio = fields.Char(string=_('Folio'), compute='_get_serie_folio', store=True)
    serie = fields.Char(string=_('Serie'), compute='_get_serie_folio', store=True)
    version = fields.Char(string=_('Version'))
    number_folio = fields.Char(string=_('Folio'), compute='_get_number_folio')
    amount_to_text = fields.Char('Amount to Text', compute='_get_amount_to_text', size=256, help='Amount of the invoice in letter')
    qr_value = fields.Char(string=_('QR Code Value'))
    qrcode_image = fields.Binary('QRCode')
    rfc_emisor = fields.Char(string=_('RFC'))
    name_emisor = fields.Char(string=_('Name'))
    xml_payment_link = fields.Char(string=_('XML link'), readonly=True)
    payment_mail_ids = fields.One2many('account.payment.mail', 'payment_id', string='Payment Mails')
    iddocumento = fields.Char(string=_('iddocumento'))
    fecha_emision = fields.Char(string=_('Fecha y Hora Certificaci\xc3\xb3n'))
    update_id = fields.One2many('account.payment.update', 'payment_id', string="Intentos")
    cancelado = fields.Boolean(string="Se hizo clic en boton cancelar?", default=False)

    @api.onchange('vde_payment')
    def onchange_vde_payment(self):
        print "Esta entrando al onchange ...............",self.vde_payment.folio_fiscal
        if self.vde_payment.folio_fiscal == False:
            print "Esta entrando aqui ...............",self.vde_payment.folio_fiscal
            raise UserError(_('El pago que intentas relacionar no tiene folio Fiscal'))

    #@api.depends('name')
    #@api.one
    #def _get_number_folio(self):
        #if self.number:
            #self.number_folio = self.name.replace('CUST.IN', '').replace('/', '')

    @api.one
    @api.depends('name')
    def _get_serie_folio(self):
	if self.partner_type == 'customer':
            if self.name != 'Draft Payment':
                self.serie = string.split(self.name,'-')[0]
                self.folio = string.split(self.name,'-')[1]

    @api.one
    @api.depends('saldo_pendiente', 'monto_pagar')
    def _get_monto_restante(self):
        if self.saldo_pendiente or self.monto_pagar:
            self.saldo_restante = self.saldo_pendiente - self.monto_pagar

    @api.model
    def create(self, vals):
        #print "entra a create ", vals
        if vals.get('line_ids'):
            inv_ids = []
            for line in vals.get('line_ids'):
                inv_ids.append(line[2].get('invoice_id'))

            vals.update({
            'invoice_ids':[(6,0,inv_ids)]
            })
        lineas=[]
        cont=1
        #print "esto es vals desde account.payment,context ", self.env.context
        #print "esto es vals desde account.payment,create ", vals
        res = super(AccountPayment, self).create(vals)
        if res.invoice_ids:
            #print "esto es invoice_ids ",res.invoice_ids
            #print "esto es invoice_ids ",res.invoice_ids[0].payment_ids
            #for payment in res.invoice_ids[0].payment_ids:
                #if payment.state=='posted' or payment.state=='recondiled':
                    #cont=cont+1
            #res.no_de_pago = len(res.invoice_ids[0].payment_ids)
            if self.env.context.get('from_invoice'):
                for invoice in res.invoice_ids:
                    #print "esto es invoice ", invoice
                    a_payment_line_ids =  self.env['advance.payment.line'].search([('invoice_id','=',invoice.id),('state','in',['posted','sent','reconciled'])])
                    #print "esto es a_payment_line_ids ", a_payment_line_ids
                    cont = len(a_payment_line_ids)+1
                    valores={
                        'invoice_id': invoice.id,
                        'reference': res.communication or False,
                        'account_id': invoice.account_id.id or False,
                        'date': res.payment_date or False,
                        #'due_date': False,
                        'original_amount': invoice.amount_total or 0.0,
                        'balance_amount': invoice.residual or 0.0,
                        #'full_reconclle',
                        'allocation': res.amount or 0.0,
                        'account_payment_id': res.id or False,
                        'currency_id': res.currency_id.id or False,
                        'no_de_pago': cont or 0,
                        'tipocambio': invoice.tipocambio or 0,
                        }
                    self.env['advance.payment.line'].create(valores)
        if res.partner_id:
            res.banco_emisor = res.partner_id.banco_emisor
            res.cuenta_emisor = res.partner_id.cuenta_emisor
        if self.env.context.get('active_ids'):
            #print "esto es active_ids ",self.env.context.get('active_ids')
            invoice_ids = self.env.context.get('active_ids')
            for invoice_id in invoice_ids:
                linea_id=self.env['account.payment.line.invoice'].create({
                    'payment_id':res.id,
                    'invoice_id':invoice_id,
                    })
                lineas.append(linea_id.id)
            res.invoice_line = [(6,0,lineas)]
        return res



    @api.model
    def create_old(self, vals):
        lineas=[]
        cont=1
        #print "esto es vals desde account.payment,context ", self.env.context
        #print "esto es vals desde account.payment,create ", vals
        res = super(AccountPayment, self).create(vals)
        if res.invoice_ids:
            #print "esto es invoice_ids ",res.invoice_ids
            #print "esto es invoice_ids ",res.invoice_ids[0].payment_ids
            for payment in res.invoice_ids[0].payment_ids:
                if payment.state=='posted' or payment.state=='recondiled':
                    cont=cont+1
            #res.no_de_pago = len(res.invoice_ids[0].payment_ids)
            res.no_de_pago = cont
            res.saldo_pendiente = res.invoice_ids[0].residual
            res.saldo_restante = res.saldo_pendiente - res.monto_pagar
        if res.partner_id:
            res.banco_emisor = res.partner_id.banco_emisor
            res.cuenta_emisor = res.partner_id.cuenta_emisor
        if self.env.context.get('active_ids'):
            #print "esto es active_ids ",self.env.context.get('active_ids')
            invoice_ids = self.env.context.get('active_ids')
            for invoice_id in invoice_ids:
                linea_id=self.env['account.payment.line.invoice'].create({
                    'payment_id':res.id,
                    'invoice_id':invoice_id,
                    })
                lineas.append(linea_id.id)
            res.invoice_line = [(6,0,lineas)]
        return res

    @api.one
    @api.depends('amount')
    def _compute_monto_pagar(self):
        if self.amount:
            self.monto_pagar = self.amount

    @api.one
    @api.depends('banco_emisor')
    def _compute_rfc_banco_emisor(self):
        if self.banco_emisor:
            self.rfc_banco_emisor = self.banco_emisor

    @api.one
    @api.depends('journal_id')
    def _compute_banco_receptor(self):
        if self.journal_id and self.journal_id.bank_id:
            self.banco_receptor = self.journal_id.bank_id.name
            self.rfc_banco_receptor = self.journal_id.bank_id.bic
        if self.journal_id:
            self.cuenta_beneficiario = self.journal_id.bank_acc_number

    @api.depends('amount', 'currency_id')
    @api.one
    def _get_amount_to_text(self):
        self.amount_to_text = amount_to_text_es_MX.get_amount_to_text(self, self.amount_total, 'es_cheque', self.currency_id.name)

    @api.model
    def _get_amount_2_text(self, amount_total):
        return amount_to_text_es_MX.get_amount_to_text(self, amount_total, 'es_cheque', self.currency_id.name)

    @api.model
    def to_json(self):
        if not self.company_id.archivo_cer:
            raise UserError(_('Archivo .cer path is missing.'))
        archivo_cer_file = open(self.company_id.archivo_cer, 'rb').read()
        if not self.company_id.archivo_key:
            raise UserError(_('Archivo .key path is missing.'))
        archivo_key_file = open(self.company_id.archivo_key, 'rb').read()
        if self.invoice_ids:
            invoice = self.invoice_ids[0]
            archivo_cer = base64.b64encode(archivo_cer_file)
            archivo_key = base64.b64encode(archivo_key_file)
            self.tipocambio = invoice.tipocambio
            request_params = {'company': {'rfc': self.company_id.rfc,
                         'api_key': self.company_id.api_key,
                         'modo_prueba': self.company_id.modo_prueba,
                         'regimen_fiscal': self.company_id.regimen_fiscal,
                         'postalcode': self.company_id.zip,
                         'nombre_fiscal': self.company_id.nombre_fiscal},
             'customer': {'name': self.partner_id.name,
                          'rfc': self.partner_id.rfc,
                          'uso_cfdi': 'P01'},
             'invoice': {'tipo_comprobante': self.tipo_comprobante,
                         'folio_complemento': self.name.replace('CUST.IN', '').replace('/', ''),
                         'serie_complemento': self.company_id.serie_complemento},
             'concept': {'claveprodserv': '84111506',
                         'calveunidad': 'ACT',
                         'cantidad': 1,
                         'descripcion': 'Pago'},
             'payment': {'moneda': self.currency_id.name,
                         'tipocambio': self.currency_id.rate,
                         'forma_pago': self.forma_pago,
                         'numero_operacion': self.numero_operacion,
                         'banco_emisor': self.banco_emisor,
                         'cuenta_emisor': self.cuenta_emisor,
                         'rfc_banco_emisor': self.rfc_banco_emisor,
                         'banco_receptor': self.banco_receptor,
                         'cuenta_beneficiario': self.cuenta_beneficiario,
                         'rfc_banco_receptor': self.rfc_banco_receptor,
                         'fecha_pago': self.fecha_pago,
                         'monto_factura': invoice.amount_total},
             'docto_relacionado': {'moneda': invoice.moneda,
                                   'tipodecambio': invoice.tipocambio,
                                   'iddocumento': invoice.folio_fiscal,
                                   'no_de_pago': self.no_de_pago,
                                   'saldo_pendiente': self.saldo_pendiente,
                                   'monto_pagar': self.monto_pagar,
                                   'saldo_restante': self.saldo_restante},
             'adicional': {'tipo_relacion': self.tipo_relacion,
                           'uuid_relacionado': self.uuid_relacionado,
                           'confirmacion': self.confirmacion},
                           'sustitucion':self.sustitucion,
             'certificados': {'archivo_cer': archivo_cer,
                              'archivo_key': archivo_key,
                              'contrasena': self.company_id.contrasena}}
        else:
            archivo_cer = base64.b64encode(archivo_cer_file)
            archivo_key = base64.b64encode(archivo_key_file)
            request_params = {'company': {'rfc': self.company_id.rfc,
                         'api_key': self.company_id.api_key,
                         'modo_prueba': self.company_id.modo_prueba,
                         'regimen_fiscal': self.company_id.regimen_fiscal,
                         'postalcode': self.company_id.zip,
                         'nombre_fiscal': self.company_id.nombre_fiscal},
             'customer': {'name': self.partner_id.name,
                          'rfc': self.partner_id.rfc,
                          'uso_cfdi': 'P01'},
             'invoice': {'tipo_comprobante': self.tipo_comprobante,
                         'folio_complemento': self.name.replace('CUST.IN', '').replace('/', ''),
                         'serie_complemento': self.company_id.serie_complemento},
             'concept': {'claveprodserv': '84111506',
                         'calveunidad': 'ACT',
                         'cantidad': 1,
                         'descripcion': 'Anticipo del bien o servicio'},
             'payment': {'moneda': self.currency_id.name,
                         'tipocambio': self.currency_id.rate,
                         'forma_pago': self.forma_pago,
                         'numero_operacion': self.numero_operacion,
                         'banco_emisor': self.banco_emisor,
                         'cuenta_emisor': self.cuenta_emisor,
                         'rfc_banco_emisor': self.rfc_banco_emisor,
                         'banco_receptor': self.banco_receptor,
                         'cuenta_beneficiario': self.cuenta_beneficiario,
                         'rfc_banco_receptor': self.rfc_banco_receptor,
                         'fecha_pago': self.fecha_pago,
                         'monto_factura': self.amount},
             'docto_relacionado': {'moneda': 'false',
                                   'tipodecambio': 'false',
                                   'iddocumento': 'false',
                                   'no_de_pago': 'false',
                                   'saldo_pendiente': 'false',
                                   'monto_pagar': 'false',
                                   'saldo_restante': 'false'},
             'adicional': {'tipo_relacion': self.tipo_relacion,
                           'uuid_relacionado': self.uuid_relacionado,
                           'confirmacion': self.confirmacion},
                           'sustitucion': self.sustitucion,
             'certificados': {'archivo_cer': archivo_cer,
                              'archivo_key': archivo_key,
                              'contrasena': self.company_id.contrasena}}
        #print '::::::::::::::::::::'
        #print request_params
        return request_params

    @api.multi
    def complete_payment(self):
        for p in self:
            values = p.to_json()
            url = '%s%s' % (p.company_id.http_factura2, '/payment?handler=OdooHandler33')
            response = requests.post(url, auth=None, verify=False, data=json.dumps(values), headers={'Content-type': 'application/json'})
            json_response = response.json()
            xml_file_link = False
            estado_pago = json_response['estado_pago']
            if estado_pago == 'problemas_pago':
                raise UserError(_(json_response['problemas_message']))
            if json_response.get('pago_xml'):
                xml_file_link = p.company_id.factura_dir + '/' + p.name.replace('/', '_') + '.xml'
                xml_file = open(xml_file_link, 'w')
                xml_payment = base64.b64decode(json_response['pago_xml'])
                xml_file.write(xml_payment)
                xml_file.close()
                p._set_data_from_xml(xml_payment)
                xml_file_name = p.name.replace('/', '_') + '.xml'
                self.env['ir.attachment'].sudo().create({'name': xml_file_name,
                 'datas': json_response['pago_xml'],
                 'datas_fname': xml_file_name,
                 'res_model': self._name,
                 'res_id': p.id,
                 'type': 'binary'})
                Template = self.env['mail.template']
                report_data = Template.env['report'].get_pdf([self.id], 'cdfi_invoice.report_payment')
                pdf_file_name = p.name.replace('/', '_') + '.pdf'
                self.env['ir.attachment'].sudo().create({'name': pdf_file_name,
                 'datas': base64.b64encode(report_data),
                 'datas_fname': pdf_file_name,
                 'res_model': self._name,
                 'res_id': p.id,
                 'type': 'binary'})
            p.write({'estado_pago': estado_pago,
             'xml_payment_link': xml_file_link})

    @api.multi
    def validate_complete_payment(self):
        self.post()
        return {'name': _('Payments'),
         'view_type': 'form',
         'view_mode': 'form',
         'res_model': 'account.payment',
         'view_id': False,
         'type': 'ir.actions.act_window',
         'res_id': self.id}

    @api.multi
    def cancel(self):
        for rec in self:
            for move in rec.move_line_ids.mapped('move_id'):
                if rec.invoice_ids:
                    rec.state = 'draft'
                    rec.cancelado = True
                    move.line_ids.remove_move_reconcile()
                move.button_cancel()
                move.unlink()
            

    @api.one
    def action_cfdi_sequence(self):
	if self.serie != "" or self.folio !="":
            if self.name == 'Draft Payment':
                if self.partner_type == 'customer':
                    if self.payment_type == 'inbound':
                        sequence_code = 'account.payment.customer.invoice'
                    if self.payment_type == 'outbound':
                        sequence_code = 'account.payment.customer.refund'
                if self.partner_type == 'supplier':
                    if self.payment_type == 'inbound':
                        sequence_code = 'account.payment.supplier.refund'
                    if self.payment_type == 'outbound':
                        sequence_code = 'account.payment.supplier.invoice'
            temp_name = self.env['ir.sequence'].with_context(ir_sequence_date=self.payment_date).next_by_code(sequence_code)
            if self.write({'name':temp_name}):
                return True
            else:
		raise UserError(_('Ocurrio un error al asignar Serie/Folio intente nuevamente.'))


    @api.one
    def action_cfdi_generate(self):
	if (self.serie==False or self.folio==False):
            print "no trae serie o folio"
            raise UserError(_('El documento no tiene Serie / Folio, por favor haga clic en el boton "Asignar Serie/Folio" e intente nuevamente.'))
        else:
            print "si trae serie o folio", self.serie
            print "si trae serie o folio", self.folio
        res={}
        valida = ''
        for intentos in self.update_id:
            if intentos.name == 'complemento_correcto':
                valida = 'comeplemento_correcto'
                continue
            elif intentos.name == 'complemento_cancelado':
                valida = 'complemento_cancelado'
                continue
        #if self.estado_factura == 'factura_correcta':
        if valida == 'comeplemento_correcto':
            raise UserError(_('El pago ya tiene un CFDI generado'))
        #if self.estado_factura == 'factura_cancelada':
        if valida == 'complemento_cancelado':
            raise UserError(_('No puede generar un CFDI de una pago con CFDI cancelado.'))
        url = self.company_id.http_factura2 or False
        #print "esto es URL ", url
        if not url:
            raise UserError(_('La compania no tiene configurado la URL del Web Service.'))
        cliente_cfdi = Client(url)
        #cliente_cfdi = zeep.Client(wsdl=url)
        #print "esto es el cliente_cfdi"
        #print cliente_cfdi
        #print "database ",self.env.cr.dbname
        response = cliente_cfdi.service.procesaDocto(self.id, self.env.cr.dbname, 'ODOO', self.env.user.company_id.ip or False, self.tipo_comprobante or '')
        #print "esto es response ", response
        #print "esto es codigo ", response['codigo']
        #print "esto es detalle ", response['detalle']
        if response['codigo']=='problemas_complemento':
            #print "entra a prblemas"
            #print "intenta crear account.payment.update"
            self.env['account.payment.update'].create({'payment_id':self.id, 'name':response['codigo'], 'detail':response['detalle']})
        if response['codigo']=='complemento_correcto':
            #print "entra a correcto"
            #print "intenta crear account.payment.update"
            self.env['account.payment.update'].create({'payment_id':self.id, 'name':response['codigo'], 'detail':response['detalle']})
            self.vde_payment.write({'sustituido':True})
            #raise UserError(_('Factura timbrada correctamente.'))
        return True

    @api.one
    def _set_data_from_xml(self, xml_payment):
        if not xml_payment:
            return None
        NSMAP = {'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
         'cfdi': 'http://www.sat.gob.mx/cfd/3',
         'tfd': 'http://www.sat.gob.mx/TimbreFiscalDigital',
         'pago10': 'http://www.sat.gob.mx/Pagos'}
        xml_data = etree.fromstring(xml_payment)
        Emisor = xml_data.find('cfdi:Emisor', NSMAP)
        RegimenFiscal = Emisor.find('cfdi:RegimenFiscal', NSMAP)
        Complemento = xml_data.find('cfdi:Complemento', NSMAP)
        TimbreFiscalDigital = Complemento.find('tfd:TimbreFiscalDigital', NSMAP)
        Pagos = Complemento.find('pago10:Pagos', NSMAP)
        Pago = Pagos.find('pago10:Pago', NSMAP)
        DoctoRelacionado = Pago.find('pago10:DoctoRelacionado', NSMAP)
        self.rfc_emisor = Emisor.attrib['Rfc']
        self.name_emisor = Emisor.attrib['Nombre']
        if self.invoice_ids:
            self.methodo_pago = DoctoRelacionado.attrib['MetodoDePagoDR']
            self.moneda = DoctoRelacionado.attrib['MonedaDR']
            self.monedap = Pago.attrib['MonedaP']
            if self.monedap != 'MXN':
                self.tipocambiop = Pago.attrib['TipoCambioP']
            if self.moneda != self.monedap:
                self.tipocambio = DoctoRelacionado.attrib['TipoCambioDR']
            self.iddocumento = DoctoRelacionado.attrib['IdDocumento']
        self.numero_cetificado = xml_data.attrib['NoCertificado']
        self.fecha_emision = xml_data.attrib['Fecha']
        self.cetificaso_sat = TimbreFiscalDigital.attrib['NoCertificadoSAT']
        self.fecha_certificacion = TimbreFiscalDigital.attrib['FechaTimbrado']
        self.selo_digital_cdfi = TimbreFiscalDigital.attrib['SelloCFD']
        self.selo_sat = TimbreFiscalDigital.attrib['SelloSAT']
        self.folio_fiscal = TimbreFiscalDigital.attrib['UUID']
        self.folio = xml_data.attrib['Folio']
        self.invoice_datetime = xml_data.attrib['Fecha']
        self.version = TimbreFiscalDigital.attrib['Version']
        self.cadena_origenal = '||%s|%s|%s|%s|%s||' % (self.version,
         self.folio_fiscal,
         self.fecha_certificacion,
         self.selo_digital_cdfi,
         self.cetificaso_sat)
        options = {'width': 275 * mm,
         'height': 275 * mm}
        amount_str = str(self.amount).split('.')
        #print 'amount_str, ', amount_str
        qr_value = '?re=%s&rr=%s&tt=%s.%s&id=%s' % (self.company_id.rfc,
         self.partner_id.rfc,
         amount_str[0].zfill(10),
         amount_str[1].ljust(6, '0'),
         self.folio_fiscal)
        self.qr_value = qr_value
        ret_val = createBarcodeDrawing('QR', value=qr_value, **options)
        self.qrcode_image = base64.encodestring(ret_val.asString('jpg'))
        self.folio_fiscal = TimbreFiscalDigital.attrib['UUID']

    @api.multi
    def send_payment(self):
        self.ensure_one()
        if not self.payment_mail_ids:
            self.payment_mail_ids.sudo().create({'payment_id': self.ids[0]})
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('vde_cfdi_33', 'email_template_payment')[1]
        except ValueError:
            template_id = False

        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False

        ctx = dict()
        ctx.update({'default_model': 'account.payment.mail',
         'default_res_id': self.payment_mail_ids[0].id,
         'default_use_template': bool(template_id),
         'default_template_id': template_id,
         'default_composition_mode': 'comment'})
        return {'type': 'ir.actions.act_window',
         'view_type': 'form',
         'view_mode': 'form',
         'res_model': 'mail.compose.message',
         'views': [(compose_form_id, 'form')],
         'view_id': compose_form_id,
         'target': 'new',
         'context': ctx}

    def _get_counterpart_move_line_vals(self, invoice=False):
        res = super(AccountPayment,self)._get_counterpart_move_line_vals(invoice)
        if self.payment_type == 'outbound' and self.partner_type == 'supplier':
            if invoice:
                name = ''
                for inv in invoice:
                    if inv.reference:
                        if name :
                            name = name + ','+inv.reference
                        else:
                            name = inv.reference
                res.update({
                    'name':name,
                })
        return res

    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        if (not self.env.context.get('from_invoice')) and (not self.env.context.get('intermoneda')):
            acc_invoice = []
            account_inv_obj = self.env['account.invoice']
            invoice_ids=[]
            if self.partner_type == 'customer':
                invoice_ids = account_inv_obj.search([('partner_id', 'in', [self.partner_id.id]),('state', '=','open'),('type','in',['out_invoice','out_refund'])])
            else:
                invoice_ids = account_inv_obj.search([('partner_id', 'in', [self.partner_id.id]),('state', '=','open'),('type','in',['in_invoice','in_refund'])])
            curr_pool=self.env['res.currency']
            for vals in invoice_ids:
                ref = ''
                if self.partner_type == 'customer':
                    ref = vals.name
                else:
                    ref = vals.reference

                original_amount = vals.amount_total
                balance_amount = vals.residual
                allocation = vals.residual
                if vals.currency_id.id != self.currency_id.id:
                    currency_id = self.currency_id.with_context(date=self.payment_date)
                    original_amount = curr_pool._compute(vals.currency_id, currency_id, original_amount, round=True)
                    balance_amount = curr_pool._compute(vals.currency_id, currency_id, balance_amount, round=True)
                    allocation = curr_pool._compute(vals.currency_id, currency_id, allocation, round=True)

                acc_invoice.append({'invoice_id':vals.id,'account_id':vals.account_id.id,
                'date':vals.date_invoice,'due_date':vals.date_due,
                'original_amount':original_amount,'balance_amount':balance_amount,
                'allocation':allocation,'full_reconclle':True,'reference':ref,'currency_id':self.currency_id.id})
            self.line_ids = acc_invoice

    @api.onchange('currency_id')
    def onchange_currency(self):
        if (not self.env.context.get('from_invoice')) and (not self.env.context.get('intermoneda')):
            	curr_pool=self.env['res.currency']
            	if self.currency_id and self.line_ids:
            		for line in self.line_ids:
            			if line.currency_id.id != self.currency_id.id:
            				currency_id = self.currency_id.with_context(date=self.payment_date)
            				line.original_amount = curr_pool._compute(line.currency_id, currency_id, line.original_amount, round=True)
            				line.balance_amount = curr_pool._compute(line.currency_id, currency_id, line.balance_amount, round=True)
            				line.allocation = curr_pool._compute(line.currency_id, currency_id, line.allocation, round=True)
            				line.currency_id = self.currency_id and self.currency_id.id or False

class account_payment_update(models.Model):

    _name = 'account.payment.update'

    payment_id = fields.Many2one('account.payment', string="Pago", readonly=True)
    name = fields.Selection(selection=[('complemento_no_generado', 'CFDI No generado'),
     ('complemento_correcto', 'CFDI generado correctamente'),
     ('problemas_complemento', 'Problemas en el CFDI'),
     ('complemento_cancelado', 'CFDI cancelado')], string=_('Estado de pago'), default='complemento_no_generado', readonly=True, copy=False)
    detail = fields.Text(string="WS Detail",readonly=True)

class AccountPaymentMail(models.Model):
    _name = 'account.payment.mail'
    _inherit = ['mail.thread']
    _description = 'Payment Mail'
    payment_id = fields.Many2one('account.payment', string='Payment')
    name = fields.Char(related='payment_id.name')
    xml_payment_link = fields.Char(related='payment_id.xml_payment_link')
    partner_id = fields.Many2one(related='payment_id.partner_id')
    company_id = fields.Many2one(related='payment_id.company_id')


#class MailTemplate(models.Model):
    #"""Templates for sending email"""
    #_inherit = 'mail.template'

    #@api.model
    #def _get_file(self, url):
        #url = url.encode('utf8')
        #filename, headers = urllib.urlretrieve(url)
        #fn, file_extension = os.path.splitext(filename)
        #return (filename, file_extension.replace('.', ''))

    #@api.multi
    #def generate_email(self, res_ids, fields = None):
        #results = super(MailTemplate, self).generate_email(res_ids, fields=fields)
        #if isinstance(res_ids, (int, long)):
            #res_ids = [res_ids]
        #res_ids_to_templates = super(MailTemplate, self).get_email_template(res_ids)
        #templates_to_res_ids = {}
        #for res_id, template in res_ids_to_templates.iteritems():
            #templates_to_res_ids.setdefault(template, []).append(res_id)

        #template_id = self.env.ref('vde_cfdi_33.email_template_payment')
        #for template, template_res_ids in templates_to_res_ids.iteritems():
            #if template.id == template_id.id:
                #for res_id in template_res_ids:
                    #payment = self.env[template.model].browse(res_id)
                    #if payment.xml_payment_link:
                        #attachments = results[res_id]['attachments'] or []
                        #names = payment.xml_payment_link.split('/')
                        #fn = names[len(names) - 1]
                        #data = open(payment.xml_payment_link, 'rb').read()
                        #attachments.append((fn, base64.b64encode(data)))
                        #results[res_id]['attachments'] = attachments

        #return results
