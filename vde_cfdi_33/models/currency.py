# -*- coding: utf-8 -*-
import math
from odoo import fields, models, api, _

class Currency(models.Model):

    _inherit = 'res.currency'

    @api.multi
    @api.depends('rounding')
    def _compute_decimal_places(self):
        for currency in self:
            if 0 < currency.rounding < 1:
                #currency.decimal_places = int(math.ceil(math.log10(1/currency.rounding)))
                currency.decimal_places = 2
            else:
                currency.decimal_places = 0

    @api.model
    def _get_conversion_rate(self, from_currency, to_currency):
        print "entra a _get_conversion_rate "
        from_currency = from_currency.with_env(self.env)
        to_currency = to_currency.with_env(self.env)
        print "esto es to_currency.rate ",to_currency.rate
        print "esto es from_currency.rate ",from_currency.rate
        return (1/to_currency.rate) / from_currency.rate

    @api.multi
    def compute(self, from_amount, to_currency, round=True):
        print "entra a compute "
        """ Convert `from_amount` from currency `self` to `to_currency`. """
        self, to_currency = self or to_currency, to_currency or self
        assert self, "compute from unknown currency"
        assert to_currency, "compute to unknown currency"
        # apply conversion rate
        if self == to_currency:
            to_amount = from_amount
        else:
            to_amount = from_amount * self._get_conversion_rate(self, to_currency)
        # apply rounding
        return to_currency.round(to_amount) if round else to_amount