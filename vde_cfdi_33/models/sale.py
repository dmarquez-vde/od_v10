#Embedded file name: C:\Program Files (x86)\Odoo 10.0\server\odoo\addons\cdfi_invoice\models\sale.py
import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _
from odoo.tools import float_is_zero
from odoo.tools.misc import formatLang
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import odoo.addons.decimal_precision as dp
import amount_to_text_es_MX

class SaleOrder(models.Model):
    _inherit = 'sale.order'
    forma_pago = fields.Selection(selection=[('01', '01 - Efectivo'),
     ('02', '02 - Cheque nominativo'),
     ('03', '03 - Transferencia electr\xc3\xb3nica de fondos'),
     ('04', '04 - Tarjeta de Cr\xc3\xa9dito'),
     ('05', '05 - Monedero electr\xc3\xb3nico'),
     ('06', '06 - Dinero electr\xc3\xb3nico'),
     ('08', '08 - Vales de despensa'),
     ('12', '12 - Daci\xc3\xb3n en pago'),
     ('13', '13 - Pago por subrogaci\xc3\xb3n'),
     ('14', '14 - Pago por consignaci\xc3\xb3n'),
     ('15', '15 - Condonaci\xc3\xb3n'),
     ('17', '17 - Compensaci\xc3\xb3n'),
     ('23', '23 - Novaci\xc3\xb3n'),
     ('24', '24 - Confusi\xc3\xb3n'),
     ('25', '25 - Remisi\xc3\xb3n de deuda'),
     ('26', '26 - Prescripci\xc3\xb3n o caducidad'),
     ('27', '27 - A satisfacci\xc3\xb3n del acreedor'),
     ('28', '28 - Tarjeta de d\xc3\xa9bito'),
     ('29', '29 - Tarjeta de servicios'),
     ('30', '30 - Aplicaci\xc3\xb3n de anticipos'),
     ('99', '99 - Por definir')], string=_('Forma de pago'))
    methodo_pago = fields.Selection(selection=[('PUE', _('Pago en una sola exhibici\xc3\xb3n')), ('PPD', _('Pago en parcialidades o diferido'))], string=_('M\xc3\xa9todo de pago'))
    uso_cfdi = fields.Selection(selection=[('G01', _('Adquisici\xc3\xb3n de mercanc\xc3\xadas')),
     ('G02', _('Devoluciones, descuentos o bonificaciones')),
     ('G03', _('Gastos en general')),
     ('I01', _('Construcciones')),
     ('I02', _('Mobiliario y equipo de oficina por inversiones')),
     ('I03', _('Equipo de transporte')),
     ('I04', _('Equipo de c\xc3\xb3mputo y accesorios')),
     ('I05', _('Dados, troqueles, moldes, matrices y herramental')),
     ('I08', _('Otra maquinaria y equipo')),
     ('D01', _('Honorarios m\xc3\xa9dicos, dentales y gastos hospitalarios')),
     ('D02', _('Gastos m\xc3\xa9dicos por incapacidad o discapacidad')),
     ('D03', _('Gastos funerales')),
     ('D04', _('Donativos')),
     ('D07', _('Primas por seguros de gastos m\xc3\xa9dicos')),
     ('D08', _('Gastos de transportaci\xc3\xb3n escolar obligatoria')),
     ('D10', _('Pagos por servicios educativos (colegiaturas)')),
     ('P01', _('Por definir'))], string=_('Uso CFDI (cliente)'))

    @api.depends('amount_total', 'currency_id')
    @api.one
    def _get_amount_to_text(self):
        self.amount_to_text = amount_to_text_es_MX.get_amount_to_text(self, self.amount_total, 'es_cheque', self.currency_id.name)

    @api.model
    def _get_amount_2_text(self, amount_total):
        return amount_to_text_es_MX.get_amount_to_text(self, amount_total, 'es_cheque', self.currency_id.name)

    @api.multi
    def _prepare_invoice(self):
        invoice_vals = super(SaleOrder, self)._prepare_invoice()
        invoice_vals.update({'forma_pago': self.forma_pago,
         'methodo_pago': self.methodo_pago,
         'uso_cfdi': self.uso_cfdi})
        return invoice_vals
