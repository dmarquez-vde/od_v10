# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 DevIntelle Consulting Service Pvt.Ltd (<http://devintellecs.com>).
#
##############################################################################

from openerp import models, fields, api, _
from openerp.exceptions import ValidationError
import datetime
import odoo.addons.decimal_precision as dp


class advance_payment_line(models.Model):
    _name = 'advance.payment.line'


    invoice_id = fields.Many2one('account.invoice',string='Factura')
    reference = fields.Char('Referencia')
    account_id = fields.Many2one('account.account', string="Cuenta")
    date = fields.Date(string="Fecha Factura")
    due_date = fields.Date(string="Due Date")
    original_amount = fields.Float(string="Monto Factura")
    balance_amount = fields.Float(string="Monto Pendiente al pago")
    full_reconclle = fields.Boolean(string="Pago Completo")
    allocation = fields.Float(string="Monto pago DR")
    allocation1 = fields.Float(string="Monto pago")
    account_payment_id = fields.Many2one('account.payment')
    diff_amt = fields.Float('Saldo Pendiente',compute='get_diff_amount',)
    currency_id = fields.Many2one('res.currency',string='Moneda Factura')
    invoice_currency_id = fields.Many2one('res.currency', related="invoice_id.currency_id")
    payment_type = fields.Selection([
        ('transfer','Transferencia Interna'),
        ('inbound', 'Recibir Dinero'),
        ('outbound', 'Enviar Dinero')
        ],string="Tipo de Pago", related="account_payment_id.payment_type")

    no_de_pago = fields.Integer('No. de pago', readonly=True)
    #saldo_pendiente = fields.Float('Saldo pendiente', readonly=True)
    #monto_pagar = fields.Float('Monto a pagar', compute='_compute_monto_pagar')
    #saldo_restante = fields.Float('Saldo restante', readonly=True, compute='_get_monto_restante', store=True)
    tipocambio = fields.Float(string=_('TC Factura'), digits=dp.get_precision('Currency'))
    tipocambiop = fields.Float(string=_('TC'), compute='_get_tc', store=True, digits=dp.get_precision('Currency'))
    tipocambiop1 = fields.Float(string=_('TC DR'), compute='_get_tc', store=True, digits=dp.get_precision('Currency'))
    tipocambiot= fields.Float('TCT', store=True, compute='_get_tc')

    state = fields.Selection([
        ('draft', 'Borrador'),
        ('posted', 'Contabilizado'),
        ('sent', 'Enviado'),
        ('reconciled', 'Conciliado')
        ], related='account_payment_id.state', store=True)

    folio_fiscal = fields.Char(string="Folio Fiscal", related="account_payment_id.folio_fiscal")

    @api.depends('date', 'currency_id')
    @api.one
    def _get_tc(self):
        #self.amount_to_text = amount_to_text_es_MX.get_amount_to_text(self, self.amount_total, 'es_cheque', self.currency_id.name)
        if self.account_payment_id.payment_date:
            invoice_date_datetime = datetime.datetime.strptime(self.account_payment_id.payment_date, '%Y-%m-%d')
        else:
            today = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
            invoice_date_datetime = datetime.datetime.strptime(today, '%Y-%m-%d %H:%M:%S')
        print "invoice_date_datetime ",invoice_date_datetime
        rate_obj = self.env['res.currency.rate']
        rate_rec = rate_obj.search([
            ('currency_id', '=', self.currency_id.id),
            ('name', '<=', str(invoice_date_datetime))
            # not sure for what purpose 'currency_rate_type_id' field exists in the table, but keep this line just in case
            ], order='name desc', limit=1)
        print "1esto es rate_rec ", rate_rec
        if rate_rec:
            rate = rate_obj.browse(rate_rec.id).rate
        else:
            rate = 1.0
        self.tipocambiop=rate
        rate_rec = rate_obj.search([
            ('currency_id', '=', self.invoice_currency_id.id),
            ('name', '<=', str(invoice_date_datetime))
            # not sure for what purpose 'currency_rate_type_id' field exists in the table, but keep this line just in case
            ], order='name desc', limit=1)
        print "1esto es rate_rec ", rate_rec
        if rate_rec:
            rate = rate_obj.browse(rate_rec.id).rate
        else:
            rate = 1.0
        self.tipocambiop1=rate

        if self.tipocambiop1==1.000000000000:
            self.tipocambiot=self.tipocambiop1
        else:
            variable = str(self.tipocambiop1)
            startLoc = 0
            endLoc = 8
            variable = variable[startLoc: endLoc]
            variable = float(variable)
            self.tipocambiot = variable
            self.tipocambiot = self.tipocambiot + 0.000001

    @api.multi
    @api.depends('balance_amount','allocation')
    def get_diff_amount(self):
        for line in self:
            line.diff_amt = line.balance_amount - line.allocation

    #@api.onchange('full_reconclle')
    #def onchange_full_reconclle(self):
        #if self.full_reconclle:
            #self.allocation = self.balance_amount
        #else:
            #self.allocation = 0.0

    #@api.onchange('allocation')
    #def onchange_allocation(self):
        #if self.allocation:
            #if self.allocation >= self.balance_amount:
                #self.full_reconclle = True
            #else:
                #self.full_reconclle = False
            #self.diff_amt = self.balance_amount - self.allocation



