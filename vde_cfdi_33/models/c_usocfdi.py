# -*- coding: utf-8 -*-
from openerp import fields,models,api,_
from datetime import date
import openerp.addons.decimal_precision as dp


class c_usocfdi (models.Model):

    _name='c.usocfdi'

    code = fields.Char(string='Uso CFDI',  required=True)
    name = fields.Char(string='Descripcion',  required=True)
    fisica = fields.Selection([('1','SI'),('2','NO')], string='Tipo Persona',  required=True)
    moral = fields.Selection([('1','SI'),('2','NO')], string='Tipo Persona',  required=True)
    start_life = fields.Date(string='Inicio Vigencia',  required=True)
    end_life = fields.Date(string='Fin Vigencia')
