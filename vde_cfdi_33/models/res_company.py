from odoo import fields, models, api, _

class ResCompany(models.Model):
    _inherit = 'res.company'
    rfc = fields.Char(string=_('RFC'))
    api_key = fields.Char(string=_('API Key'))
    http_factura = fields.Char(string=_('WS URL invoice'))
    http_factura2 = fields.Char(string=_('WS URL payment'))
    factura_dir = fields.Char(string=_('Directorio XML'))
    factura_dir2 = fields.Char(string=_('Directorio XML'))
    modo_prueba = fields.Boolean(string=_('Modo prueba'))
    serie_factura = fields.Char(string=_('Serie factura'))
    regimen_fiscal = fields.Many2one('c.regimenfiscal', string="Regimen Fiscal")
    #regimen_fiscal = fields.Selection(selection=[('601', _('General de Ley Personas Morales')),
     #('603', _('Personas Morales con Fines no Lucrativos')),
     #('605', _('Sueldos y Salarios e Ingresos Asimilados a Salarios')),
     #('606', _('Arrendamiento')),
     #('608', _('Dem\xc3\xa1s ingresos')),
     #('609', _('Consolidaci\xc3\xb3n')),
     #('610', _('Residentes en el Extranjero sin Establecimiento Permanente en M\xc3\xa9xico')),
     #('611', _('Ingresos por Dividendos (socios y accionistas)')),
     #('612', _('Personas F\xc3\xadsicas con Actividades Empresariales y Profesionales')),
     #('614', _('Ingresos por intereses')),
     #('616', _('Sin obligaciones fiscales')),
     #('620', _('Sociedades Cooperativas de Producci\xc3\xb3n que optan por diferir sus ingresos')),
     #('621', _('Incorporaci\xc3\xb3n Fiscal')),
     #('622', _('Actividades Agr\xc3\xadcolas, Ganaderas, Silv\xc3\xadcolas y Pesqueras')),
     #('623', _('Opcional para Grupos de Sociedades')),
     #('624', _('Coordinados')),
     #('628', _('Hidrocarburos')),
     #('607', _('R\xc3\xa9gimen de Enajenaci\xc3\xb3n o Adquisici\xc3\xb3n de Bienes')),
     #('629', _('De los Reg\xc3\xadmenes Fiscales Preferentes y de las Empresas Multinacionales')),
     #('630', _('Enajenaci\xc3\xb3n de acciones en bolsa de valores')),
     #('615', _('R\xc3\xa9gimen de los ingresos por obtenci\xc3\xb3n de premios'))], string=_('R\xc3\xa9gimen Fiscal'))
    archivo_cer = fields.Binary(string=_('Archivo .cer'))
    archivo_key = fields.Binary(string=_('Archivo .key'))
    contrasena = fields.Char(string=_('Contrase\xc3\xb1a'))
    nombre_fiscal = fields.Char(string=_('Raz\xc3\xb3n social'))
    serie_complemento = fields.Char(string=_('Serie complemento de pago'))
    numero_certificado = fields.Char(string=_('Numero de Certificado'))
    ip = fields.Char(string="Local IP Address")
    #mostrador_id = fields.Many2one('res.partner', string="Cliente mostrador")
