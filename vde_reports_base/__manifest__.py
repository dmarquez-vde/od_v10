# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2018 Vos Datos Enterprise Suite S.A. de C.V.
#    (<https://vde-suite.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'VDE Reports BASE',
    'version': '10.0.2.0.0',
    'summary': "Export Excel Report Base",
    'category': 'Reports',
    'author': 'VDE-Suite',
    'company': 'VDE-Suite',
    'website': 'http://www.vde-suite.com',
    'depends': [
                'base',
                'account',
                'report_xlsx'
                ],
    'data': [
            'views/menu.xml',
            ],
    'images': ['static/description/banner.jpg'],
    'license': "AGPL-3",
    'installable': True,
    'auto_install': False,
}
