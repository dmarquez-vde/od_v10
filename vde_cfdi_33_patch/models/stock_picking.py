# -*- coding: utf-8 -*-
from odoo import api, fields, models

class stock_picking(models.Model):
    _inherit = 'stock.picking'

    def _get_invoice_vals(self, key, inv_type, journal_id, move):
        partner, currency_id, company_id, user_id = key
        if inv_type in ('out_invoice', 'out_refund'):
            account_id = partner.property_account_receivable_id.id
            payment_term = partner.property_payment_term_id.id or False
        else:
            account_id = partner.property_account_payable_id.id
            payment_term = partner.property_supplier_payment_term_id.id or False
        return {
            'origin': move.picking_id.name,
            'date_invoice': self._context.get('date_inv', False),
            'user_id': user_id,
            'partner_id': partner.id,
            'account_id': account_id,
            'payment_term_id': payment_term,
            'type': inv_type,
            'fiscal_position_id': partner.property_account_position_id.id,
            'company_id': company_id,
            'currency_id': currency_id,
            'journal_id': journal_id,
            'uso_cfdi':partner.uso_cfdi.id or False,
            'forma_pago': partner.forma_pago.id or False,
            'methodo_pago': partner.methodo_pago.id or False,
            'tipo_comprobante': 'I'
        }