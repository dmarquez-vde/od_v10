# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields
from openerp.tools.translate import _
from odoo.exceptions import UserError, ValidationError


JOURNAL_TYPE_MAP = {
    ('outgoing', 'customer'): ['sale'],
    ('outgoing', 'supplier'): ['purchase_refund'],
    ('outgoing', 'transit'): ['sale', 'purchase_refund'],
    ('incoming', 'supplier'): ['purchase'],
    ('incoming', 'customer'): ['sale_refund'],
    ('incoming', 'transit'): ['purchase', 'sale_refund'],
}


class stock_invoice_onshipping(models.TransientModel):

    _name = "stock.invoice.onshipping"
    _description = "Stock Invoice Onshipping"

    def _get_currency(self):
        so_obj = self.env['sale.order']
        sp_obj = self.env['stock.picking']
        sp_id = sp_obj.browse(self.env.context.get('active_id'))
        so_id = so_obj.search([('name','=',sp_id.origin)])
        return so_id.pricelist_id.currency_id.id

    def _get_journal(self):
        journal_obj = self.env['account.journal']
        journal_type = self._get_journal_type()
        self._cr.execute("select * from pg_tables where schemaname='public' and tablename='wizard_l10n_pt_saft'")
        aux=self._cr.fetchone()
        if aux!=None and aux[0]!=None:
            if journal_type == 'sale_refund':
                x = [('type', '=', 'sale'),('por_defeito', '=', True)]
            elif journal_type == 'purchase_refund':
                x = [('type', '=', 'purchase'),('por_defeito', '=', True)]
            else:
                x = [('type', '=', journal_type),('por_defeito', '=', True)]
        else:
            if journal_type == 'sale_refund':
                x = [('type', '=', 'sale')]
            elif journal_type == 'purchase_refund':
                x = [('type', '=', 'purchase')]
            else:
                x = [('type', '=', journal_type)]

        journals = journal_obj.search(x)
        return journals and journals[0] or False

    def _get_journal_type(self):
        res_ids = self._context and self._context.get('active_ids', [])
        pick_obj = self.env['stock.picking']
        pickings = pick_obj.browse(res_ids)
        pick = pickings and pickings[0]
        if not pick or not pick.move_lines:
            return 'sale'
        type = pick.picking_type_id.code
        usage = pick.move_lines[0].location_id.usage if type == 'incoming' else pick.move_lines[0].location_dest_id.usage
        return JOURNAL_TYPE_MAP.get((type, usage), ['sale'])[0]

    journal_id = fields.Many2one('account.journal', 'Destination Journal', required=True, default=_get_journal)
    journal_type = fields.Selection([('purchase_refund', 'Refund Purchase'), ('purchase', 'Create Supplier Invoice'), ('sale_refund', 'Refund Sale'), ('sale', 'Create Customer Invoice')],
                                      'Journal Type', readonly=True, default=_get_journal_type)
    group = fields.Boolean("Group by partner")
    invoice_date = fields.Date('Invoice Date')
    currency_id = fields.Many2one('res.currency', string="Moneda", default=_get_currency)

    def onchange_journal_id(self, journal_id):

        domain = {}
        value = {}
        active_id = self._context.get('active_id')
        if active_id:
            picking = self.env['stock.picking'].browse(active_id)
            type = picking.picking_type_id.code
            usage = picking.move_lines[0].location_id.usage if type == 'incoming' else picking.move_lines[0].location_dest_id.usage
            journal_types = JOURNAL_TYPE_MAP.get((type, usage), ['sale', 'purchase', 'sale_refund', 'purchase_refund'])
            domain['journal_id'] = [('type', 'in', journal_types)]
        if journal_id:
            journal = self.env['account.journal'].browse(journal_id)
            value['journal_type'] = journal.type
        return {'value': value, 'domain': domain}

    def view_init(self, fields_list):

        res = super(stock_invoice_onshipping, self).view_init(fields_list)
        pick_obj = self.env['stock.picking']
        count = 0
        active_ids = self._context.get('active_ids',[])
        for pick in pick_obj.browse(active_ids):
            if pick.invoice_state != '2binvoiced':
                count += 1
        if len(active_ids) == count:
            raise ValidationError(_('None of these picking lists require invoicing.'))
        return res

    def open_invoice(self):

        invoice_ids = self.create_invoice()
        if not invoice_ids:
            raise ValidationError(_('No invoice created!'))
        else:
            for invoice_id in invoice_ids:
                self.env['account.invoice'].browse(invoice_id).write({'currency_id': self.currency_id.id})
        data = self.browse()
        action_model = False
        action = {}

        journal2type = {'sale':'out_invoice', 'purchase':'in_invoice' , 'sale_refund':'out_refund', 'purchase_refund':'in_refund'}
        inv_type = journal2type.get(data.journal_type) or 'out_invoice'
        data_pool = self.env['ir.model.data']
        if inv_type == "out_invoice":
            action_id = data_pool.xmlid_to_res_id('account.action_invoice_tree1')
        elif inv_type == "in_invoice":
            action_id = data_pool.xmlid_to_res_id('account.action_invoice_tree2')
        elif inv_type == "out_refund":
            action_id = data_pool.xmlid_to_res_id('account.action_invoice_tree1')
        elif inv_type == "in_refund":
            action_id = data_pool.xmlid_to_res_id('account.action_invoice_tree2')


        if action_id:
            action_pool = self.env['ir.actions.act_window']
            action = action_pool.sudo().browse(action_id)
            #action[0]['domain'] = "[('id','in', "+str(invoice_ids)+")]"
            dummy, form_view = self.env['ir.model.data'].get_object_reference('account', 'invoice_form')
            dummy, tree_view = self.env['ir.model.data'].get_object_reference('account', 'invoice_tree')
            return {
                      'name': 'Invoice',
                      'view_type': 'form',
                      'view_mode': 'tree,form',
                      'res_model': 'account.invoice',
                      'view_id': False,
                      'views': [(tree_view or False, 'tree'),
                                (form_view or False, 'form'),
                                (False, 'calendar'), (False, 'graph')],
                     'type': 'ir.actions.act_window',
                     'domain':[('id','in', invoice_ids)],
                 }
        return True

    def create_invoice(self):
        context = dict(self._context or {})
        picking_pool = self.env['stock.picking']
        data = self.browse(self._ids[0])
        journal2type = {'sale':'out_invoice', 'purchase':'in_invoice', 'sale_refund':'out_refund', 'purchase_refund':'in_refund'}
        context['date_inv'] = data.invoice_date
        acc_journal = self.env["account.journal"]
        inv_type = journal2type.get(data.journal_type) or 'out_invoice'
        context['inv_type'] = inv_type

        active_ids = context.get('active_ids', [])

        res = picking_pool.action_invoice_create(active_ids, journal_id = data.journal_id.id, group = data.group, type = inv_type)#self._cr, self._uid, , context=self._context
        return res
