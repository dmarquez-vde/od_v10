# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from datetime import datetime, timedelta
from psycopg2 import OperationalError

from dateutil.relativedelta import relativedelta

#----------------------------------------------------------
# Move
#----------------------------------------------------------

class stock_move(models.Model):
    _inherit = "stock.move"


    invoice_state = fields.Selection([("invoiced", "Invoiced"),("2binvoiced", "To Be Invoiced"),("none", "Not Applicable")], "Invoice Control",
                     select=True, required=True, track_visibility='onchange',states={'draft': [('readonly', False)]}, default = 'none')

    def create(self, vals):
    ### check if origin is sale order ####
        entrou_n_origin=0
        if 'origin' in vals and vals['origin']!=None:
            entrou=0
            entrou_n_origin=1
            so= self.env['sale.order'].search([('name', '=', vals['origin'])])
            for obj_so in so:
                entrou=1
                if obj_so.order_policy=='picking':
                    vals['invoice_state']='2binvoiced'
                else:
                    vals['invoice_state']='none'

            if entrou==0:
                if 'partner_id' in vals and vals['partner_id']!=None:
                    partner= self.env['res.partner'].search([('id', '=', vals['partner_id'])])
                    for obj_partner in self.env['res.partner'].browse(partner):
                        if obj_partner.order_policy=='picking':
                            vals['invoice_state']='2binvoiced'
                        else:
                            vals['invoice_state']='none'

        ### verificar se existe o certification instalado ###
        self._cr.execute("select * from pg_tables where schemaname='public' and tablename='wizard_l10n_pt_saft'")
        aux=self._cr.fetchone()
        if aux!=None and aux[0]!=None:
            if entrou_n_origin==0:
                if 'inv_line_id' in vals and vals['inv_line_id']!=None:
                    ### verificar se já existe NC ####
                        if 'picking_id' in vals and vals['picking_id']!=None:
                            for obj_pick in self.env['stock.picking'].browse(vals['picking_id']):

                                if obj_pick.is_gd==True:
                                    entrou_gd=0
                                    inv= self.env['account.invoice'].search([('origin', '=', obj_pick.origin),('type', '=', 'out_refund')])
                                    for obj_inv in self.env['account.invoice'].browse(inv):
                                        entrou_gd=1

                                        vals['invoice_state']='none'

                                    if entrou_gd==0:
                                        vals['invoice_state']='2binvoiced'


                                    ########## se criar fatura da GD ##########
                                    self._cr.execute("select order_line_id from sale_order_line_invoice_rel where invoice_line_id="+str(vals['inv_line_id']))
                                    order_line_id=self._cr.fetchone()

                                    if order_line_id!=None and order_line_id[0]!=None:
                                        ######## atualizar qnt delivered ########
                                        sol= self.env['sale.order.line'].search( [('id', '=', order_line_id[0])])
                                        for obj_sol in self.env['sale.order.line'].browse(sol):
                                            delivered= obj_sol.qty_delivered
                                            total= delivered-vals['product_uom_qty']
                                            self.env['sale.order.line'].write(order_line_id[0], {'qty_delivered': total})

        id = super(stock_move, self).create(vals)
        return id


    def _get_master_data(self, move, company):
        ''' returns a tuple (browse_record(res.partner), ID(res.users), ID(res.currency)'''
        currency = company.currency_id.id
        partner = move.picking_id and move.picking_id.partner_id
        if partner:
            code = self.get_code_from_locs(move)
            if partner.property_product_pricelist and code == 'outgoing':
                currency = partner.property_product_pricelist.currency_id.id
        return partner, self._uid, currency

    def get_code_from_locs(self, move):
        """
        Returns the code the picking type should have.  This can easily be used
        to check if a move is internal or not
        move, location_id and location_dest_id are browse records
        """
        location_id = False
        location_dest_id = False
        code = 'internal'
        src_loc = location_id or move.location_id
        dest_loc = location_dest_id or move.location_dest_id
        if src_loc.usage == 'internal' and dest_loc.usage != 'internal':
            code = 'outgoing'
        if src_loc.usage != 'internal' and dest_loc.usage == 'internal':
            code = 'incoming'
        return code

    def _create_invoice_line_from_vals(self, move, invoice_line_vals):
        return self.env['account.invoice.line'].create(invoice_line_vals)

    def _get_price_unit_invoice(self, move_line, type):
        """ Gets price unit for invoice
        @param move_line: Stock move lines
        @param type: Type of invoice
        @return: The price unit for the move line
        """

        if type in ('in_invoice', 'in_refund'):
            return move_line.price_unit
        else:
            # If partner given, search price in its sale pricelist
            if move_line.partner_id and move_line.partner_id.property_product_pricelist:
                pricelist_obj = self.env["product.pricelist"]
                pricelist = move_line.partner_id.property_product_pricelist.id
                price = pricelist_obj.price_get(move_line.product_id.id, move_line.product_uom_qty, move_line.partner_id.id)

                if price:
                    return price
        return move_line.product_id.list_price

    def _get_invoice_line_vals(self, move, partner, inv_type):
        fp_obj = self.env['account.fiscal.position']
        # Get account_id
        if inv_type in ('out_invoice', 'out_refund'):
            account_id = move.product_id.property_account_income_id.id
            if not account_id:
                account_id = move.product_id.categ_id.property_account_income_categ_id.id
        else:
            account_id = move.product_id.property_account_expense_id.id
            if not account_id:
                account_id = move.product_id.categ_id.property_account_expense_categ_id.id
        fiscal_position = partner.property_account_position_id
        account_id = fp_obj.map_account(account_id)

        # set UoS if it's a sale and the picking doesn't have one
        uom_id = move.product_uom.id
        quantity = move.product_uom_qty
        if move.product_uom:
            uom_id = move.product_uom.id
            quantity = move.product_uom_qty

        if move.procurement_id.sale_line_id.tax_id:
            taxes_ids = [tax.id for tax in move.procurement_id.sale_line_id.tax_id]

            return {
                'name': move.name,
                'account_id': account_id,
                'product_id': move.product_id.id,
                'uom_id': uom_id,
                'quantity': quantity,
                'price_unit': self._get_price_unit_invoice(move, inv_type),
                'invoice_line_tax_ids': [(6, 0, taxes_ids)],
                'discount': 0.0,
                'account_analytic_id': False,
            }

        else:

            return {
                'name': move.name,
                'account_id': account_id,
                'product_id': move.product_id.id,
                'uom_id': uom_id,
                'quantity': quantity,
                'price_unit': self._get_price_unit_invoice(move, inv_type),
                'discount': 0.0,
                'account_analytic_id': False,
            }


    def _get_moves_taxes(self, moves, inv_type):
        #extra moves with the same picking_id and product_id of a move have the same taxes
        extra_move_tax = {}
        is_extra_move = {}
        for move in moves:
            if move.picking_id:
                is_extra_move[move.id] = True
                if not (move.picking_id, move.product_id) in extra_move_tax:
                    extra_move_tax[move.picking_id, move.product_id] = 0
            else:
                is_extra_move[move.id] = False
        return (is_extra_move, extra_move_tax)

#----------------------------------------------------------
# Picking
#----------------------------------------------------------

class stock_picking(models.Model):
    _inherit = 'stock.picking'

    def __get_invoice_state(self):
        result = {}
        for pick in self:
            result[pick.id] = 'none'
            for move in pick.move_lines:
                if move.invoice_state == 'invoiced':
                    result[pick.id] = 'invoiced'
                elif move.invoice_state == '2binvoiced':
                    result[pick.id] = '2binvoiced'
                    break
        return result

    def __get_picking_move(self):
        res = []
        for move in self.self.env['stock.move'].browse():
            if move.picking_id and move.invoice_state != move.picking_id.invoice_state:
                res.append(move.picking_id.id)
        return res

    def _set_inv_state(self, picking_id, name, value):
        pick = self.browse(picking_id)
        moves = [x.id for x in pick.move_lines]
        move_obj= self.env["stock.move"]
        move_obj.write(moves, {'invoice_state': value})

    invoice_state = fields.Selection([("invoiced", "Invoiced"),("2binvoiced", "To Be Invoiced"),("none", "Not Applicable")],
    string="Invoice Control", required=True, default=__get_invoice_state)


    def _create_invoice_from_picking(self, picking, vals):
        ''' This function simply creates the invoice from the given values. It is overriden in delivery module to add the delivery costs.
        '''
        invoice_obj = self.env['account.invoice']
        return invoice_obj.create(vals)

    def _get_partner_to_invoice(self, picking):
        """ Gets the partner that will be invoiced
            Note that this function is inherited in the sale and purchase modules
            @param picking: object of the picking for which we are selecting the partner to invoice
            @return: object of the partner to invoice
        """
        return picking.partner_id and picking.partner_id.id

    def action_invoice_create(self, ids, journal_id, group=False, type='out_invoice'):
        """ Creates invoice based on the invoice state selected for picking.
        @param journal_id: Id of journal
        @param group: Whether to create a group invoice or not
        @param type: Type invoice to be created
        @return: Ids of created invoices for the pickings
        """
        todo = {}
        res_ids = self._context and self._context.get('active_ids', [])
        pick_obj = self.env['stock.picking']
        pickings = pick_obj.browse(res_ids)

        for picking in pickings:
            partner = self._get_partner_to_invoice(picking)
            #grouping is based on the invoiced partner
            if group:
                key = partner
            else:
                key = picking.id
            for move in picking.move_lines:
                if move.invoice_state == '2binvoiced':
                    if (move.state != 'cancel') and not move.scrapped:
                        todo.setdefault(key, [])
                        todo[key].append(move)
        invoices = []

        for moves in todo.values():
            invoices += self._invoice_create_line(moves, journal_id, type)
            ###############até aqui esta a bater certo, igual a v9###################

        return invoices

    def _get_invoice_vals(self, key, inv_type, journal_id, move):
        partner, currency_id, company_id, user_id = key
        if inv_type in ('out_invoice', 'out_refund'):
            account_id = partner.property_account_receivable_id.id
            payment_term = partner.property_payment_term_id.id or False
        else:
            account_id = partner.property_account_payable_id.id
            payment_term = partner.property_supplier_payment_term_id.id or False
        return {
            'origin': move.picking_id.name,
            'date_invoice': self._context.get('date_inv', False),
            'user_id': user_id,
            'partner_id': partner.id,
            'account_id': account_id,
            'payment_term_id': payment_term,
            'type': inv_type,
            'fiscal_position_id': partner.property_account_position_id.id,
            'company_id': company_id,
            'currency_id': currency_id,
            'journal_id': journal_id,
        }

    def _invoice_create_line(self, moves, journal_id, inv_type='out_invoice'):
        invoice_obj = self.env['account.invoice']
        move_obj = self.env['stock.move']
        invoices = {}
        is_extra_move, extra_move_tax = move_obj._get_moves_taxes(moves, inv_type)
        product_price_unit = {}
        moves_tratados=[]
        for move in moves:
            guia= self.env['stock.picking'].search([('origin', '=', move.picking_id.name),('state', '=', 'done')])
            if guia:
                for obj_guia in self.env['stock.picking'].browse(guia):
                    if obj_guia.picking_type_id.code=='incoming':
                        for move_gi in obj_guia.move_lines:
                            if move_gi.product_id.id==move.product_id.id:
                                if move.product_qty!=move_gi.product_qty:
                                    if move not in moves_tratados:
                                        moves_tratados.append(move)
            else:
                moves_tratados.append(move)

        for move in moves_tratados:
            company = move.company_id
            origin = move.picking_id.name
            partner, user_id, currency_id = move_obj._get_master_data(move, company)

            key = (partner, currency_id, company.id, user_id)
            invoice_vals = self._get_invoice_vals(key, inv_type, journal_id, move)

            if key not in invoices:
                # Get account and payment terms
                invoice_id = self._create_invoice_from_picking(move.picking_id, invoice_vals).id
                invoices[key] = invoice_id
            else:
                invoice = invoice_obj.browse(invoices[key])
                merge_vals = {}
                if not invoice.origin or invoice_vals['origin'] not in invoice.origin.split(', '):
                    invoice_origin = filter(None, [invoice.origin, invoice_vals['origin']])
                    merge_vals['origin'] = ', '.join(invoice_origin)
                if invoice_vals.get('name', False) and (not invoice.name or invoice_vals['name'] not in invoice.name.split(', ')):
                    invoice_name = filter(None, [invoice.name, invoice_vals['name']])
                    merge_vals['name'] = ', '.join(invoice_name)
                if merge_vals:
                    invoice.write(merge_vals)

            invoice_line_vals = move_obj._get_invoice_line_vals(move, partner, inv_type)#cr, uid, , context=context
            invoice_line_vals['invoice_id'] = invoices[key]
            invoice_line_vals['origin'] = origin
            # if not is_extra_move[move.id]:
            #     product_price_unit[invoice_line_vals['product_id'], invoice_line_vals['uom_id']] = invoice_line_vals['price_unit']
            # if is_extra_move[move.id] and (invoice_line_vals['product_id'], invoice_line_vals['uom_id']) in product_price_unit:
            #     invoice_line_vals['price_unit'] = product_price_unit[invoice_line_vals['product_id'], invoice_line_vals['uom_id']]
    	    # if move.sale_line_id and move.sale_line_id.price_unit:
            if move.procurement_id.sale_line_id and move.procurement_id.sale_line_id.price_unit:
    	    	invoice_line_vals['price_unit'] = move.procurement_id.sale_line_id.price_unit
    	    else:
    	    	invoice_line_vals['price_unit'] = move.price_unit
            if is_extra_move[move.id]:
                desc = (inv_type in ('out_invoice', 'out_refund') and move.product_id.product_tmpl_id.description_sale) or \
                    (inv_type in ('in_invoice','in_refund') and move.product_id.product_tmpl_id.description_purchase)
                invoice_line_vals['name'] += ' ' + desc if desc else ''
                if extra_move_tax[move.picking_id, move.product_id]:
                    invoice_line_vals['invoice_line_tax_ids'] = extra_move_tax[move.picking_id, move.product_id]
                #the default product taxes
                elif (0, move.product_id) in extra_move_tax:
                    invoice_line_vals['invoice_line_tax_ids'] = extra_move_tax[0, move.product_id]

            move_obj._create_invoice_line_from_vals(move, invoice_line_vals)#cr, uid, , context=context
            move_obj.write({'invoice_state': 'invoiced'})

            ##########################################################################
            ############### retornar qnt faturada para a so se existir ###############
            ##########################################################################
            # # Update delivered quantities on sale order lines
            sale_line_id= move.procurement_id.sale_line_id.id
            product_id= move.product_id.id
            qty= move.product_qty
            if sale_line_id:
                ft_line= self.env['account.invoice.line'].search([('invoice_id', '=', invoices.values()[0]),('product_id', '=', product_id),('quantity', '=', qty)])
                for inv_line in ft_line:
                    id_sale_order_line = self.env['sale.order.line'].browse(sale_line_id)

                    ######## preencher campo many2many do invoice_line na tabela da so line ########
                    id_sale_order_line.write({'invoice_lines': [(4, inv_line.id)]})

            else:
                #### se não existir a linha da so no move ####
                ########## se criar fatura da GD ##########
                pick= self.env['stock.picking'].search([('id', '=', move.picking_id.id)])
                for obj_pick in pick:
                    origin= obj_pick.origin

                    inv= self.env['account.invoice'].search([('internal_number', '=', origin)])
                    for obj_inv in inv:

                        ft_line= self.env['account.invoice.line'].search([('invoice_id', '=', obj_inv.id),('product_id', '=', product_id),('quantity', '=', qty)])
                        for inv_line in ft_line:

                            self.cr.execute("select order_line_id from sale_order_line_invoice_rel where invoice_line_id="+str(inv_line.id))
                            order_line_id=self._cr.fetchone()

                            if order_line_id!=None and order_line_id[0]!=None:
                                ft_line= self.env['account.invoice.line'].search([('invoice_id', '=', invoices.values()[0]),('product_id', '=', product_id),('quantity', '=', qty)])
                                for inv_line in ft_line:
                                    id_sale_order_line = self.env['sale.order.line'].browse(order_line_id[0])
                                    ######## preencher campo many2many do invoice_line na tabela da so line ########
                                    id_sale_order_line.write({'invoice_lines': [(4, inv_line.id)]})

        invoice = invoice_obj.browse(invoices.values())
        invoice.compute_taxes()
        return invoices.values()

    def _prepare_values_extra_move(self, op, product, remaining_qty):
        """
        Need to pass invoice_state of picking when an extra move is created which is not a copy of a previous
        """
        res = super(stock_picking, self)._prepare_values_extra_move(op, product, remaining_qty)
        res.update({'invoice_state': op.picking_id.invoice_state})
        if op.linked_move_operation_ids:
            res.update({'price_unit': op.linked_move_operation_ids[-1].move_id.price_unit})
        return res


#----------------------------------------------------------
# INVOICE
#----------------------------------------------------------

class account_invoice(models.Model):
    _inherit = "account.invoice"

class sale_order_inherit(models.Model):
    _inherit = "sale.order"

    order_policy = fields.Selection([('picking', 'On Delivery Order'),('prepaid', 'Before Delivery'),], 'Create Invoice', required=True,
        readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        help="""On delivery order: A draft invoice can be created from the delivery order when the products have been delivered. \nBefore delivery: A draft invoice is created from the sales order and must be paid before the products can be delivered.""",
        default = 'picking')

class res_partner_inherit(models.Model):
    _inherit = "res.partner"

    order_policy = fields.Selection([('picking', 'On Delivery Order'),('prepaid', 'Before Delivery'),], 'Create Invoice',
        help="""On delivery order: A draft invoice can be created from the delivery order when the products have been delivered. \nBefore delivery: A draft invoice is created from the sales order and must be paid before the products can be delivered.""",
        default = 'picking')


class ProcurementOrder(models.Model):

    _inherit='procurement.order'

    @api.multi
    def _run(self):
        #print "entra _run"
        if self.rule_id.action == 'move':
            if not self.rule_id.location_src_id:
                self.message_post(body=_('No source location defined!'))
                return False
            # create the move as SUPERUSER because the current user may not have the rights to do it (mto product launched by a sale for example)
            self.env['stock.move'].sudo().create(self.with_context(sale=True)._get_stock_move_values())
            #print "despues de sudo.create"
            return True
        return super(ProcurementOrder, self)._run()


    def _get_stock_move_values(self):
        ''' Returns a dictionary of values that will be used to create a stock move from a procurement.
        This function assumes that the given procurement has a rule (action == 'move') set on it.

        :param procurement: browse record
        :rtype: dictionary
        '''
        group_id = False
        if self.rule_id.group_propagation_option == 'propagate':
            group_id = self.group_id.id
        elif self.rule_id.group_propagation_option == 'fixed':
            group_id = self.rule_id.group_id.id
        date_expected = (datetime.strptime(self.date_planned, DEFAULT_SERVER_DATETIME_FORMAT) - relativedelta(days=self.rule_id.delay or 0)).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        # it is possible that we've already got some move done, so check for the done qty and create
        # a new move with the correct qty
        qty_done = sum(self.move_ids.filtered(lambda move: move.state == 'done').mapped('product_uom_qty'))
        qty_left = max(self.product_qty - qty_done, 0)
        #print "esto es context ", self.env.context
        if not self.env.context.get('sale'):
            return {
                'name': self.name[:2000],
                'company_id': self.rule_id.company_id.id or self.rule_id.location_src_id.company_id.id or self.rule_id.location_id.company_id.id or self.company_id.id,
                'product_id': self.product_id.id,
                'product_uom': self.product_uom.id,
                'product_uom_qty': qty_left,
                'partner_id': self.rule_id.partner_address_id.id or (self.group_id and self.group_id.partner_id.id) or False,
                'location_id': self.rule_id.location_src_id.id,
                'location_dest_id': self.location_id.id,
                'move_dest_id': self.move_dest_id and self.move_dest_id.id or False,
                'procurement_id': self.id,
                'rule_id': self.rule_id.id,
                'procure_method': self.rule_id.procure_method,
                'origin': self.origin,
                'picking_type_id': self.rule_id.picking_type_id.id,
                'group_id': group_id,
                'route_ids': [(4, route.id) for route in self.route_ids],
                'warehouse_id': self.rule_id.propagate_warehouse_id.id or self.rule_id.warehouse_id.id,
                'date': date_expected,
                'date_expected': date_expected,
                'propagate': self.rule_id.propagate,
                'priority': self.priority,
            }
        else:
            estado=''
            id_so=self.env['sale.order'].search([('name','=',self.origin)])
            #print "esto es id_so ", id_so.id
            so=self.env['sale.order'].browse(id_so.id)
            if so.order_policy == 'picking':
                estado = '2binvoiced'
            elif so.order_policy=='prepaid':
                estado = 'invoiced'
            else:
                estado = 'none'
            print "esto es so ", estado
            return {
                'name': self.name[:2000],
                'company_id': self.rule_id.company_id.id or self.rule_id.location_src_id.company_id.id or self.rule_id.location_id.company_id.id or self.company_id.id,
                'product_id': self.product_id.id,
                'product_uom': self.product_uom.id,
                'product_uom_qty': qty_left,
                'partner_id': self.rule_id.partner_address_id.id or (self.group_id and self.group_id.partner_id.id) or False,
                #'location_id': so.location_id.id or self.rule_id.location_src_id.id,
                'location_id': self.rule_id.location_src_id.id,
                'location_dest_id': self.location_id.id,
                'move_dest_id': self.move_dest_id and self.move_dest_id.id or False,
                'procurement_id': self.id,
                'rule_id': self.rule_id.id,
                'procure_method': self.rule_id.procure_method,
                'origin': self.origin,
                'picking_type_id': self.rule_id.picking_type_id.id,
                'group_id': group_id,
                'route_ids': [(4, route.id) for route in self.route_ids],
                'warehouse_id': self.rule_id.propagate_warehouse_id.id or self.rule_id.warehouse_id.id,
                'date': date_expected,
                'date_expected': date_expected,
                'propagate': self.rule_id.propagate,
                'priority': self.priority,
                'invoice_state': estado,
            }
