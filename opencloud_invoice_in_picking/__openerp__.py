# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Invoice in Picking',
    'version': '1.0',
    'author': 'Opencloud',
    'license': 'LGPL-3',
    'summary': 'Generate invoices based on Picking',
    'description': """  """,
    'website': "http://www.opencloud.pro",
    'depends': ['stock', 'account', 'sale'],
    'category': 'Tools',
    'sequence': 16,
    'demo': [],
    'data': [
        'wizard/stock_invoice_onshipping_view.xml',
        'stock_account_view.xml',
    ],
    'test': [],
    'installable': True,
    'auto_install': False,
    'images': ['images/imagem_princi_inv_picking.png'],
    'price': 48.00,
    'currency': 'EUR',
}
