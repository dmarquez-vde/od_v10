# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.tools import float_compare
from odoo.exceptions import UserError
import datetime

class AccountMove(models.Model):
    _inherit = "account.move"

    @api.multi
    def ajuste_cambiario(self, pago):
        print "esto es context desde ajuste_cambiario ", self.env.context
        pago = self.env['account.payment'].browse(pago)
        for rec in self:
            move_lines = self.env['account.move.line'].search([('move_id','=',self.id),('payment_id','=',pago.id)])
            print "esto es move_lines ",move_lines
            sum_d = 0
            sum_c = 0
            dif = 0
            for line in move_lines:
                print "esto es line ", line
                sum_c = sum_c + line.credit
                sum_d = sum_d + line.debit
            if pago.payment_type == 'inbound':
                dif = round(sum_d - sum_c, 2)
                if dif > 0:
                    vals = {
                        'name': _('Currency exchange rate difference'),
                        'debit': 0.0,
                        'credit':  dif,
                        'account_id':self.company_id.currency_exchange_journal_id.default_credit_account_id.id,
                        'move_id': self.id,
                        'payment_id': pago.id,
                        'currency_id': pago.journal_id.currency_id.id,
                        #'amount_currency': aml.amount_residual_currency,
                        #'amount_currency': signal > 0 and monto_moneda_cambiaria or signal <0 and -monto_moneda_cambiaria or 0.0,
                        'amount_currency': 0.0,
                        'partner_id': pago.partner_id.id,
                        'date_maturity': datetime.datetime.today().strftime('%Y-%m-%d'),
                        'invoice_idd': self.env.context.get('invoice_id'),
                        }
                    print "esto es vals ", vals
                    move_id = self.env['account.move.line'].create(vals)
                    if move_id:
                        return True
                elif dif < 0:
                    vals = {
                        'name': _('Currency exchange rate difference'),
                        'debit': abs(dif),
                        'credit':  0.0,
                        'account_id':self.company_id.currency_exchange_journal_id.default_debit_account_id.id,
                        'move_id': self.id,
                        'payment_id': pago.id,
                        'currency_id': pago.journal_id.currency_id.id,
                        #'amount_currency': aml.amount_residual_currency,
                        #'amount_currency': signal > 0 and monto_moneda_cambiaria or signal <0 and -monto_moneda_cambiaria or 0.0,
                        'amount_currency': 0.0,
                        'partner_id': pago.partner_id.id,
                        'date_maturity': datetime.datetime.today().strftime('%Y-%m-%d'),
                        'invoice_idd': self.env.context.get('invoice_id'),
                        }
                    print "esto es vals ", vals
                    move_id = self.env['account.move.line'].create(vals)
                    if move_id:
                        return True


            if pago.payment_type == 'outbound':
                dif = round(sum_c - sum_d, 2)
                if dif < 0:
                    dif=(-1)*(dif)
                    vals = {
                        'name': _('Currency exchange rate difference'),
                        'debit': 0.0,
                        'credit':  abs(dif),
                        'account_id':self.company_id.currency_exchange_journal_id.default_credit_account_id.id,
                        'move_id': self.id,
                        'payment_id': pago.id,
                        'currency_id': pago.journal_id.currency_id.id,
                        #'amount_currency': aml.amount_residual_currency,
                        #'amount_currency': signal > 0 and monto_moneda_cambiaria or signal <0 and -monto_moneda_cambiaria or 0.0,
                        'amount_currency': 0.0,
                        'partner_id': pago.partner_id.id,
                        'date_maturity': datetime.datetime.today().strftime('%Y-%m-%d'),
                        'invoice_idd': self.env.context.get('invoice_id'),
                        }
                    print "esto es vals ", vals
                    move_id = self.env['account.move.line'].create(vals)
                    if move_id:
                        return True
                elif dif > 0:
                    dif=(-1)*(dif)
                    vals = {
                        'name': _('Currency exchange rate difference'),
                        'debit': abs(dif),
                        'credit':  0.0,
                        'account_id':self.company_id.currency_exchange_journal_id.default_debit_account_id.id,
                        'move_id': self.id,
                        'payment_id': pago.id,
                        'currency_id': pago.journal_id.currency_id.id,
                        #'amount_currency': aml.amount_residual_currency,
                        #'amount_currency': signal > 0 and monto_moneda_cambiaria or signal <0 and -monto_moneda_cambiaria or 0.0,
                        'amount_currency': 0.0,
                        'partner_id': pago.partner_id.id,
                        'date_maturity': datetime.datetime.today().strftime('%Y-%m-%d'),
                        'invoice_idd': self.env.context.get('invoice_id'),
                        }
                    print "esto es vals ", vals
                    move_id = self.env['account.move.line'].create(vals)
                    if move_id:
                        return True


    @api.multi
    def assert_balanced(self):
        print "entra a assert_balanced"
        if not self.ids:
            return True
        prec = self.env['decimal.precision'].precision_get('Account')

        self._cr.execute("""\
            SELECT      move_id
            FROM        account_move_line
            WHERE       move_id in %s
            GROUP BY    move_id
            HAVING      abs(sum(debit) - sum(credit)) > %s
            """, (tuple(self.ids), 10 ** (-max(5, prec))))
        #if len(self._cr.fetchall()) != 0:
            #raise UserError(_("Cannot create unbalanced journal entry."))
        return True

    invoice_id = fields.Many2one('account.invoice', string="Factura")



class AccountFullReconcile(models.Model):
    _inherit = "account.full.reconcile"

    @api.multi
    def unlink(self):
        """ When removing a full reconciliation, we need to revert the eventual journal entries we created to book the
            fluctuation of the foreign currency's exchange rate.
            We need also to reconcile together the origin currency difference line and its reversal in order to completly
            cancel the currency difference entry on the partner account (otherwise it will still appear on the aged balance
            for example).
        """
        for rec in self:
            print "esto es exchange_move_id ",rec.exchange_move_id
            print "esto es exchange_partial_rec_id ",rec.exchange_partial_rec_id
            if not rec.exchange_move_id or not rec.exchange_partial_rec_id:
                continue
            #reverse the exchange rate entry
            reversed_move_id = rec.exchange_move_id.reverse_moves()[0]
            reversed_move = self.env['account.move'].browse(reversed_move_id)
            #search the original line and its newly created reversal
            for aml in reversed_move.line_ids:
                if aml.account_id.reconcile:
                    break
            if aml:
                precision = aml.currency_id and aml.currency_id.rounding or aml.company_id.currency_id.rounding
                if aml.debit or float_compare(aml.amount_currency, 0, precision_rounding=precision) == 1:
                    pair_to_rec = aml | rec.exchange_partial_rec_id.credit_move_id
                else:
                    pair_to_rec = aml | rec.exchange_partial_rec_id.debit_move_id
                #remove the partial reconciliation of the exchange rate entry as well
                rec.exchange_partial_rec_id.with_context(full_rec_lookup=False).unlink()
                #reconcile together the original exchange rate line and its reversal
                pair_to_rec.reconcile()
        return super(AccountFullReconcile, self).unlink()
