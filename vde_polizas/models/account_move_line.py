# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.tools import float_compare
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare
import datetime
import time

class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    fixed = fields.Boolean(string="Fixed?", default=False)
    invoice_idd = fields.Many2one('account.invoice', string="my Factura")
    gain_updated = fields.Boolean(string="Gaini updated?", default=False)

    def _get_pair_to_reconcile(self):
        print "entra _get_pair_to_reconcile", self
        pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))
        #field is either 'amount_residual' or 'amount_residual_currency' (if the reconciled account has a secondary currency set)
        field = self[0].account_id.currency_id and 'amount_residual_currency' or 'amount_residual'
        rounding = self[0].company_id.currency_id.rounding
        if self[0].currency_id and all([x.amount_currency and x.currency_id == self[0].currency_id for x in self]):
            #or if all lines share the same currency
            field = 'amount_residual_currency'
            rounding = self[0].currency_id.rounding
        if self._context.get('skip_full_reconcile_check') == 'amount_currency_excluded':
            field = 'amount_residual'
        elif self._context.get('skip_full_reconcile_check') == 'amount_currency_only':
            field = 'amount_residual_currency'
        #target the pair of move in self that are the oldest
        sorted_moves = sorted(self, key=lambda a: a.date)
        print "esto es sorted_moves ", sorted_moves
        debit = credit = False
        print "FIELD ",field

        if self.env.context.get('intermoneda')==True and pago.payment_type=='outbound':
            print "INTERMONEDA outbound"
            for aml in sorted_moves:
                print "aml ", aml
                if credit and debit:
                    break
                print "aml[field] ", aml[field]
                print "float_compare ",float_compare(aml[field], 0, precision_rounding=rounding)
                print "debit ", debit
                print "credit", credit
                if not aml.currency_id.id:
                    print "not aml.currency_id.id:"
                    if float_compare(aml[field], 0, precision_rounding=rounding) == 1 and not debit:
                        debit = aml
                    elif float_compare(aml[field], 0, precision_rounding=rounding) == -1 and not credit:
                        credit = aml
                else:
                    print "else"
                    if aml['credit']>0 and not credit:
                        credit = aml
                    elif aml['debit']>0 and not debit:
                        debit = aml
        elif self.env.context.get('intermoneda') == True and pago.payment_type=='inbound':
            print "INTERMONEDA inboud"
            for aml in sorted_moves:
                print "aml ", aml
                if credit and debit:
                    break
                print "aml[field] ", aml[field]
                print "float_compare ",float_compare(aml[field], 0, precision_rounding=rounding)
                print "debit ", debit
                print "credit", credit
                if not aml.currency_id.id:
                    print "not aml.currency_id.id"
                    if float_compare(aml[field], 0, precision_rounding=rounding) == 1 and not debit:
                        debit = aml
                    elif float_compare(aml[field], 0, precision_rounding=rounding) == -1 and not credit:
                        credit = aml
                else:
                    print "else"
                    if aml['credit']>0 and not credit:
                        credit = aml
                    elif aml['debit']>0 and not debit:
                        debit = aml
        elif not self.env.context.get('intermoneda'):
            print "MISMA MONEDA inboud"
            for aml in sorted_moves:
                print "aml ", aml
                if credit and debit:
                    break
                print "aml[field] ", aml[field]
                print "float_compare ",float_compare(aml[field], 0, precision_rounding=rounding)
                print "debit ", debit
                print "credit", credit
                #if float_compare(aml[field], 0, precision_rounding=rounding) == -1 and not debit:
                if not aml.currency_id.id:
                    if aml[field] < 0 and not credit:
                        credit = aml
                    elif aml[field] >0 and not debit:
                    #elif float_compare(aml[field], 0, precision_rounding=rounding) == 1 and not credit:
                        debit = aml
                    elif float_compare(aml[field], 0, precision_rounding=rounding) == 0 and not debit:
                        debit = aml
                    elif float_compare(aml[field], 0, precision_rounding=rounding) == 0 and not credit:
                        credit = aml
                else:
                    if aml['credit']>0 and not credit:
                        credit = aml
                    elif aml['debit']>0 and not debit:
                        debit = aml

        print "debit en _get_pair_to_reconcile",debit
        print "credit en _get_pair_to_reconcile",credit

        return debit, credit


    def auto_reconcile_lines(self):
        print "esto es context desde auto_reconcile_lines", self.env.context
        print "entra a auto_reconcile_lines ",self
        """ This function iterates recursively on the recordset given as parameter as long as it
            can find a debit and a credit to reconcile together. It returns the recordset of the
            account move lines that were not reconciled during the process.
        """
        if not self.env.context.get('intermoneda'):
            pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))
            print "AUTO_RECONCILE_LINE no intermoneda"
            if not self.ids:
                return self
            sm_debit_move, sm_credit_move = self._get_pair_to_reconcile()
            print "esto es sm_debit_move ",sm_debit_move
            print "esto es sm_credit_move ",sm_credit_move
            #there is no more pair to reconcile so return what move_line are left
            if not sm_credit_move or not sm_debit_move:
                print "NOT RETURN SELF"
                return self

            field = self[0].account_id.currency_id and 'amount_residual_currency' or 'amount_residual'
            if not sm_debit_move.debit and not sm_debit_move.credit:
                #both debit and credit field are 0, consider the amount_residual_currency field because it's an exchange difference entry
                field = 'amount_residual_currency'
            if self[0].currency_id and all([x.currency_id == self[0].currency_id for x in self]):
                #all the lines have the same currency, so we consider the amount_residual_currency field
                field = 'amount_residual_currency'
            if self._context.get('skip_full_reconcile_check') == 'amount_currency_excluded':
                field = 'amount_residual'
            elif self._context.get('skip_full_reconcile_check') == 'amount_currency_only':
                field = 'amount_residual_currency'
            print "FIELD ", field
            #Reconcile the pair together
            if pago.payment_type=='outbound':
                print "outbound"
                amount_reconcile = min(sm_debit_move[field], -sm_credit_move[field])
                print "amount_reconcile ",amount_reconcile
                #Remove from recordset the one(s) that will be totally reconciled
                if amount_reconcile == sm_debit_move[field]:
                    print "sm_debit_move[field] ", sm_debit_move[field]
                    self -= sm_debit_move
                if amount_reconcile == -sm_credit_move[field]:
                    print "sm_debit_move[field] ", -sm_credit_move[field]
                    self -= sm_credit_move

                #Check for the currency and amount_currency we can set
                currency = False
                amount_reconcile_currency = 0
                if sm_debit_move.currency_id == sm_credit_move.currency_id and sm_debit_move.currency_id.id:
                    currency = sm_credit_move.currency_id.id
                    amount_reconcile_currency = min(sm_debit_move.amount_residual_currency, -sm_credit_move.amount_residual_currency)

                #amount_reconcile = min(sm_debit_move.amount_residual, -sm_credit_move.amount_residual)
                if not sm_debit_move.currency_id.id or not sm_debit_move.currency_id.id:
                    print "not sm_debit_move.currency_id.id or not sm_debit_move.currency_id.id"
                    amount_reconcile = abs(min(sm_credit_move.credit, -sm_debit_move.debit))
                else:
                    print "else"
                    amount_reconcile = abs(min(-sm_credit_move.amount_currency, sm_debit_move.amount_currency))
                print "AAAAAAAA ", amount_reconcile
                if self._context.get('skip_full_reconcile_check') == 'amount_currency_excluded':
                    amount_reconcile_currency = 0.0
                    currency = self._context.get('manual_full_reconcile_currency')
                elif self._context.get('skip_full_reconcile_check') == 'amount_currency_only':
                    currency = self._context.get('manual_full_reconcile_currency')

                self.env['account.partial.reconcile'].create({
                    'debit_move_id': sm_debit_move.id,
                    'credit_move_id': sm_credit_move.id,
                    #'amount': sm_debit_move['debit'],
                    'amount': amount_reconcile,
                    'amount_currency': amount_reconcile_currency,
                    'currency_id': currency,
                })
            if pago.payment_type=='inbound':
                print "inbound"
                amount_reconcile = min(sm_debit_move[field], -sm_credit_move[field])
                print "amount_reconcile ",amount_reconcile
                #Remove from recordset the one(s) that will be totally reconciled
                if amount_reconcile == sm_debit_move[field]:
                    print "sm_debit_move[field] ", sm_debit_move[field]
                    self -= sm_debit_move
                if amount_reconcile == -sm_credit_move[field]:
                    print "sm_debit_move[field] ", -sm_credit_move[field]
                    self -= sm_credit_move

                #Check for the currency and amount_currency we can set
                currency = False
                amount_reconcile_currency = 0
                if sm_debit_move.currency_id == sm_credit_move.currency_id and sm_debit_move.currency_id.id:
                    currency = sm_credit_move.currency_id.id
                    amount_reconcile_currency = min(sm_debit_move.amount_residual_currency, -sm_credit_move.amount_residual_currency)

                #amount_reconcile = min(sm_debit_move.amount_residual, -sm_credit_move.amount_residual)
                if not sm_credit_move.currency_id.id or not sm_debit_move.currency_id.id:
                    print "not sm_credit_move.currency_id.id or not sm_debit_move.currency_id.id"
                    amount_reconcile = abs(min(sm_debit_move.debit, -sm_credit_move.credit))
                else:
                    print "else"
                    amount_reconcile = abs(min(sm_debit_move.amount_currency, -sm_credit_move.amount_currency))
                print "AAAAAAAA ", amount_reconcile
                if self._context.get('skip_full_reconcile_check') == 'amount_currency_excluded':
                    amount_reconcile_currency = 0.0
                    currency = self._context.get('manual_full_reconcile_currency')
                elif self._context.get('skip_full_reconcile_check') == 'amount_currency_only':
                    currency = self._context.get('manual_full_reconcile_currency')

                self.env['account.partial.reconcile'].create({
                    'debit_move_id': sm_debit_move.id,
                    'credit_move_id': sm_credit_move.id,
                    'amount': amount_reconcile,
                    'amount_currency': amount_reconcile_currency,
                    'currency_id': currency,
                })


            #Iterate process again on self
            return self.auto_reconcile_lines()

        if self.env.context.get('intermoneda')==False:
            print "AUTO_RECONCILE_LINE intermoneda == False"
            if not self.ids:
                return self
            sm_debit_move, sm_credit_move = self._get_pair_to_reconcile()
            print "esto es sm_debit_move ",sm_debit_move
            print "esto es sm_credit_move ",sm_credit_move
            #there is no more pair to reconcile so return what move_line are left
            if not sm_credit_move or not sm_debit_move:
                return self

            field = self[0].account_id.currency_id and 'amount_residual_currency' or 'amount_residual'
            if not sm_debit_move.debit and not sm_debit_move.credit:
                #both debit and credit field are 0, consider the amount_residual_currency field because it's an exchange difference entry
                field = 'amount_residual_currency'
            if self[0].currency_id and all([x.currency_id == self[0].currency_id for x in self]):
                #all the lines have the same currency, so we consider the amount_residual_currency field
                field = 'amount_residual_currency'
            if self._context.get('skip_full_reconcile_check') == 'amount_currency_excluded':
                field = 'amount_residual'
            elif self._context.get('skip_full_reconcile_check') == 'amount_currency_only':
                field = 'amount_residual_currency'
            #Reconcile the pair together
            amount_reconcile = min(sm_debit_move[field], -sm_credit_move[field])
            #Remove from recordset the one(s) that will be totally reconciled
            if amount_reconcile == sm_debit_move[field]:
                self -= sm_debit_move
            if amount_reconcile == -sm_credit_move[field]:
                self -= sm_credit_move

            #Check for the currency and amount_currency we can set
            currency = False
            amount_reconcile_currency = 0
            if sm_debit_move.currency_id == sm_credit_move.currency_id and sm_debit_move.currency_id.id:
                currency = sm_credit_move.currency_id.id
                amount_reconcile_currency = min(sm_debit_move.amount_residual_currency, -sm_credit_move.amount_residual_currency)

            amount_reconcile = min(sm_debit_move.amount_residual, -sm_credit_move.amount_residual)

            if self._context.get('skip_full_reconcile_check') == 'amount_currency_excluded':
                amount_reconcile_currency = 0.0
                currency = self._context.get('manual_full_reconcile_currency')
            elif self._context.get('skip_full_reconcile_check') == 'amount_currency_only':
                currency = self._context.get('manual_full_reconcile_currency')

            self.env['account.partial.reconcile'].create({
                'debit_move_id': sm_debit_move.id,
                'credit_move_id': sm_credit_move.id,
                'amount': amount_reconcile,
                'amount_currency': amount_reconcile_currency,
                'currency_id': currency,
            })

            #Iterate process again on self
            return self.auto_reconcile_lines()

        elif self.env.context.get('intermoneda')==True:
            print "AUTO_RECONCILE_LINE intermoneda == True ", self.ids
            if not self.ids:
                return self
            sm_debit_move, sm_credit_move = self._get_pair_to_reconcile()
            print "esto es sm_debit_move ",sm_debit_move
            print "esto es sm_credit_move ",sm_credit_move

            #there is no more pair to reconcile so return what move_line are left
            if not sm_credit_move or not sm_debit_move:
                return self

            field = self[0].account_id.currency_id and 'amount_residual_currency' or 'amount_residual'
            if not sm_debit_move.debit and not sm_debit_move.credit:
                #both debit and credit field are 0, consider the amount_residual_currency field because it's an exchange difference entry
                field = 'amount_residual_currency'
            if self[0].currency_id and all([x.currency_id == self[0].currency_id for x in self]):
                #all the lines have the same currency, so we consider the amount_residual_currency field
                field = 'amount_residual_currency'
            if self._context.get('skip_full_reconcile_check') == 'amount_currency_excluded':
                field = 'amount_residual'
            elif self._context.get('skip_full_reconcile_check') == 'amount_currency_only':
                field = 'amount_residual_currency'

            pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))

            if pago.payment_type=='inbound':
                print"inbound"
                #Reconcile the pair together
                amount_reconcile = min(sm_debit_move[field], -sm_credit_move[field])
                print "esto es amount_reconcile ", amount_reconcile
                #Remove from recordset the one(s) that will be totally reconciled
                if amount_reconcile == sm_debit_move[field]:
                    self -= sm_debit_move
                if amount_reconcile == -sm_credit_move[field]:
                    self -= sm_credit_move
                print "esto es amount_reconcile ", amount_reconcile
                #Check for the currency and amount_currency we can set
                currency = False
                amount_reconcile_currency = 0
                if sm_debit_move.currency_id == sm_credit_move.currency_id and sm_debit_move.currency_id.id:
                    currency = sm_credit_move.currency_id.id
                    amount_reconcile_currency = min(sm_debit_move.amount_residual_currency, -sm_credit_move.amount_residual_currency)
                #amount_reconcile = min(sm_debit_move.amount_residual, -sm_credit_move.amount_residual)
                if not sm_credit_move.currency_id.id or not sm_debit_move.currency_id.id:
                    print "not sm_credit_move.currency_id.id or not sm_debit_move.currency_id.id"
                    amount_reconcile = abs(min(sm_debit_move.debit, -sm_credit_move.credit))
                else:
                    print "else"
                    amount_reconcile = abs(min(sm_debit_move.amount_currency, -sm_credit_move.amount_currency))
                print "AAAAAAAA ", amount_reconcile
                if self._context.get('skip_full_reconcile_check') == 'amount_currency_excluded':
                    amount_reconcile_currency = 0.0
                    currency = self._context.get('manual_full_reconcile_currency')
                elif self._context.get('skip_full_reconcile_check') == 'amount_currency_only':
                    currency = self._context.get('manual_full_reconcile_currency')
                print "esto es amount_reconcile ", amount_reconcile
                print "esto es amount_reconcile_currency ", amount_reconcile_currency
                self.env['account.partial.reconcile'].create({
                    'debit_move_id': sm_debit_move.id,
                    'credit_move_id': sm_credit_move.id,
                    'amount': amount_reconcile,
                    'amount_currency': amount_reconcile_currency,
                    'currency_id': currency,
                })

                #Iterate process again on self
                return self.auto_reconcile_lines()

            if pago.payment_type=='outbound':
                print"outbound"
                #Reconcile the pair together
                amount_reconcile = min(sm_debit_move[field], -sm_credit_move[field])
                print "esto es amount_reconcile ", amount_reconcile
                #Remove from recordset the one(s) that will be totally reconciled
                if amount_reconcile == sm_debit_move[field]:
                    self -= sm_debit_move
                if amount_reconcile == -sm_credit_move[field]:
                    self -= sm_credit_move
                print "esto es amount_reconcile ", amount_reconcile
                #Check for the currency and amount_currency we can set
                currency = False
                amount_reconcile_currency = 0
                if sm_debit_move.currency_id == sm_credit_move.currency_id and sm_debit_move.currency_id.id:
                    currency = sm_credit_move.currency_id.id
                    amount_reconcile_currency = min(sm_debit_move.amount_residual_currency, -sm_credit_move.amount_residual_currency)
                
                #amount_reconcile = min(sm_debit_move.amount_residual, -sm_credit_move.amount_residual)
                #if not sm_debit_move.currency_id.id or not sm_debit_move.currency_id.id:
                if not sm_credit_move.currency_id.id or not sm_debit_move.currency_id.id:
                    print "not sm_debit_move.currency_id.id or not sm_debit_move.currency_id.id"
                    amount_reconcile = abs(min(sm_credit_move.credit, -sm_debit_move.debit))
                else:
                    print "else"
                    amount_reconcile = abs(min(-sm_credit_move.amount_currency, sm_debit_move.amount_currency))
                print "AAAAAAAA ", amount_reconcile
                if self._context.get('skip_full_reconcile_check') == 'amount_currency_excluded':
                    amount_reconcile_currency = 0.0
                    currency = self._context.get('manual_full_reconcile_currency')
                elif self._context.get('skip_full_reconcile_check') == 'amount_currency_only':
                    currency = self._context.get('manual_full_reconcile_currency')
                print "esto es amount_reconcile ", amount_reconcile
                print "esto es amount_reconcile_currency ", amount_reconcile_currency
                self.env['account.partial.reconcile'].create({
                    'debit_move_id': sm_debit_move.id,
                    'credit_move_id': sm_credit_move.id,
                    'amount': amount_reconcile,
                    'amount_currency': amount_reconcile_currency,
                    'currency_id': currency,
                })

                #Iterate process again on self
                return self.auto_reconcile_lines()




    @api.multi
    def reconcile(self, writeoff_acc_id=False, writeoff_journal_id=False):
        print "esto es context desde reconcile", self.env.context
        print "entra a reconcile "
        print "esto es self ",self
        print "esto es writeoff_acc_id ",writeoff_acc_id
        print "esto es writeoff_journal_id ",writeoff_journal_id
        # Empty self can happen if the user tries to reconcile entries which are already reconciled.
        # The calling method might have filtered out reconciled lines.
        if not self:
            return True

        #Perform all checks on lines
        company_ids = set()
        all_accounts = []
        partners = set()
        print "esto es self ", self
        for line in self:
            print "esto es line ", line.id
            print "esto es line ", line.name
            print "esto es line ", line.reconciled
            company_ids.add(line.company_id.id)
            all_accounts.append(line.account_id)
            if (line.account_id.internal_type in ('receivable', 'payable')):
                partners.add(line.partner_id.id)
            if line.reconciled:
                raise UserError(_('You are trying to reconcile some entries that are already reconciled!'))
        if len(company_ids) > 1:
            raise UserError(_('To reconcile the entries company should be the same for all entries!'))
        if len(set(all_accounts)) > 1:
            raise UserError(_('Entries are not of the same account!'))
        if not all_accounts[0].reconcile:
            raise UserError(_('The account %s (%s) is not marked as reconciliable !') % (all_accounts[0].name, all_accounts[0].code))
        if len(partners) > 1:
            raise UserError(_('The partner has to be the same on all lines for receivable and payable accounts!'))

        #reconcile everything that can be
        remaining_moves = self.auto_reconcile_lines()

        print "esto es remaining_moves ",remaining_moves

        #if writeoff_acc_id specified, then create write-off move with value the remaining amount from move in self
        if writeoff_acc_id and writeoff_journal_id and remaining_moves:
            all_aml_share_same_currency = all([x.currency_id == self[0].currency_id for x in self])
            writeoff_vals = {
                'account_id': writeoff_acc_id.id,
                'journal_id': writeoff_journal_id.id
            }
            if not all_aml_share_same_currency:
                writeoff_vals['amount_currency'] = False
            writeoff_to_reconcile = remaining_moves._create_writeoff(writeoff_vals)
            #add writeoff line to reconcile algo and finish the reconciliation
            remaining_moves = (remaining_moves + writeoff_to_reconcile).auto_reconcile_lines()
            return writeoff_to_reconcile
        return True

    @api.depends('debit', 'credit', 'amount_currency', 'currency_id', 'matched_debit_ids', 'matched_credit_ids', 'matched_debit_ids.amount', 'matched_credit_ids.amount', 'move_id.state')
    def _amount_residual(self):
        print "esto es context desde _amount_residual ", self.env.context
        print "ids ", self
        pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))
        amount_mxn = 0
        credit_sum = 0
        amount_invoice = 0
        move_invoice_line = 0
        print "esto es lo importante",pago.payment_type
        pagos = 0
        valor_factura = 0
        factura = 0
        amount_residual_currency = 0
        pago_currency = 0
        for line in self:
            print "for line ", line
            print "for line.amount_residual ", line.amount_residual
            if not line.account_id.reconcile and line.account_id.internal_type != 'liquidity':
                line.reconciled = False
                line.amount_residual = 0
                line.amount_residual_currency = 0
                continue
            amount = abs(line.debit - line.credit)
            amount_currency = line.amount_currency
            sign = 1 if (line.debit - line.credit) > 0 else -1
            print "esto es sign ", sign
            if not line.debit and not line.credit and line.amount_currency and line.currency_id:
                #residual for exchange rate entries
                sign = 1 if float_compare(line.amount_currency, 0, precision_rounding=line.currency_id.rounding) == 1 else -1
            print "esto es sign ", sign
            print "esto es line.matched_debit_ids ", line.matched_debit_ids
            print "esto es line.matched_credit_ids ", line.matched_credit_ids
            print "esto es line.matched merged ", (line.matched_debit_ids + line.matched_credit_ids)
            for partial_line in (line.matched_debit_ids + line.matched_credit_ids):
                print "esto es partial_line ", partial_line
                if pago.payment_type == 'outbound':
                    print "PAGO OUTBOUND"
                    if line.invoice_id.id:
                        factura = line
                        if not line.currency_id.id:
                            valor_factura = line.credit
                        else:
                            valor_factura = line.amount_currency
                        for moves in partial_line:
                            print "moves ", moves
                            for debit_move in moves.debit_move_id:
                                print "debit_move ", debit_move
                                if debit_move.currency_id:
                                    print "line.currency_id.id ",line.currency_id.id
                                    print "debit_move.currency_id.id ", debit_move.currency_id.id
                                    if not line.currency_id.id and not debit_move.currency_id.id:
                                        print "debit_move.amount_currency ",debit_move.amount_currency 
                                        #debit_amount = partial_line.debit_move_id.currency_id.with_context(date=debit_move.date).compute(debit_move.amount_currency, self.env.user.company_id.currency_id)
                                        debit_amount = abs(debit_move.amount_currency)
                                    else:
                                        print "else"
                                        if line.currency_id.id == debit_move.currency_id.id:
                                            print "line.currency_id.id == debit_move.currency_id.id"
                                            debit_amount = abs(debit_move.amount_currency)
                                        else:
                                            print "else"
                                            print "debit_amount", debit_move.currency_id.with_context(date=debit_move.date).compute(debit_move.amount_currency, self.env.user.company_id.currency_id)
                                            debit_amount = debit_move.currency_id.with_context(date=debit_move.date).compute(debit_move.amount_currency, self.env.user.company_id.currency_id)
                                            debit_amount = abs(debit_amount)
                                else:
                                    print "debit_move.amount_residual ",debit_move.amount_residual
                                    print "debit_move.debit ",debit_move.debit
                                    debit_amount = debit_move.debit
                                print "debit_amount ",debit_amount
                                pagos = pagos + debit_amount
                            print "pagos ", pagos
                if pago.payment_type == 'inbound':
                    print "PAGO INBOUND"
                    if line.invoice_id.id:
                        factura = line
                        if not line.currency_id.id:
                            valor_factura = line.debit
                        else:
                            valor_factura = abs(line.amount_currency)
                        for moves in partial_line:
                            print "moves ", moves
                            for credit_move in moves.credit_move_id:
                                print "credit_move ", credit_move
                                if credit_move.currency_id:
                                    print "line.currency_id.id ",line.currency_id.id
                                    print "credit_move.currency_id.id ", credit_move.currency_id.id
                                    if not line.currency_id.id and not credit_move.currency_id.id:
                                        print "credit_move.amount_currency ",credit_move.amount_currency 
                                        #credit_amount = partial_line.credit_move_id.currency_id.with_context(date=credit_move.date).compute(credit_move.amount_currency, self.env.user.company_id.currency_id)
                                        credit_amount = abs(credit_move.amount_currency)
                                    else:
                                        print "else"
                                        if line.currency_id.id == credit_move.currency_id.id:
                                            print "line.currency_id.id == credit_move.currency_id.id"
                                            credit_amount = abs(credit_move.amount_currency)
                                        else:
                                            print "else"
                                            print "credit_amount", credit_move.currency_id.with_context(date=credit_move.date).compute(credit_move.amount_currency, self.env.user.company_id.currency_id)
                                            credit_amount = credit_move.currency_id.with_context(date=credit_move.date).compute(credit_move.amount_currency, self.env.user.company_id.currency_id)
                                            credit_amount = abs(credit_amount)   
                                else:
                                    print "credit_move.amount_residual ",credit_move.amount_residual
                                    print "credit_move.credit ",credit_move.credit
                                    credit_amount = credit_move.credit
                                print "credit_amount ",credit_amount
                                pagos = pagos + credit_amount
                            print "pagos ", pagos
                if pago.payment_type == False:
                    print "ES CANCELACION DE PAGO"
                    for moves in partial_line:
                        print "moves ", moves
                        if line.id == moves.credit_move_id.id:
                            print "PAGO PROVEEDOR line.id == moves.credit_move_id "
                            for debit_move in moves.debit_move_id:
                                print "debit_move ", debit_move
                                if debit_move.currency_id:
                                    print "debit_move.amount_currency ",debit_move.amount_currency 
                                    if debit_move.currency_id.id == line.currency_id.id:
                                        debit_amount = debit_move.amount_currency
                                    else:
                                        debit_amount = partial_line.debit_move_id.currency_id.with_context(date=debit_move.date).compute(debit_move.amount_currency, self.env.user.company_id.currency_id)
                                else:
                                    print "debit_move.amount_residual ",debit_move.amount_residual
                                    print "debit_move.debit ",debit_move.debit
                                    debit_amount = debit_move.debit
                                print "debit_amount ",debit_amount
                            pagos = pagos + debit_amount
                        print "pagos ", pagos
                        if line.id == moves.debit_move_id.id:
                            print "line.id == moves.debit_move_id.id"
                            for credit_move in moves.credit_move_id:
                                print "credit_move ", credit_move
                                if credit_move.currency_id:
                                    print "credit_move.amount_currency ",credit_move.amount_currency 
                                    if credit_move.currency_id.id == line.currency_id.id:
                                        credit_amount = credit_move.amount_currency 
                                    else:
                                        credit_amount = partial_line.credit_move_id.currency_id.with_context(date=credit_move.date).compute(credit_move.amount_currency, self.env.user.company_id.currency_id)
                                else:
                                    print "credit_amount.amount_residual ",credit_move.amount_residual
                                    print "credit_amount.debit ",credit_move.credit
                                    credit_amount = credit_move.credit
                                print "credit_amount ",credit_amount
                            pagos = pagos + credit_amount
                        print "pagos ", pagos
            reconciled = False
            digits_rounding_precision = line.company_id.currency_id.rounding
            if float_is_zero(amount, precision_rounding=digits_rounding_precision):
                print "float_is_zero(amount, precision_rounding=digits_rounding_precision):"
                if line.currency_id and line.amount_currency:
                    print "if line.currency_id and line.amount_currency:"
                    if float_is_zero(amount_residual_currency, precision_rounding=line.currency_id.rounding):
                        print "if float_is_zero(amount_residual_currency, precision_rounding=line.currency_id.rounding):"
                        reconciled = True
                else:
                    print "else"
                    reconciled = True
            print "esto es reconciled ", reconciled
            line.reconciled = reconciled
            print "line ", line
            print "line.amount_residual ", line.amount_residual
            print "line.invoice_id ", line.invoice_id
            print "pago.payment_type ", pago.payment_type
            if (pago.payment_type == False):
                invoice_cancel = amount
                print "REC PAGO == FALSE"
                print "amount ", amount
                print "amount_currency ", amount_currency
                print "sign ", sign
                print "LEN ", len(line.matched_debit_ids + line.matched_credit_ids)
                #ENTRA AQUI AL CREAR LA FACTURA PARA CALCULAR MONTO A PAGAR
                if len(line.matched_debit_ids + line.matched_credit_ids) ==0 and line.invoice_id.id:
                    print "FIRST TIME1"
                    if not line.currency_id.id:
                        line.amount_residual = line.company_id.currency_id.round(amount * sign)
                        line.amount_residual_currency = line.currency_id and line.currency_id.round(amount_residual_currency * sign) or 0.0
                    elif line.currency_id.id:
                        line.amount_residual = line.company_id.currency_id.round(line.amount_currency * sign)
                        line.amount_residual_currency = line.currency_id and line.currency_id.round(line.amount_currency * sign) or 0.0
                elif len(line.matched_debit_ids + line.matched_credit_ids) >0  and not line.invoice_id.id:
                    print "FIRST TIME"
                elif len(line.matched_debit_ids + line.matched_credit_ids) >0  and line.invoice_id.id:
                    print "CANCEL1 ", line.amount_residual
                    #print "invoice_cancel ", invoice_canel
                    if line.invoice_id.type == 'in_invoice':
                        print "in_invoice"
                        if not line.currency_id.id:
                            print "not line.currency_id.id"
                            line.amount_residual = line.company_id.currency_id.round((amount - pagos) * -1)
                            line.amount_residual_currency = line.currency_id and line.currency_id.round(amount_residual_currency * sign) or 0.0
                        else:
                            print "line.currency_id.id"
                            line.amount_residual = line.company_id.currency_id.round((amount_currency + pagos) * sign)
                            line.amount_residual_currency = line.currency_id and line.currency_id.round((amount_currency + pagos) * sign) or 0.0
                    elif line.invoice_id.type == 'out_invoice':
                        print "out_invoice"
                        if not line.currency_id.id:
                            print "not line.currency_id.id"
                            line.amount_residual = line.company_id.currency_id.round((amount - pagos) * 1)
                            line.amount_residual_currency = line.currency_id and line.currency_id.round(amount_residual_currency * sign) or 0.0
                        else:
                            print "line.currency_id.id"
                            line.amount_residual = line.company_id.currency_id.round((amount_currency + pagos) * sign)
                            line.amount_residual_currency = line.currency_id and line.currency_id.round((amount_currency + pagos) * sign) or 0.0
                if len(line.matched_debit_ids + line.matched_credit_ids) == 0 and not line.invoice_id.id:
                    print "ESCRIBE LA PRIMER LINEAE DEL PAGO"
                    print "amount ", amount
                    print "sign ", sign
                    if line.invoice_id.type=='out_invoice' and not line.invoice_id.id:
                        print "line.invoice_id.type=='out_invoice' or not line.invoice_id.id"
                        line.amount_residual = line.company_id.currency_id.round(amount)
                        line.amount_residual_currency = line.currency_id and line.currency_id.round(amount_residual_currency) or 0.0
                    elif line.invoice_id.type=='in_invoice' and not line.invoice_id.id:
                        print "line.invoice_id.type=='in_invoice' or not line.invoice_id.id"
                        line.amount_residual = line.company_id.currency_id.round(amount * sign)
                        line.amount_residual_currency = line.currency_id and line.currency_id.round(amount_residual_currency * sign) or 0.0
                    elif pago.payment_type==False:
                        print "line.invoice_id.type==False"
            if pago.payment_type == 'outbound':
                if line.invoice_id.id:
                    print "REC OUTBOUND ", factura.amount_residual
                    if not line.currency_id.id:
                        print "not line.currency_id.id"
                        factura.amount_residual = line.company_id.currency_id.round((valor_factura-pagos) * -1)
                        factura.amount_residual_currency = line.currency_id and line.currency_id.round(amount_residual_currency * -1) or 0.0
                    if not line.currency_id.id and self.env.context.get('intermoneda')==True:
                        print "not line.currency_id.id and self.env.context.get('intermoneda')==True"
                        if pago.currency_id.id == factura.currency_id.id:
                            print "pago.currency_id.id == factura.currency_id.id"
                            factura.amount_residual = line.company_id.currency_id.round((valor_factura-pago.currency_id.with_context(date=pago.payment_date).compute(pagos, self.env.user.company_id.currency_id)) * -1)
                            factura.amount_residual_currency = line.currency_id and line.currency_id.round(amount_residual_currency * -1) or 0.0
                        else:
                            print "else"
                            factura.amount_residual = line.company_id.currency_id.round((valor_factura-pagos) * -1)
                            factura.amount_residual_currency = line.currency_id and line.currency_id.round(amount_residual_currency * -1) or 0.0
                    elif line.currency_id.id:
                        print "line.currency_id.id"
                        if pago.currency_id.id == factura.currency_id.id and pago.currency_id.id == self.env.user.company_id.id:
                            print "pago.currency_id.id == factura.currency_id.id"
                            line.amount_residual = line.company_id.currency_id.round((valor_factura-pago_currency.with_context(date=pago.payment_date).compute(pagos, self.env.user.company_id.currency_id)) * -1)
                            line.amount_residual_currency = line.currency_id and line.currency_id.round(amount_residual_currency * -1) or 0.0
                        else:
                            print "else"
                            factura.amount_residual = line.company_id.currency_id.round((valor_factura + pagos) * -1)
                            line.amount_residual_currency = line.currency_id and line.currency_id.round(valor_factura-pagos * -1) or 0.0
                else:
                    print "ELSEEEEEEEE"
                    if line.currency_id.id:
                        pago_currency = line.currency_id
            if pago.payment_type == 'inbound':
                if line.invoice_id.id:
                    print "REC INBOUND ", line.amount_residual
                    if not line.currency_id.id:
                        print "not line.currency_id.id"
                        line.amount_residual = line.company_id.currency_id.round((valor_factura-pagos) * 1)
                        line.amount_residual_currency = line.currency_id and line.currency_id.round(amount_residual_currency * 1) or 0.0
                    if not line.currency_id.id and self.env.context.get('intermoneda')==True:
                        print "not line.currency_id.id and self.env.context.get('intermoneda')==True"
                        if pago.currency_id.id == factura.currency_id.id and pago.currency_id.id == self.env.user.company_id.currency_id.id:
                            print "pago.currency_id.id == factura.currency_id.id"
                            line.amount_residual = line.company_id.currency_id.round((valor_factura-pago_currency.with_context(date=pago.payment_date).compute(pagos, self.env.user.company_id.currency_id)) * 1)
                            line.amount_residual_currency = line.currency_id and line.currency_id.round(amount_residual_currency * 1) or 0.0
                        else:
                            print "else"
                            factura.amount_residual = line.company_id.currency_id.round((valor_factura - pagos) * 1)
                            line.amount_residual_currency = line.currency_id and line.currency_id.round(valor_factura-pagos * 1) or 0.0       
                    elif line.currency_id.id:
                        print "line.currency_id.id"
                        if pago.currency_id.id == factura.currency_id.id and pago.currency_id.id == self.env.user.company_id.currency_id.id:
                            print "pago.currency_id.id == factura.currency_id.id"
                            line.amount_residual = line.company_id.currency_id.round((valor_factura-pago_currency.with_context(date=pago.payment_date).compute(pagos, self.env.user.company_id.currency_id)) * 1)
                            line.amount_residual_currency = line.currency_id and line.currency_id.round(amount_residual_currency * 1) or 0.0
                        else:
                            print "else"
                            line.amount_residual = line.company_id.currency_id.round((valor_factura-pagos) * 1)
                            line.amount_residual_currency = line.currency_id and line.currency_id.round(valor_factura-pagos * 1) or 0.0
                else:
                    print "ELSEEEEEEEE"
                    if line.currency_id.id:
                        pago_currency = line.currency_id