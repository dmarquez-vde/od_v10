# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero
from odoo.tools import float_is_zero, float_compare
from collections import OrderedDict
import datetime


class AccountPartialReconcileCashBasis(models.Model):
    _inherit = 'account.partial.reconcile'

    def _get_tax_cash_basis_lines(self, value_before_reconciliation):
        # Search in account_move if we have any taxes account move lines
        print "entra a _get_tax_cash_basis_lines"
        tax_group = {}
        total_by_cash_basis_account = {}
        move_date = self.debit_move_id.date
        line_to_create = []

        #PAGOS EN MISMA MONEDA
        if self.env.context.get('intermoneda')==False:
            # Search in account_move if we have any taxes account move lines
            print "entra a intermoneda = False"
            print "esto es context desde _get_tax_cash_basis_lines ", self.env.context
            print "esto es self.debit_move_id ", self.debit_move_id
            print "esto es self.credit_move_id ", self.credit_move_id
            pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))
            print "esto es pago ", pago.journal_id.currency_id.name
            if pago.journal_id.currency_id.name != False:
                ids_linea_partner=self.env['account.move.line'].search([('fixed','=',False),('account_id','=',self.debit_move_id.account_id.id),('move_id','=',self.env.context.get('pago_poliza_id'))])
                print "esto es ids_linea_partner ", ids_linea_partner
                currency = pago.journal_id.currency_id
                for id_linea_partner in ids_linea_partner:
                    print "esto es id_linea_partner ",id_linea_partner
                    #pago_factura = currency.with_context(date=self.env.context.get('fecha_factura')).compute(amount_diff, self.company_id.currency_id)
                    #print "esto es pago_factura ", pago_factura
                    ##total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(self.amount, self.company_id.currency_id)
                    linea_partner=self.env['account.move.line'].browse(id_linea_partner.id)
                    pago_factura = currency.with_context(date=self.env.context.get('fecha_factura')).compute(abs(linea_partner.amount_currency), self.company_id.currency_id)
                    print "esto es pago_factura ", pago_factura
                    linea_partner.write({'reconciled':False})
                    if linea_partner.debit > 0:
                        debito = linea_partner.debit
                        print "esto es debito ", debito
                        linea_partner.write({'debit':pago_factura})
                    if linea_partner.credit > 0:
                        credito = linea_partner.credit
                        print "esto es credito ", credito
                        linea_partner.write({'credit':pago_factura})
                    linea_partner.write({'reconciled':True,'fixed':True})
                    print "esto es linea partnerrrr ", linea_partner
            #PARA PAGOS DE CLIENTE en MXN misma moneda
            if (self.credit_move_id.journal_id.type not in ['sale', 'purchase', 'general']) and self.env['account.payment'].browse(self.env.context.get('pago_id')).currency_id.name == 'MXN':
                print "entra credit_move PARA PAGOS DE CLIENTES con pago en MXN"
                move_id = self.debit_move_id.move_id
                if move_id:
                    print "numero 1",move_id.line_ids
                    cantidad=len(move_id.line_ids)
                    print "esto es cantidad",cantidad
                    #if cantidad >= 4:
                    factura = self.env['account.invoice'].browse(self.env.context.get('invoice_id'))
                    print"esto son las facturas",factura.id
                    base_amount=factura.amount_total
                    print"base_amount",factura.amount_total
                    monto_pago = abs(self.env.context.get('monto_pago'))

                    porcentaje_pago= (monto_pago*100)/base_amount
                    print"porcentaje_pago",porcentaje_pago
                    print"monto_pago",monto_pago
                    #lineas_poliza = self.env['account.move.line'].search([('move_id','=',self.env.context.get('pago_poliza_id'))])
                    #for linea_poliza in lineas_poliza:
                        #impuestos_ids.append(linea_poliza.account_id.id)
                    for line1 in factura.tax_line_ids:
                        linea_poliza=self.env['account.move.line'].search([('account_id','=',line1.account_id.id),('move_id','=',self.env.context.get('pago_poliza_id')),('invoice_idd','=',factura.id)])
                        if linea_poliza:
                            continue
                        else:
                            print"esto son las facturas",line1.amount
                            base_impuesto=line1.amount*(porcentaje_pago/100)
                            line_to_create.append((0,0,{
                                'name': '/',
                                #'debit': asiento.credit if asiento.credit else 0.0,
                                #'credit': asiento.debit if asiento.debit else 0.0,
                                'debit': abs(base_impuesto) or 0.0,
                                'credit': 0.0,
                                #'account_id': line.tax_line_id.account_id.id,
                                'account_id': line1.tax_id.account_id.id,
                                'tax_exigible': True,
                                'payment_id': self.env.context.get('pago_id'),
                                'invoice_idd': self.env.context.get('invoice_id'),
                            }))
                            #impuestos_ids.append(line.tax_line_id.account_id.id)
                            print "esto es line_to_createeeeeeeeee arriba", line_to_create
                            #monto_pago = abs(self.env.context.get('monto_pago'))
                            #monto_impuesto1 = monto_pago * tax_amount
                            #monto_impuesto1 = (monto_pago / (1+tax_amount))*tax_amount
                            #monto_impuesto1 =  currency.with_context(date=self.env.context.get('fecha_pago')).compute(monto_impuesto1, self.env.user.company_id.currency_id)
                            line_to_create.append((0,0,{
                                'name': '/',
                                #'debit': asiento.credit if asiento.credit else 0.0,
                                #'credit': asiento.debit if asiento.debit else 0.0,
                                'debit': 0.0,
                                'credit': abs(base_impuesto) or 0.0,
                                #'account_id': line.tax_line_id.cash_basis_account.id,
                                'account_id': line1.tax_id.cash_basis_account.id,
                                'tax_exigible': True,
                                'payment_id': self.env.context.get('pago_id'),
                                'invoice_idd': self.env.context.get('invoice_id'),
                            }))

                            #impuestos_ids.append(line.tax_line_id.cash_basis_account.id)
                            #impuestos_ids=True
                            print "esto es line_to_createeeeeeeeee abajo ", line_to_create
            #PARA PAGOS DE CLIENTE en USD misma moneda
            if (self.credit_move_id.journal_id.type not in ['sale', 'purchase', 'general']) and self.env['account.payment'].browse(self.env.context.get('pago_id')).currency_id.name == 'USD':
                print "entra credit_move PARA PAGOS DE CLIENTES con pago en USD"
                move_id = self.debit_move_id.move_id
                if move_id:
                    tax_per = 0
                    for line in move_id.line_ids:
                        if line.tax_line_id and line.tax_line_id.use_cash_basis:
                            print "DMZ LINE taxes", line.tax_line_id
                            tax_amount = (line.tax_line_id.amount/100) or 0
                            tax_per = tax_per + tax_amount
                            print "DMZ LINE taxes tax_per ", tax_per
                    for line in move_id.line_ids:
                        print "DMZ LINE ", line
                        currency = line.currency_id or line.company_id.currency_id
                        #if not line.fixed:
                        if line.tax_line_id and line.tax_line_id.use_cash_basis:
                            tax_amount = (line.tax_line_id.amount/100) or 0
                            base_amount = self.credit_move_id.credit or 0
                            print "esto es base_amount ", base_amount
                            #monto_impuesto = base_amount * tax_amount
                            print "esto es base_amount ", (base_amount / (1+tax_amount))
                            #monto_impuesto = (base_amount / (1+tax_amount))*tax_amount
                            monto_impuesto = (base_amount/(1+tax_per)) * tax_amount
                            rec_ids = self.env['account.move.line'].search([
                                ('payment_id', '=', self.env.context.get('pago_id')),
                                ('account_id', '=', line.tax_line_id.account_id.id),
                                ('invoice_idd', '=', self.env.context.get('invoice_id')),
                                ])
                            if len(rec_ids)<1:
                                line_to_create.append((0,0,{
                                    'name': '/',
                                    #'debit': asiento.credit if asiento.credit else 0.0,
                                    #'credit': asiento.debit if asiento.debit else 0.0,
                                    'debit': abs(monto_impuesto) or 0.0,
                                    'credit': 0.0,
                                    'account_id': line.tax_line_id.account_id.id,
                                    'tax_exigible': True,
                                    'payment_id': self.env.context.get('pago_id'),
                                    'invoice_idd': self.env.context.get('invoice_id'),
                                }))
                                print "esto es line_to_createeeeeeeeee ", line_to_create
                                monto_pago = abs(self.env.context.get('monto_pago'))
                                #monto_impuesto1 = monto_pago * tax_amount
                                #monto_impuesto1 = (monto_pago / (1+tax_amount))*tax_amount
                                monto_impuesto1 = (monto_pago / (1+tax_per))*tax_amount
                                monto_impuesto1 =  currency.with_context(date=self.env.context.get('fecha_pago')).compute(monto_impuesto1, self.env.user.company_id.currency_id)
                                line_to_create.append((0,0,{
                                    'name': '/',
                                    #'debit': asiento.credit if asiento.credit else 0.0,
                                    #'credit': asiento.debit if asiento.debit else 0.0,
                                    'debit': 0.0,
                                    'credit': abs(monto_impuesto1) or 0.0,
                                    'account_id': line.tax_line_id.cash_basis_account.id,
                                    'tax_exigible': True,
                                    'payment_id': self.env.context.get('pago_id'),
                                    'invoice_idd': self.env.context.get('invoice_id'),
                                }))
                                print "esto es line_to_createeeeeeeeee ", line_to_create


            #PARA PAGOS DE PROVEEDOR misma moneda
            if (self.debit_move_id.journal_id.type not in ['sale', 'purchase', 'general']):
                print "entra debit_move PARA PAGOS DE PROVEDOOR"
                move_id = self.credit_move_id.move_id
                if move_id:
                    print "numero 1",move_id.line_ids
                    cantidad=len(move_id.line_ids)
                    print "esto es cantidad",cantidad
                    factura = self.env['account.invoice'].browse(self.env.context.get('invoice_id'))
                    print "esto es id de factura",factura.id
                    if factura.partner_id.id == 13556:
                        for line in move_id.line_ids:
                            print "entro en el for cuando es if"
                            print "numero 2",move_id
                            currency = line.currency_id or line.company_id.currency_id


                            #if not line.fixed:
                            print"numero 1 ", line.tax_line_id
                            print"numero 2 ",line.tax_line_id.use_cash_basis
                            if line.tax_line_id and line.tax_line_id.use_cash_basis:
                                print "ya valio"
                                factura = self.env['account.invoice'].browse(self.env.context.get('invoice_id'))
                                print "esto es factura cuando ya valio",factura.amount_untaxed
                                tax_amount = (line.tax_line_id.amount/100) or 0
                                base_amount = self.debit_move_id.debit or 0
                                print"11111",self.credit_move_id.debit
                                print"22222",self.debit_move_id.credit
                                print"3333",self.debit_move_id.debit
                                print"4444",self.credit_move_id
                                print "esto es base_amount ", base_amount
                                print"esto es tax amount",tax_amount
                                if tax_amount<=0:
                                    monto_impuesto = (factura.amount_untaxed) * (abs(tax_amount))
                                else:
                                    monto_impuesto = (factura.amount_untaxed) * (tax_amount)
                                print"esto es lo que sale de monto impuesto",monto_impuesto
                                rec_ids = self.env['account.move.line'].search([
                                    ('payment_id', '=', self.env.context.get('pago_id')),
                                    ('account_id', '=', line.tax_line_id.account_id.id),
                                    ('invoice_idd', '=', self.env.context.get('invoice_id')),
                                    ])
                                if len(rec_ids)<1:
                                    line_to_create.append((0,0,{
                                        'name': '/',
                                        #'debit': asiento.credit if asiento.credit else 0.0,
                                        #'credit': asiento.debit if asiento.debit else 0.0,
                                        'debit': 0.0,
                                        'credit': abs(monto_impuesto) or 0.0,
                                        'account_id': line.tax_line_id.account_id.id,
                                        'tax_exigible': True,
                                        'payment_id': self.env.context.get('pago_id'),
                                        'invoice_idd': self.env.context.get('invoice_id'),
                                    }))
                                    print "esto es line_to_createeeeeeeeee ", line_to_create
                                    monto_pago = abs(self.env.context.get('monto_pago'))
                                    #monto_impuesto1 = monto_pago * tax_amount
                                    monto_impuesto1 = (monto_pago / (1+tax_amount))*tax_amount
                                    monto_impuesto1 =  currency.with_context(date=self.env.context.get('fecha_pago')).compute(monto_impuesto1, self.env.user.company_id.currency_id)
                                    monto_impuesto1=abs(monto_impuesto)
                                    line_to_create.append((0,0,{
                                        'name': '/',
                                        #'debit': asiento.credit if asiento.credit else 0.0,
                                        #'credit': asiento.debit if asiento.debit else 0.0,
                                        'debit': abs(monto_impuesto1) or 0.0,
                                        'credit': 0.0,
                                        'account_id': line.tax_line_id.cash_basis_account.id,
                                        'tax_exigible': True,
                                        'payment_id': self.env.context.get('pago_id'),
                                        'invoice_idd': self.env.context.get('invoice_id'),
                                    }))
                                    print "esto es line_to_createeeeeeeeee ", line_to_create

                    else:
                        print "entro en el for cuando es else"
                        impuestos= self.env['account.invoice.tax'].search([('invoice_id','=', factura.id)])
                        for impuesto in impuestos:
                            print"IMPUESTOOOOO MONTO",impuesto.amount
                            for line in move_id.line_ids:
                                if impuesto.account_id.id != line.account_id.id:
                                    continue
                                print "numero 2",move_id
                                currency = line.currency_id or line.company_id.currency_id
                                if not line.fixed:
                                    print"numero 1 ", line.tax_line_id
                                    print"numero 2 ",line.tax_line_id.use_cash_basis
                                if line.tax_line_id and line.tax_line_id.use_cash_basis:
                                    print "ya valio"
                                    factura = self.env['account.invoice'].browse(self.env.context.get('invoice_id'))
                                    pagos_lines = self.env['advance.payment.line'].search([('invoice_id','=', factura.id)])
                                    for pago in pagos_lines:
                                        cambio = pago.tipocambiop
                                        allocation1= pago.allocation1

                                    print "esto es pago en facturas de proveedor",cambio
                                    print "esto es factura cuando ya valio",factura.amount_untaxed
                                    tax_amount = (line.tax_line_id.amount/100) or 0
                                    print"11111",self.credit_move_id.debit
                                    print"22222",self.debit_move_id.credit
                                    print"3333",self.debit_move_id.debit
                                    print"4444",self.credit_move_id
                                    print"esto es tax amount",tax_amount
                                    total= factura.amount_total * (1/factura.tipocambio)
                                    monto_pesos= (impuesto.amount/factura.tipocambio)
                                    base_amount = (allocation1/factura.tipocambio)
                                    print "dmz impuestos.amount ", impuesto.amount
                                    print "dmz factura.tipocambio ", factura.tipocambio
                                    print "dmz base_amount ",base_amount
                                    print "dmz monto_pesos ",monto_pesos
                                    print "dmz total ", total
                                    monto_impuesto = (base_amount*monto_pesos) / total

                                    print"esto es lo que sale de monto impuesto",monto_impuesto
                                    rec_ids = self.env['account.move.line'].search([
                                        ('payment_id', '=', self.env.context.get('pago_id')),
                                        ('account_id', '=', line.tax_line_id.account_id.id),
                                        ('invoice_idd', '=', self.env.context.get('invoice_id')),
                                        ])
                                    if len(rec_ids)<1:
                                        line_to_create.append((0,0,{
                                            'name': '/',
                                            #'debit': asiento.credit if asiento.credit else 0.0,
                                            #'credit': asiento.debit if asiento.debit else 0.0,
                                            'debit': 0.0,
                                            'credit': abs(monto_impuesto) or 0.0,
                                            'account_id': line.tax_line_id.account_id.id,
                                            'tax_exigible': True,
                                            'payment_id': self.env.context.get('pago_id'),
                                            'invoice_idd': self.env.context.get('invoice_id'),
                                        }))
                                        print "esto es line_to_createeeeeeeeee ", line_to_create
                                        monto_pago = abs(self.env.context.get('monto_pago'))
                                        #monto_impuesto1 = monto_pago * tax_amount
                                        total= factura.amount_total * (1/cambio)
                                        monto_pesos= (impuesto.amount/cambio)
                                        base_amount = (allocation1/cambio)
                                        print "esto es base_amount ", base_amount
                                        monto_impuesto1 = (base_amount*monto_pesos) / total
                                        monto_impuesto1 =  currency.with_context(date=self.env.context.get('fecha_pago')).compute(monto_impuesto1, self.env.user.company_id.currency_id)
                                        monto_impuesto1 = (base_amount*monto_pesos) / total
                                        line_to_create.append((0,0,{
                                            'name': '/',
                                            #'debit': asiento.credit if asiento.credit else 0.0,
                                            #'credit': asiento.debit if asiento.debit else 0.0,
                                            'debit': abs(monto_impuesto1) or 0.0,
                                            'credit': 0.0,
                                            'account_id': line.tax_line_id.cash_basis_account.id,
                                            'tax_exigible': True,
                                            'payment_id': self.env.context.get('pago_id'),
                                            'invoice_idd': self.env.context.get('invoice_id'),
                                        }))
                                        print "esto es line_to_createeeeeeeeee ", line_to_create
        pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))
        print"esto es antes de",pago.payment_type

        #PAGOS MULTIMONEDA cliente
        if self.env.context.get('intermoneda') == True and pago.payment_type=='inbound':
            pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))
            print "entra a intermoneda = True"

            #PARA PAGOS DE CLIENTE multimoneda
            if (self.credit_move_id.journal_id.type not in ['sale', 'purchase', 'general']):
                print "entra credit_move cliente"
                print "entra credit_move"
                print "entra credit_move ",self.credit_move_id
                print "entra credit_move ",self.debit_move_id
                move_id = self.debit_move_id.move_id
                tax_per = 0
                if move_id:
                    for line in move_id.line_ids:
                        if line.tax_line_id and line.tax_line_id.use_cash_basis:
                            print "DMZ LINE taxes", line.tax_line_id
                            tax_amount = (line.tax_line_id.amount/100) or 0
                            tax_per = tax_per + tax_amount
                            print "DMZ LINE taxes tax_per ", tax_per
                    for line in move_id.line_ids:
                        currency = line.currency_id or line.company_id.currency_id
                        #if not line.fixed:
                        print"numero 1 ", line.tax_line_id
                        print"numero 2 ",line.tax_line_id.use_cash_basis
                        if line.tax_line_id and line.tax_line_id.use_cash_basis:
                            tax_amount = (line.tax_line_id.amount/100) or 0
                            base_amount = self.credit_move_id.credit or 0
                            print "esto es base_amount ", base_amount
                            #monto_impuesto = base_amount * tax_amount
                            #monto_impuesto = (base_amount / (1+tax_amount))*tax_amount
                            monto_impuesto = (base_amount / (1+tax_per))*tax_amount
                            print "esto es monto_impuesto ", monto_impuesto
                            rec_ids = self.env['account.move.line'].search([
                                ('payment_id', '=', self.env.context.get('pago_id')),
                                ('account_id', '=', line.tax_line_id.account_id.id),
                                ('invoice_idd', '=', self.env.context.get('invoice_id')),
                                ])




                            if len(rec_ids)<1:
                                if pago.currency_id.name == 'MXN':
                                    print "PAGO == PESOS"
                                    line_to_create.append((0,0,{
                                        'name': '/',
                                        #'debit': asiento.credit if asiento.credit else 0.0,
                                        #'credit': asiento.debit if asiento.debit else 0.0,
                                        'debit': abs(monto_impuesto) or 0.0,
                                        'credit': 0.0,
                                        'account_id': line.tax_line_id.account_id.id,
                                        'tax_exigible': True,
                                        'payment_id': self.env.context.get('pago_id'),
                                        'invoice_idd': self.env.context.get('invoice_id'),
                                    }))
                                    print "esto es line_to_createeeeeeeeee ", line_to_create
                                    #monto_pago = abs(self.env.context.get('monto_pago'))
                                    pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))
                                    print "esto es pago ", pago
                                    pay_amount = self.env.context.get('monto_pago')
                                    print "esto es pay_amount ", pay_amount
                                    pay_date = self.env.context.get('fecha_pago')
                                    print "esto es pay_date ", pay_date
                                    invoice_date_datetime = datetime.datetime.strptime(self.env.context.get('fecha_factura'), '%Y-%m-%d')
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                    ('currency_id', '=', line.invoice_id.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate = 1.0
                                    print "esto es rate ", rate
                                    pay_ori_amount = pay_amount / (1/rate)
                                    print "esto es pay_ori_amount ", pay_ori_amount
                                    invoice_date_datetime = datetime.datetime.strptime(pay_date, '%Y-%m-%d')
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                    ('currency_id', '=', line.invoice_id.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate = 1.0
                                    print "esto es rate ", rate
                                    pay_amount_new = abs(round(pay_ori_amount * (1/rate),2))
                                    monto_pago = pay_amount_new
                                    #monto_impuesto1 = monto_pago * tax_amount
                                    #monto_impuesto1 = (monto_pago / (1+tax_amount))*tax_amount
                                    monto_impuesto1 = (monto_pago / (1+tax_per))*tax_amount
                                    #monto_impuesto1 =  currency.with_context(date=self.env.context.get('fecha_pago')).compute(monto_impuesto1, self.env.user.company_id.currency_id)
                                    line_to_create.append((0,0,{
                                        'name': '/',
                                        #'debit': asiento.credit if asiento.credit else 0.0,
                                        #'credit': asiento.debit if asiento.debit else 0.0,
                                        'debit': 0.0,
                                        'credit': abs(monto_impuesto1) or 0.0,
                                        'account_id': line.tax_line_id.cash_basis_account.id,
                                        'tax_exigible': True,
                                        'payment_id': self.env.context.get('pago_id'),
                                        'invoice_idd': self.env.context.get('invoice_id'),
                                    }))
                                    print "esto es line_to_createeeeeeeeee1 ", line_to_create
                                    #line.write({'fixed':True})
                                    factura = self.env['account.invoice'].browse(self.env.context.get('invoice_id'))
                                    diferencia_tax = monto_impuesto - monto_impuesto1
                                    print "esto es diferencia_tax ", diferencia_tax
                                    #print "esto es factura.id ", factura.id
                                    print "esto es context 109", self.env.context
                                    print "esto es context 110", self.env.context.get('pago_id')
                                    mov_cambiario = self.env['account.move.line'].search([('payment_id','=',self.env.context.get('pago_id'))])
                                    print "esto es mov_cambiario ", mov_cambiario
                                    mov_cambiario = self.env['account.move.line'].search([('gain_updated','=',False),('invoice_idd','=',factura.id), ('payment_id','=',self.env.context.get('pago_id'))])
                                    #print "esto es mov_cambiario_rec ", mov_cambiario_rec
                                    #mov_cambiario = self.env['account.move.line'].browse(mov_cambiario_rec.id)
                                    #print "esto es mov_cambiario ", mov_cambiario
                                    #if mov_cambiario:
                                        #if mov_cambiario.credit > 0:
                                            #print "entra if credit"
                                            #credit = mov_cambiario.credit
                                            #print "esto es credit ", credit
                                            #credit = credit-abs(diferencia_tax)
                                            #print "esto es credit ", credit
                                            #if diferencia_tax < 0:
                                                #mov_cambiario.write({'credit':credit, 'gain_updated':True})
                                        #elif mov_cambiario.debit > 0:
                                            #print "entra if debit"
                                            #debit = mov_cambiario.debit
                                            #print "esto es debit ", debit
                                            #debit = debit - abs(diferencia_tax)
                                            #if diferencia_tax > 0:
                                                #mov_cambiario.write({'debit':debit,'gain_updated':True})
                            if len(rec_ids)<1:
                                if pago.currency_id.name != 'MXN':
                                    print "PAGO <> PESOS"
                                    line_to_create.append((0,0,{
                                            'name': '/',
                                            #'debit': asiento.credit if asiento.credit else 0.0,
                                            #'credit': asiento.debit if asiento.debit else 0.0,
                                            'debit': abs(monto_impuesto) or 0.0,
                                            'credit': 0.0,
                                            'account_id': line.tax_line_id.account_id.id,
                                            'tax_exigible': True,
                                            'payment_id': self.env.context.get('pago_id'),
                                            'invoice_idd': self.env.context.get('invoice_id'),
                                        }))
                                    print "esto es line_to_createeeeeeeeee ", line_to_create
                                    pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))
                                    print "esto es pago ", pago
                                    pay_amount = self.env.context.get('monto_pago')
                                    print "esto es pay_amount ", pay_amount
                                    pay_date = self.env.context.get('fecha_pago')
                                    print "esto es pay_date ", pay_date
                                    invoice_date_datetime = datetime.datetime.strptime(self.env.context.get('fecha_factura'), '%Y-%m-%d')
                                    print "esto es invoice_date_datetime ",invoice_date_datetime
                                    print "esto es factura pago ", pago.journal_id.currency_id
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                    ('currency_id', '=', pago.journal_id.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate = 1.0
                                    print "esto es rate ", rate
                                    pay_ori_amount = pay_amount * (1/rate)
                                    print "esto es pay_ori_amount ", pay_ori_amount
                                    invoice_date_datetime = datetime.datetime.strptime(pay_date, '%Y-%m-%d')
                                    print "esto es invoice_date_datetime ",invoice_date_datetime
                                    print "esto es factura pago ", pago.journal_id.currency_id
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                    ('currency_id', '=', pago.journal_id.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate = 1.0
                                    print "esto es rate ", rate
                                    pay_amount_new = abs(round(pay_amount * (1/rate),2))
                                    monto_pago = pay_amount_new
                                    print "esto es monto_pago ", monto_pago
                                    #monto_impuesto1 = monto_pago * tax_amount
                                    #monto_impuesto1 = (monto_pago / (1+tax_amount))* tax_amount
                                    monto_impuesto1 = (monto_pago / (1+tax_per))*tax_amount
                                    print "esto es monto_impuesto1 ",monto_impuesto1
                                    #monto_impuesto1 =  currency.with_context(date=self.env.context.get('fecha_pago')).compute(monto_impuesto1, self.env.user.company_id.currency_id)
                                    line_to_create.append((0,0,{
                                        'name': '/',
                                        #'debit': asiento.credit if asiento.credit else 0.0,
                                        #'credit': asiento.debit if asiento.debit else 0.0,
                                        'debit': 0.0,
                                        'credit': abs(monto_impuesto1) or 0.0,
                                        'account_id': line.tax_line_id.cash_basis_account.id,
                                        'tax_exigible': True,
                                        'payment_id': self.env.context.get('pago_id'),
                                        'invoice_idd': self.env.context.get('invoice_id'),
                                    }))
                                    print "esto es line_to_createeeeeeeeee1 ", line_to_create


        #PAGOS MULTIMONEDA proveedor
        if self.env.context.get('intermoneda') == True and pago.payment_type=='outbound':
            pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))
            print "entra a intermoneda = True"

            #PARA PAGOS proveedor multimoneda
            if (self.credit_move_id.journal_id.type not in ['sale', 'purchase', 'general']):
                move_id = self.debit_move_id.move_id
                if move_id:
                    factura = self.env['account.invoice'].browse(self.env.context.get('invoice_id'))
                    #impuestos= self.env['account.invoice.tax'].search([('invoice_id','=', factura.id)])
                    #for impuesto in impuestos:
                        #print"IMPUESTOOOOO MONTO",impuesto.amount
                    tax_per = 0
                    for line in move_id.line_ids:
                        if line.tax_line_id and line.tax_line_id.use_cash_basis:
                            print "DMZ LINE taxes", line.tax_line_id
                            tax_amount = (line.tax_line_id.amount/100) or 0
                            tax_per = tax_per + tax_amount
                            print "DMZ LINE taxes tax_per ", tax_per
                    for line in move_id.line_ids:
                        #if impuesto.account_id.id != line.account_id.id:
                        #    continue
                        #print "line ", line
                        currency = line.currency_id or line.company_id.currency_id
                        #if not line.fixed:
                        print"numero 1 ", line.tax_line_id
                        print"numero 2 ",line.tax_line_id.use_cash_basis
                        if line.tax_line_id and line.tax_line_id.use_cash_basis:
                            #print "ya valio"
                            #factura = self.env['account.invoice'].browse(self.env.context.get('invoice_id'))
                            #pagos_lines = self.env['advance.payment.line'].search([('invoice_id','=', factura.id)])
                            #for pago in pagos_lines:
                            #    cambio = pago.tipocambiop1
                            #    cambio1 = pago.tipocambiop
                            #    allocation1= pago.allocation1

                            #print "esto es pago en facturas de proveedor",cambio
                            #print "esto es factura cuando ya valio",factura.amount_untaxed
                            #tax_amount = (line.tax_line_id.amount/100) or 0
                            print"11111",self.credit_move_id.debit
                            print"22222",self.debit_move_id.credit
                            print"3333",self.debit_move_id.debit
                            print"4444",self.credit_move_id
                            #print"esto es tax amount",tax_amount
                            #print "dmz factura.amount_total ", factura.amount_total
                            #print "dmz factura.tipocambio ", factura.tipocambio
                            #total= factura.amount_total * (1/factura.tipocambio)
                            #print "dmz impuesto.amount ", impuesto.amount
                            #monto_pesos= (impuesto.amount/factura.tipocambio)
                            #print "dmz monto_pesos ", monto_pesos
                            #print "dmz allocation1 ", allocation1
                            #print "dmz cambio ", cambio
                            #base_amount = (allocation1*cambio)/factura.tipocambio
                            #print "esto es base_amount ", base_amount
                            #monto_impuesto = (base_amount*monto_pesos) / total
                            tax_amount = (line.tax_line_id.amount/100) or 0
                            print "esto es tax_amount ", tax_amount
                            base_amount = self.credit_move_id.debit or 0
                            print "esto es base_amount ", base_amount
                            #monto_impuesto = base_amount * tax_amount
                            #monto_impuesto = (base_amount / (1+tax_amount))*tax_amount
                            monto_impuesto = (base_amount / (1+tax_per))*tax_amount
                            print "esto es monto_impuesto ", monto_impuesto

                            print"esto es lo que sale de monto impuesto",monto_impuesto
                            rec_ids = self.env['account.move.line'].search([
                                ('payment_id', '=', self.env.context.get('pago_id')),
                                ('account_id', '=', line.tax_line_id.account_id.id),
                                ('invoice_idd', '=', self.env.context.get('invoice_id')),
                                ])


                            if len(rec_ids)<1:
                                if pago.currency_id.name == 'MXN':
                                    print "PAGO == MXN"
                                    line_to_create.append((0,0,{
                                        'name': '/',
                                        #'debit': asiento.credit if asiento.credit else 0.0,
                                        #'credit': asiento.debit if asiento.debit else 0.0,
                                        'debit': 0.0,
                                        'credit': abs(monto_impuesto) or 0.0,
                                        'account_id': line.tax_line_id.account_id.id,
                                        'tax_exigible': True,
                                        'payment_id': self.env.context.get('pago_id'),
                                        'invoice_idd': self.env.context.get('invoice_id'),
                                    }))
                                    print "esto es line_to_createeeeeeeeee ", line_to_create
                                    #monto_pago = abs(self.env.context.get('monto_pago'))
                                    pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))
                                    print "esto es pago ", pago
                                    pay_amount = self.env.context.get('monto_pago')
                                    print "esto es pay_amount ", pay_amount
                                    pay_date = self.env.context.get('fecha_pago')
                                    print "esto es pay_date ", pay_date
                                    invoice_date_datetime = datetime.datetime.strptime(self.env.context.get('fecha_factura'), '%Y-%m-%d')
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                    ('currency_id', '=', line.invoice_id.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate = 1.0
                                    print "esto es rate ", rate
                                    print "LA LINEA ANTERIOR",line_to_create[0][2]
                                    pay_ori_amount = pay_amount / (1/rate)
                                    print "esto es pay_ori_amount ", pay_ori_amount
                                    invoice_date_datetime = datetime.datetime.strptime(pay_date, '%Y-%m-%d')
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                    ('currency_id', '=', line.invoice_id.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate = 1.0
                                    print "esto es rate ", rate
                                    pay_amount_new = abs(round(pay_ori_amount * (1/rate),2))
                                    monto_pago = pay_amount_new
                                    #monto_impuesto1 = monto_pago * tax_amount

                                    #total= factura.amount_total * (1/cambio)
                                    #monto_pesos= (impuesto.amount/cambio)
                                    #base_amount = (allocation1/cambio1)
                                    #print "esto es base_amount ", base_amount
                                    #monto_impuesto1 = (base_amount*monto_pesos) / total
                                    #monto_impuesto1 = (monto_pago*tax_per) / tax_amount
                                    monto_impuesto1 = (monto_pago / (1+tax_per))*tax_amount
                                    #monto_impuesto1 =  currency.with_context(date=self.env.context.get('fecha_pago')).compute(monto_impuesto1, self.env.user.company_id.currency_id)
                                    line_to_create.append((0,0,{
                                        'name': '/',
                                        #'debit': asiento.credit if asiento.credit else 0.0,
                                        #'credit': asiento.debit if asiento.debit else 0.0,
                                        'debit': abs(monto_impuesto1) or 0.0,
                                        'credit': 0.0,
                                        'account_id': line.tax_line_id.cash_basis_account.id,
                                        'tax_exigible': True,
                                        'payment_id': self.env.context.get('pago_id'),
                                        'invoice_idd': self.env.context.get('invoice_id'),
                                    }))
                                    print "esto es line_to_createeeeeeeeee1 ", line_to_create
                                    #line.write({'fixed':True})
                                    factura = self.env['account.invoice'].browse(self.env.context.get('invoice_id'))
                                    diferencia_tax = monto_impuesto - monto_impuesto1
                                    print "esto es diferencia_tax ", diferencia_tax
                                    #print "esto es factura.id ", factura.id
                                    print "esto es context 109", self.env.context
                                    print "esto es context 110", self.env.context.get('pago_id')
                                    mov_cambiario = self.env['account.move.line'].search([('payment_id','=',self.env.context.get('pago_id'))])
                                    print "esto es mov_cambiario ", mov_cambiario
                                    mov_cambiario = self.env['account.move.line'].search([('gain_updated','=',False),('invoice_idd','=',factura.id), ('payment_id','=',self.env.context.get('pago_id'))])

                            if len(rec_ids)<1:
                                if pago.currency_id.name != 'MXN':
                                    print "PAGO <> MXN"
                                    line_to_create.append((0,0,{
                                            'name': '/',
                                            #'debit': asiento.credit if asiento.credit else 0.0,
                                            #'credit': asiento.debit if asiento.debit else 0.0,
                                            'credit': abs(monto_impuesto) or 0.0,
                                            'debit': 0.0,
                                            'account_id': line.tax_line_id.account_id.id,
                                            'tax_exigible': True,
                                            'payment_id': self.env.context.get('pago_id'),
                                            'invoice_idd': self.env.context.get('invoice_id'),
                                        }))
                                    print "esto es line_to_createeeeeeeeee ", line_to_create
                                    pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))
                                    print "esto es pago ", pago
                                    pay_amount = self.env.context.get('monto_pago')
                                    print "esto es pay_amount ", pay_amount
                                    pay_date = self.env.context.get('fecha_pago')
                                    print "esto es pay_date ", pay_date
                                    invoice_date_datetime = datetime.datetime.strptime(self.env.context.get('fecha_factura'), '%Y-%m-%d')
                                    print "esto es invoice_date_datetime ",invoice_date_datetime
                                    print "esto es factura pago ", pago.journal_id.currency_id
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                    ('currency_id', '=', pago.journal_id.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate = 1.0
                                    print "esto es rate ", rate
                                    print "LA LINEA ANTERIOR",line_to_create[0][2]
                                    #line_to_create[0][2].update({'debit':line_to_create[0][2].get('debit')*(1/rate)})
                                    pay_ori_amount = pay_amount * (1/rate)
                                    print "esto es pay_ori_amount ", pay_ori_amount
                                    invoice_date_datetime = datetime.datetime.strptime(pay_date, '%Y-%m-%d')
                                    print "esto es invoice_date_datetime ",invoice_date_datetime
                                    print "esto es factura pago ", pago.journal_id.currency_id
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                    ('currency_id', '=', pago.journal_id.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate = 1.0
                                    print "esto es rate ", rate
                                    pay_amount_new = abs(round(pay_amount * (1/rate),2))
                                    monto_pago = pay_amount_new
                                    print "esto es monto_pago ", monto_pago
                                    #monto_impuesto1 = monto_pago * tax_amount
                                    monto_impuesto1 = (monto_pago / (1+tax_per))* tax_amount
                                    print "esto es monto_impuesto1 ",monto_impuesto1
                                    #monto_impuesto1 =  currency.with_context(date=self.env.context.get('fecha_pago')).compute(monto_impuesto1, self.env.user.company_id.currency_id)
                                    line_to_create.append((0,0,{
                                        'name': '/',
                                        #'debit': asiento.credit if asiento.credit else 0.0,
                                        #'credit': asiento.debit if asiento.debit else 0.0,
                                        'credit': 0.0,
                                        'debit': abs(monto_impuesto1) or 0.0,
                                        'account_id': line.tax_line_id.cash_basis_account.id,
                                        'tax_exigible': True,
                                        'payment_id': self.env.context.get('pago_id'),
                                        'invoice_idd': self.env.context.get('invoice_id'),
                                    }))
                                    print "esto es line_to_createeeeeeeeee1 ", line_to_create















            if (self.debit_move_id.journal_id.type not in ['sale', 'purchase', 'general']):
                print "entra credit_move proveedor"
                print "entra credit_move_id ",self.credit_move_id
                print "entra debit_move_id ",self.debit_move_id
                move_id = self.credit_move_id.move_id
                print "esto es move_id",move_id
                if move_id:
                    tax_per = 0
                    for line in move_id.line_ids:
                        if line.tax_line_id and line.tax_line_id.use_cash_basis:
                            print "DMZ LINE taxes", line.tax_line_id
                            tax_amount = (line.tax_line_id.amount/100) or 0
                            tax_per = tax_per + tax_amount
                            print "DMZ LINE taxes tax_per ", tax_per
                    for line in move_id.line_ids:
                        currency = line.currency_id or line.company_id.currency_id
                        #if not line.fixed:
                        print"numero 1 ", line.tax_line_id
                        print"numero 2 ",line.tax_line_id.use_cash_basis
                        if line.tax_line_id and line.tax_line_id.use_cash_basis:
                            tax_amount = (line.tax_line_id.amount/100)
                            base_amount = self.debit_move_id.debit


                            print"11111",self.credit_move_id.debit
                            print"22222",self.debit_move_id.credit
                            print"3333",self.debit_move_id.debit
                            print"4444",self.credit_move_id
                            print "esto es base_amount ", base_amount
                            print"esto es tax amount",tax_amount
                            #monto_impuesto = base_amount * tax_amount
                            monto_impuesto = (base_amount / (1+tax_per))*tax_amount
                            print "esto es monto_impuesto ", monto_impuesto
                            rec_ids = self.env['account.move.line'].search([
                                ('payment_id', '=', self.env.context.get('pago_id')),
                                ('account_id', '=', line.tax_line_id.account_id.id),
                                ('invoice_idd', '=', self.env.context.get('invoice_id')),
                                ])




                            if len(rec_ids)<1:
                                if pago.currency_id.name == 'MXN':
                                    line_to_create.append((0,0,{
                                        'name': '/',
                                        #'debit': asiento.credit if asiento.credit else 0.0,
                                        #'credit': asiento.debit if asiento.debit else 0.0,
                                        'debit': 0.0,
                                        'credit': abs(monto_impuesto) or 0.0,
                                        'account_id': line.tax_line_id.account_id.id,
                                        'tax_exigible': True,
                                        'payment_id': self.env.context.get('pago_id'),
                                        'invoice_idd': self.env.context.get('invoice_id'),
                                    }))
                                    print "esto es line_to_createeeeeeeeee ", line_to_create
                                    #monto_pago = abs(self.env.context.get('monto_pago'))
                                    pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))
                                    print "esto es pago ", pago
                                    pay_amount = self.env.context.get('monto_pago')
                                    print "esto es pay_amount ", pay_amount
                                    pay_date = self.env.context.get('fecha_pago')
                                    print "esto es pay_date ", pay_date
                                    invoice_date_datetime = datetime.datetime.strptime(self.env.context.get('fecha_factura'), '%Y-%m-%d')
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                    ('currency_id', '=', line.invoice_id.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate = 1.0
                                    print "esto es rate ", rate
                                    pay_ori_amount = pay_amount / (1/rate)
                                    print "esto es pay_ori_amount ", pay_ori_amount
                                    invoice_date_datetime = datetime.datetime.strptime(pay_date, '%Y-%m-%d')
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                    ('currency_id', '=', line.invoice_id.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate = 1.0
                                    print "esto es rate ", rate
                                    pay_amount_new = abs(round(pay_ori_amount * (1/rate),2))
                                    monto_pago = pay_amount_new
                                    #monto_impuesto1 = monto_pago * tax_amount
                                    #monto_impuesto1 = (monto_pago / (1+tax_amount))*tax_amount
                                    monto_impuesto1 = (monto_pago / (1+tax_per))*tax_amount
                                    #monto_impuesto1 =  currency.with_context(date=self.env.context.get('fecha_pago')).compute(monto_impuesto1, self.env.user.company_id.currency_id)
                                    line_to_create.append((0,0,{
                                        'name': '/',
                                        #'debit': asiento.credit if asiento.credit else 0.0,
                                        #'credit': asiento.debit if asiento.debit else 0.0,
                                        'debit': abs(monto_impuesto1) or 0.0,
                                        'credit': 0.0,
                                        'account_id': line.tax_line_id.cash_basis_account.id,
                                        'tax_exigible': True,
                                        'payment_id': self.env.context.get('pago_id'),
                                        'invoice_idd': self.env.context.get('invoice_id'),
                                    }))
                                    print "esto es line_to_createeeeeeeeee1 ", line_to_create
                                    #line.write({'fixed':True})
                                    factura = self.env['account.invoice'].browse(self.env.context.get('invoice_id'))
                                    diferencia_tax = monto_impuesto - monto_impuesto1
                                    print "esto es diferencia_tax ", diferencia_tax
                                    #print "esto es factura.id ", factura.id
                                    print "esto es context 109", self.env.context
                                    print "esto es context 110", self.env.context.get('pago_id')
                                    mov_cambiario = self.env['account.move.line'].search([('payment_id','=',self.env.context.get('pago_id'))])
                                    print "esto es mov_cambiario ", mov_cambiario
                                    mov_cambiario = self.env['account.move.line'].search([('gain_updated','=',False),('invoice_idd','=',factura.id), ('payment_id','=',self.env.context.get('pago_id'))])
                                    #print "esto es mov_cambiario_rec ", mov_cambiario_rec
                                    #mov_cambiario = self.env['account.move.line'].browse(mov_cambiario_rec.id)
                                    #print "esto es mov_cambiario ", mov_cambiario
                                    #if mov_cambiario:
                                        #if mov_cambiario.credit > 0:
                                            #print "entra if credit"
                                            #credit = mov_cambiario.credit
                                            #print "esto es credit ", credit
                                            #credit = credit-abs(diferencia_tax)
                                            #print "esto es credit ", credit
                                            #if diferencia_tax < 0:
                                                #mov_cambiario.write({'credit':credit, 'gain_updated':True})
                                        #elif mov_cambiario.debit > 0:
                                            #print "entra if debit"
                                            #debit = mov_cambiario.debit
                                            #print "esto es debit ", debit
                                            #debit = debit - abs(diferencia_tax)
                                            #if diferencia_tax > 0:
                                                #mov_cambiario.write({'debit':debit,'gain_updated':True})
                            if len(rec_ids)<1:
                                if pago.currency_id.name != 'MXN':
                                    line_to_create.append((0,0,{
                                            'name': '/',
                                            #'debit': asiento.credit if asiento.credit else 0.0,
                                            #'credit': asiento.debit if asiento.debit else 0.0,
                                            'debit': 0.0,
                                            'credit': abs(monto_impuesto) or 0.0,
                                            'account_id': line.tax_line_id.account_id.id,
                                            'tax_exigible': True,
                                            'payment_id': self.env.context.get('pago_id'),
                                            'invoice_idd': self.env.context.get('invoice_id'),
                                        }))
                                    print "esto es line_to_createeeeeeeeee ", line_to_create
                                    pago = self.env['account.payment'].browse(self.env.context.get('pago_id'))
                                    print "esto es pago ", pago
                                    pay_amount = self.env.context.get('monto_pago')
                                    print "esto es pay_amount ", pay_amount
                                    pay_date = self.env.context.get('fecha_pago')
                                    print "esto es pay_date ", pay_date
                                    invoice_date_datetime = datetime.datetime.strptime(self.env.context.get('fecha_factura'), '%Y-%m-%d')
                                    print "esto es invoice_date_datetime ",invoice_date_datetime
                                    print "esto es factura pago ", pago.journal_id.currency_id
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                    ('currency_id', '=', pago.journal_id.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate = 1.0
                                    print "esto es rate ", rate
                                    pay_ori_amount = pay_amount * (1/rate)
                                    print "esto es pay_ori_amount ", pay_ori_amount
                                    invoice_date_datetime = datetime.datetime.strptime(pay_date, '%Y-%m-%d')
                                    print "esto es invoice_date_datetime ",invoice_date_datetime
                                    print "esto es factura pago ", pago.journal_id.currency_id
                                    rate_obj = self.env['res.currency.rate']
                                    rate_rec = rate_obj.search([
                                    ('currency_id', '=', pago.journal_id.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                    if rate_rec:
                                        rate = rate_obj.browse(rate_rec.id).rate
                                    else:
                                        rate = 1.0
                                    print "esto es rate ", rate
                                    pay_amount_new = abs(round(pay_amount * (1/rate),2))
                                    monto_pago = pay_amount_new
                                    print "esto es monto_pago ", monto_pago
                                    #monto_impuesto1 = monto_pago * tax_amount
                                    #monto_impuesto1 = (monto_pago / (1+tax_amount))* tax_amount
                                    monto_impuesto1 = (monto_pago / (1+tax_per))*tax_amount
                                    print "esto es monto_impuesto1 ",monto_impuesto1
                                    #monto_impuesto1 =  currency.with_context(date=self.env.context.get('fecha_pago')).compute(monto_impuesto1, self.env.user.company_id.currency_id)
                                    line_to_create.append((0,0,{
                                        'name': '/',
                                        #'debit': asiento.credit if asiento.credit else 0.0,
                                        #'credit': asiento.debit if asiento.debit else 0.0,
                                        'debit': abs(monto_impuesto1) or 0.0,
                                        'credit': 0.0,
                                        'account_id': line.tax_line_id.cash_basis_account.id,
                                        'tax_exigible': True,
                                        'payment_id': self.env.context.get('pago_id'),
                                        'invoice_idd': self.env.context.get('invoice_id'),
                                    }))
                                    print "esto es line_to_createeeeeeeeee1 ", line_to_create

        print "esto es line_to_createeeeeeeeee xxxxxxxxxxxxxxxx", line_to_create
        return line_to_create, move_date


    def create_tax_cash_basis_entry(self, value_before_reconciliation):
        print "esto es context desde create_tax_cash_basis_entry", self.env.context
        line_to_create, move_date = self._get_tax_cash_basis_lines(value_before_reconciliation)
        print "esto es line_to_create ", line_to_create
        print "esto es move_date ", move_date

        if len(line_to_create) > 0:
            # Check if company_journal for cash basis is set if not, raise exception
            if not self.company_id.tax_cash_basis_journal_id:
                raise UserError(_('There is no tax cash basis journal defined '
                                  'for this company: "%s" \nConfigure it in Accounting/Configuration/Settings') %
                                (self.company_id.name))
            move_vals = {
                'journal_id': self.company_id.tax_cash_basis_journal_id.id,
                'line_ids': line_to_create,
                'tax_cash_basis_rec_id': self.id
            }
            # The move date should be the maximum date between payment and invoice (in case
            # of payment in advance). However, we should make sure the move date is not
            # recorded after the period lock date as the tax statement for this period is
            # probably already sent to the estate.
            if move_date > self.company_id.period_lock_date:
                move_vals['date'] = move_date
            for linea in line_to_create:
                linea[2].update({'move_id':self.env.context.get('pago_poliza_id')})
                line = self.env['account.move.line'].create(linea[2])
                print "esto es line ", line

    @api.model
    def create(self, vals):

        print "****************ENTRANDO A CREATE"
        
        aml = []
        if vals.get('debit_move_id', False):
            aml.append(vals['debit_move_id'])
        if vals.get('credit_move_id', False):
            aml.append(vals['credit_move_id'])
        #print "esto es aml ", aml
        # Get value of matched percentage from both move before reconciliating
        lines = self.env['account.move.line'].browse(aml)
        #VDE lines trae las partidas del cliente de la poliza del pago y la partida del cliente de la factura
        #print "esto es lines ", lines
        value_before_reconciliation = {}
        for line in lines:
            print "****** LINE ", line
            print "****** LINE ", line.amount_residual
            if not value_before_reconciliation.get(line.move_id.id, False):
                value_before_reconciliation[line.move_id.id] = line.move_id.matched_percentage
        #print "esto es value_before_reconciliation ", value_before_reconciliation
        # Reconcile
        res = super(AccountPartialReconcileCashBasis, self).create(vals)
        # eventually create a tax cash basis entry
        res.create_tax_cash_basis_entry(value_before_reconciliation)
        return res
        print "ALGO DMZZZZZZZZZZZZZZZZZZZ"
        if self.env.context.get('intermoneda')==False:
            print "****************intermoneda = False"
            print "****************ENTRANDO A CREATE"
            aml = []
            if vals.get('debit_move_id', False):
                aml.append(vals['debit_move_id'])
            if vals.get('credit_move_id', False):
                aml.append(vals['credit_move_id'])
            #print "esto es aml ", aml
            # Get value of matched percentage from both move before reconciliating
            lines = self.env['account.move.line'].browse(aml)
            #VDE lines trae las partidas del cliente de la poliza del pago y la partida del cliente de la factura
            #print "esto es lines ", lines
            value_before_reconciliation = {}
            for line in lines:
                if not value_before_reconciliation.get(line.move_id.id, False):
                    value_before_reconciliation[line.move_id.id] = line.move_id.matched_percentage
            #print "esto es value_before_reconciliation ", value_before_reconciliation
            # Reconcile
            res = super(AccountPartialReconcileCashBasis, self).create(vals)
            # eventually create a tax cash basis entry
            res.create_tax_cash_basis_entry(value_before_reconciliation)
            return res
        elif self.env.context.get('intermoneda')==True:
            print "****************intermoneda = True"
            print "****************ENTRANDO A CREATE"
            aml = []
            if vals.get('debit_move_id', False):
                aml.append(vals['debit_move_id'])
            if vals.get('credit_move_id', False):
                aml.append(vals['credit_move_id'])
            #print "esto es aml ", aml
            # Get value of matched percentage from both move before reconciliating
            lines = self.env['account.move.line'].browse(aml)
            #VDE lines trae las partidas del cliente de la poliza del pago y la partida del cliente de la factura
            #print "esto es lines ", lines
            value_before_reconciliation = {}
            for line in lines:
                if not value_before_reconciliation.get(line.move_id.id, False):
                    value_before_reconciliation[line.move_id.id] = line.move_id.matched_percentage
            #print "esto es value_before_reconciliation ", value_before_reconciliation
            # Reconcile
            res = super(AccountPartialReconcileCashBasis, self).create(vals)
            # eventually create a tax cash basis entry
            res.create_tax_cash_basis_entry(value_before_reconciliation)
            return res

    @api.model
    def compute_full_after_batch_reconcile(self):
        print "entra a compute_full_after_batch_reconcile"
        """ After running the manual reconciliation wizard and making full reconciliation, we need to run this method to create
            potentially an exchange rate entry that will balance the remaining amount_residual_currency (possibly several aml).

            This ensure that all aml in the full reconciliation are reconciled (amount_residual = amount_residual_currency = 0).
        """
        total_debit = 0
        total_credit = 0
        total_amount_currency = 0
        currency = False
        aml_to_balance_currency = self.env['account.move.line']
        partial_rec_set = self.env['account.partial.reconcile']
        aml_id = False
        partial_rec_id = False
        maxdate = None
        for aml in self:
            total_debit += aml.debit
            total_credit += aml.credit
            if aml.amount_residual_currency:
                aml_to_balance_currency |= aml
            maxdate = max(aml.date, maxdate)
            if not currency and aml.currency_id:
                currency = aml.currency_id
            if aml.currency_id and aml.currency_id == currency:
                total_amount_currency += aml.amount_currency
            partial_rec_set |= aml.matched_debit_ids | aml.matched_credit_ids

        if currency and aml_to_balance_currency:
            aml = aml_to_balance_currency[0]
            #eventually create journal entries to book the difference due to foreign currency's exchange rate that fluctuates
            partial_rec = aml.credit and aml.matched_debit_ids[0] or aml.matched_credit_ids[0]

        if currency and aml_to_balance_currency:
            aml = aml_to_balance_currency[0]
            #eventually create journal entries to book the difference due to foreign currency's exchange rate that fluctuates
            partial_rec = aml.credit and aml.matched_debit_ids[0] or aml.matched_credit_ids[0]
            aml_id, partial_rec_id = partial_rec.with_context(skip_full_reconcile_check=True).create_exchange_rate_entry(aml_to_balance_currency, 0.0, total_amount_currency, currency, maxdate)
            self |= aml_id
            partial_rec_set |= partial_rec_id
            total_amount_currency += aml_id.amount_currency

        partial_rec_ids = [x.id for x in list(partial_rec_set)]
        #if the total debit and credit are equal, and the total amount in currency is 0, the reconciliation is full
        digits_rounding_precision = self[0].company_id.currency_id.rounding
        if float_compare(total_debit, total_credit, precision_rounding=digits_rounding_precision) == 0 \
          and (not currency or float_is_zero(total_amount_currency, precision_rounding=currency.rounding)):
            #in that case, mark the reference on the partial reconciliations and the entries
            self.env['account.full.reconcile'].with_context(check_move_validity=False).create({
                'partial_reconcile_ids': [(6, 0, partial_rec_ids)],
                'reconciled_line_ids': [(6, 0, self.ids)],
                'exchange_move_id': aml_id.move_id.id if aml_id else False,
                'exchange_partial_rec_id': partial_rec_id.id if partial_rec_id else False})


    def create_exchange_rate_entry(self, aml_to_fix, amount_diff, diff_in_currency, currency, move_date):
        print "esto es context desde create_exchange_rate_entry", self.env.context
        print "entra a create_exchange_rate_entry"
        """ Automatically create a journal entry to book the exchange rate difference.
            That new journal entry is made in the company `currency_exchange_journal_id` and one of its journal
            items is matched with the other lines to balance the full reconciliation.
        """
        for rec in self:
            if not rec.company_id.currency_exchange_journal_id:
                raise UserError(_("You should configure the 'Exchange Rate Journal' in the accounting settings, to manage automatically the booking of accounting entries related to differences between exchange rates."))
            if not rec.company_id.income_currency_exchange_account_id.id:
                raise UserError(_("You should configure the 'Gain Exchange Rate Account' in the accounting settings, to manage automatically the booking of accounting entries related to differences between exchange rates."))
            if not rec.company_id.expense_currency_exchange_account_id.id:
                raise UserError(_("You should configure the 'Loss Exchange Rate Account' in the accounting settings, to manage automatically the booking of accounting entries related to differences between exchange rates."))
            move_vals = {'journal_id': rec.company_id.currency_exchange_journal_id.id}


            # The move date should be the maximum date between payment and invoice (in case
            # of payment in advance). However, we should make sure the move date is not
            # recorded after the end of year closing.
            if move_date > rec.company_id.fiscalyear_lock_date:
                move_vals['date'] = move_date
            move = rec.env['account.move'].create(move_vals)
            amount_diff = rec.company_id.currency_id.round(amount_diff)
            diff_in_currency = currency.round(diff_in_currency)
            line_to_reconcile = rec.env['account.move.line'].with_context(check_move_validity=False).create({
                'name': _('Currency exchange rate difference'),
                'debit': amount_diff < 0 and -amount_diff or 0.0,
                'credit': amount_diff > 0 and amount_diff or 0.0,
                'account_id': rec.debit_move_id.account_id.id,
                'move_id': move.id,
                'currency_id': currency.id,
                'amount_currency': -diff_in_currency,
                'partner_id': rec.debit_move_id.partner_id.id,
            })
            rec.env['account.move.line'].create({
                'name': _('Currency exchange rate difference'),
                'debit': amount_diff > 0 and amount_diff or 0.0,
                'credit': amount_diff < 0 and -amount_diff or 0.0,
                'account_id': amount_diff > 0 and rec.company_id.currency_exchange_journal_id.default_debit_account_id.id or rec.company_id.currency_exchange_journal_id.default_credit_account_id.id,
                'move_id': move.id,
                'currency_id': currency.id,
                'amount_currency': diff_in_currency,
                'partner_id': rec.debit_move_id.partner_id.id,
            })
            for aml in aml_to_fix:
                partial_rec = rec.env['account.partial.reconcile'].create({
                    'debit_move_id': aml.credit and line_to_reconcile.id or aml.id,
                    'credit_move_id': aml.debit and line_to_reconcile.id or aml.id,
#                    'amount': abs(aml.amount_residual),
                    'amount': aml.amount_residual,
#                    'amount_currency': abs(aml.amount_residual_currency),
                    'amount_currency': aml.amount_residual_currency,
                    'currency_id': currency.id,
                })
            move.post()
        return line_to_reconcile, partial_rec

class AccountPartialReconcile(models.Model):
    _inherit = 'account.partial.reconcile'


    def _fix_multiple_exchange_rates_diff(self, amls_to_fix, amount_diff, diff_in_currency, currency, move, total=False):

        if total==False:
            signal = 0
            dif = 0
            print "esto es context from _fix_multiple_exchange_rates_diff", self.env.context
            print "_fix_multiple_exchange_rates_diff ", move
            self.ensure_one()
            move_lines = self.env['account.move.line'].with_context(check_move_validity=False)
            partial_reconciles = self.with_context(skip_full_reconcile_check=True)
            amount_diff = self.company_id.currency_id.round(amount_diff)
            diff_in_currency = currency.round(diff_in_currency)
            print "esto es amount_diff ", amount_diff
            print "esto es len ", len(amls_to_fix)
            print "esto es amls_to_fix ", amls_to_fix
            for aml in amls_to_fix:
                if aml.fixed == False:
                    print "esto es aml ", aml
                    print "esto es aml.amount_residual ",aml.amount_residual
                    residual = aml.amount_residual
                    print "Esto es lines_fixed ",
                    ids_linea_partner=self.env['account.move.line'].search([('move_id','=',self.env.context.get('pago_poliza_id'))])
                    print "esto es ids_linea_partner ", ids_linea_partner
                    #ids_linea_partner=self.env['account.move.line'].search([('fixed','=',False),('account_id','=',self.debit_move_id.account_id.id),('move_id','=',self.env.context.get('pago_poliza_id'))])
                    #print "esto es ids_linea_partner ", ids_linea_partner
                    #for id_linea_partner in ids_linea_partner:
                        #print "esto es id_linea_partner ",id_linea_partner
                        ##pago_factura = currency.with_context(date=self.env.context.get('fecha_factura')).compute(amount_diff, self.company_id.currency_id)
                        ##print "esto es pago_factura ", pago_factura
                        ###total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(self.amount, self.company_id.currency_id)
                        #linea_partner=self.env['account.move.line'].browse(id_linea_partner.id)
                        #pago_factura = currency.with_context(date=self.env.context.get('fecha_factura')).compute(abs(linea_partner.amount_currency), self.company_id.currency_id)
                        #print "esto es pago_factura ", pago_factura
                        #linea_partner.write({'reconciled':False})
                        #if linea_partner.debit > 0:
                            #debito = linea_partner.debit
                            #print "esto es debito ", debito
                            #linea_partner.write({'debit':pago_factura})
                        #if linea_partner.credit > 0:
                            #credito = linea_partner.credit
                            #print "esto es credito ", credito
                            #linea_partner.write({'credit':pago_factura})
                        #linea_partner.write({'reconciled':True,'fixed':True})
                        #print "esto es linea partnerrrr ", linea_partner
                    pago_factura = currency.with_context(date=self.env.context.get('fecha_factura')).compute(amount_diff, self.company_id.currency_id)
                    pago_pago = currency.with_context(date=self.env.context.get('fecha_pago')).compute(amount_diff, self.company_id.currency_id)
                    if pago_factura and pago_pago:
                        print "todo esta bien"
                    else:
                        raise UserError(_('No se encuenta registro de tipo de cambio, puede ser para la fecha de la factura o la fecha del pago!'))
                    print "esto es pago_pago ", pago_pago
                    #dif = pago_factura - pago_pago
                    if self.env['account.payment'].browse(self.env.context.get('pago_id')).payment_type == 'inbound':
                        if pago_pago < pago_factura:
                            dif = pago_factura - pago_pago
                            signal = 1
                        if pago_pago > pago_factura:
                            dif = pago_pago - pago_factura
                            signal = -1
                        print "esto es signal ", signal
                        monto_moneda_cambiaria = currency.with_context(date=self.env.context.get('fecha_pago')).compute(dif,self.company_id.currency_id)
                        print "esto es monto_moneda cambiaria ", monto_moneda_cambiaria

                        if monto_moneda_cambiaria==0:
                            print"ya no hace nada"
                        else:
                            monto_moneda_cambiaria = abs(dif)/(1/(abs(dif) / abs(monto_moneda_cambiaria)))
                        print "esto es monto_moneda cambiaria ", monto_moneda_cambiaria
                        vals = {
                            'name': _('Currency exchange rate difference'),
                            'debit': signal > 0 and dif or 0.0,
                            'credit': signal < 0 and dif or 0.0,
                            'account_id': signal > 0 and self.company_id.currency_exchange_journal_id.default_debit_account_id.id or self.company_id.currency_exchange_journal_id.default_credit_account_id.id,
                            'move_id': self.env.context.get('pago_poliza_id'),
                            #'move_id': move.id,
                            'payment_id': self.env.context.get('pago_id'),
                            'currency_id': currency.id,
                            #'amount_currency': aml.amount_residual_currency,
                            #'amount_currency': signal > 0 and monto_moneda_cambiaria or signal <0 and -monto_moneda_cambiaria or 0.0,
                            'amount_currency': 0.0,
                            'partner_id': self.debit_move_id.partner_id.id,
                            'date_maturity': datetime.datetime.today().strftime('%Y-%m-%d'),
                            'invoice_idd': aml.invoice_id.id,
                            }
                        print "esto es vals ", vals
                        #move_lines.create({
                            #'name': _('Currency exchange rate difference'),
                            #'debit': signal > 0 and dif or 0.0,
                            #'credit': signal < 0 and dif or 0.0,
                            #'account_id': signal > 0 and self.company_id.currency_exchange_journal_id.default_debit_account_id.id or self.company_id.currency_exchange_journal_id.default_credit_account_id.id,
                            #'move_id': self.env.context.get('pago_poliza_id'),
                            #'move_id': move.id,
                            #'payment_id': self.env.context.get('pago_id'),
                            #'currency_id': currency.id,
                            #'amount_currency': aml.amount_residual_currency,
                            #'amount_currency': signal > 0 and monto_moneda_cambiaria or signal <0 and -monto_moneda_cambiaria or 0.0,
                            #'amount_currency': 0.0,
                            #'partner_id': self.debit_move_id.partner_id.id,
                            #'date_maturity': datetime.datetime.today().strftime('%Y-%m-%d'),
                            #'invoice_idd': aml.invoice_id.id,
                            #})
                        print "esto es move_lines ", move_lines


                    if self.env['account.payment'].browse(self.env.context.get('pago_id')).payment_type == 'outbound':
                        if pago_pago < pago_factura:
                            dif = pago_factura - pago_pago
                            signal = 1
                        if pago_pago > pago_factura:
                            dif = pago_pago - pago_factura
                            signal = -1
                        print "esto es signal ", signal
                        monto_moneda_cambiaria = currency.with_context(date=self.env.context.get('fecha_pago')).compute(dif,self.company_id.currency_id)
                        print "esto es monto_moneda cambiaria ", monto_moneda_cambiaria

                        if monto_moneda_cambiaria==0:
                            print"ya no hace nada"
                        else:
                            monto_moneda_cambiaria = abs(dif)/(1/(abs(dif) / abs(monto_moneda_cambiaria)))
                        print "esto es monto_moneda cambiaria ", monto_moneda_cambiaria
                        vals = {
                            'name': _('Currency exchange rate difference'),
                            'debit': signal > 0 and dif or 0.0,
                            'credit': signal < 0 and dif or 0.0,
                            'account_id': signal > 0 and self.company_id.currency_exchange_journal_id.default_debit_account_id.id or self.company_id.currency_exchange_journal_id.default_credit_account_id.id,
                            'move_id': self.env.context.get('pago_poliza_id'),
                            #'move_id': move.id,
                            'payment_id': self.env.context.get('pago_id'),
                            'currency_id': currency.id,
                            #'amount_currency': aml.amount_residual_currency,
                            #'amount_currency': signal > 0 and monto_moneda_cambiaria or signal <0 and -monto_moneda_cambiaria or 0.0,
                            'amount_currency': 0.0,
                            'partner_id': self.debit_move_id.partner_id.id,
                            'date_maturity': datetime.datetime.today().strftime('%Y-%m-%d'),
                            'invoice_idd': aml.invoice_id.id,
                            }
                        print "esto es vals ", vals
                        #move_lines.create({
                            #'name': _('Currency exchange rate difference'),
                            #'debit': signal > 0 and dif or 0.0,
                            #'credit': signal < 0 and dif or 0.0,
                            #'account_id': signal > 0 and self.company_id.currency_exchange_journal_id.default_debit_account_id.id or self.company_id.currency_exchange_journal_id.default_credit_account_id.id,
                            #'move_id': self.env.context.get('pago_poliza_id'),
                            #'move_id': move.id,
                            #'payment_id': self.env.context.get('pago_id'),
                            #'currency_id': currency.id,
                            #'amount_currency': aml.amount_residual_currency,
                            #'amount_currency': signal > 0 and monto_moneda_cambiaria or signal <0 and -monto_moneda_cambiaria or 0.0,
                            #'amount_currency': 0.0,
                            #'partner_id': self.debit_move_id.partner_id.id,
                            #'date_maturity': datetime.datetime.today().strftime('%Y-%m-%d'),
                            #'invoice_idd': aml.invoice_id.id,
                            #})
                        print "esto es move_lines ", move_lines

            partial_reconciles._compute_partial_lines()
            return move_lines, partial_reconciles


    def _compute_partial_lines(self):
        if self.env.context.get('intermoneda')==False:
            print "esto es context desde _compute_partial_lines ", self.env.context
            print "entra a _compute_partial_lines"
            if self._context.get('skip_full_reconcile_check'):
                #when running the manual reconciliation wizard, don't check the partials separately for full
                #reconciliation or exchange rate because it is handled manually after the whole processing
                return self
            #check if the reconcilation is full
            #first, gather all journal items involved in the reconciliation just created
            partial_rec_set = OrderedDict.fromkeys([x for x in self])
            aml_set = aml_to_balance = self.env['account.move.line']
            total_debit = 0
            total_credit = 0
            total_amount_currency = 0
            #make sure that all partial reconciliations share the same secondary currency otherwise it's not
            #possible to compute the exchange difference entry and it has to be done manually.
            currency = list(partial_rec_set)[0].currency_id
            maxdate = None
            print "esto es partial_rec_set ", partial_rec_set
            print "esto es aml_set ", aml_set
            for partial_rec in partial_rec_set:
                print "entra al primer for"
                if partial_rec.currency_id != currency:
                    print "entra partial_rec.currency_id != currency:"
                    #no exchange rate entry will be created
                    currency = None
                for aml in [partial_rec.debit_move_id, partial_rec.credit_move_id]:
                    print "entra sgundo for "
                    if aml not in aml_set:
                        if aml.amount_residual or aml.amount_residual_currency:
                            aml_to_balance |= aml
                        maxdate = max(aml.date, maxdate)
                        total_debit += aml.debit
                        total_credit += aml.credit
                        aml_set |= aml
                        if aml.currency_id and aml.currency_id == currency:
                            total_amount_currency += aml.amount_currency
                        elif partial_rec.currency_id and partial_rec.currency_id == currency:
                            #if the aml has no secondary currency but is reconciled with other journal item(s) in secondary currency, the amount
                            #currency is recorded on the partial rec and in order to check if the reconciliation is total, we need to convert the
                            #aml.balance in that foreign currency
                            total_amount_currency += aml.company_id.currency_id.with_context(date=aml.date).compute(aml.balance, partial_rec.currency_id)
                    for x in aml.matched_debit_ids | aml.matched_credit_ids:
                        partial_rec_set[x] = None
            partial_rec_ids = [x.id for x in partial_rec_set.keys()]
            aml_ids = aml_set.ids
            #then, if the total debit and credit are equal, or the total amount in currency is 0, the reconciliation is full
            digits_rounding_precision = aml_set[0].company_id.currency_id.rounding
            print "esto es currency ", currency
            print "esto es total_amount_currency ",total_amount_currency
            #print "esto es currency.rounding ",currency.rounding
            print "esto es total_debit ",total_debit
            print "esto es total_credit ",total_credit
            #print "esto es float_is_zero ", float_is_zero(total_amount_currency, precision_rounding=currency.rounding)
            #print "esto es float_comparte ", float_compare(total_debit, total_credit, precision_rounding=digits_rounding_precision)
            #if (currency and float_is_zero(total_amount_currency, precision_rounding=currency.rounding)) or float_compare(total_debit, total_credit, precision_rounding=digits_rounding_precision) == 0:
                #print "entra a if linea 452"
                #exchange_move_id = False
                #exchange_partial_rec_id = False
                #print "esto es currency ", currency
                #print "esto es aml_to_balance ", aml_to_balance
                #if currency and aml_to_balance:
                    #exchange_move=False
                    #rate_diff_amls, rate_diff_partial_recs = partial_rec._fix_multiple_exchange_rates_diff(aml_to_balance, abs(self.env.context.get('monto_pago')), total_amount_currency, currency, exchange_move, False)
                    #aml_ids += rate_diff_amls.ids
                    #partial_rec_ids += rate_diff_partial_recs.ids
                    ##exchange_move.post()
                    ##exchange_move_id = exchange_move.id
                    #exchange_partial_rec_id = rate_diff_partial_recs[-1:].id

            #else:
                #print "entra al else linea 477"
                #exchange_move_id = False
                #exchange_partial_rec_id = False
                #print "esto es currency ", currency
                #print "esto es aml_to_balance ", aml_to_balance
                #if currency and aml_to_balance:
                    #exchange_move=False
                    #rate_diff_amls, rate_diff_partial_recs = partial_rec._fix_multiple_exchange_rates_diff(aml_to_balance, abs(self.env.context.get('monto_pago')), total_amount_currency, currency, exchange_move, False)
                    #aml_ids += rate_diff_amls.ids
                    #partial_rec_ids += rate_diff_partial_recs.ids
                    ##exchange_move.post()
                    ##exchange_move_id = exchange_move.id
                    #exchange_partial_rec_id = rate_diff_partial_recs[-1:].id
        elif self.env.context.get('intermoneda')==True:
            print "esto es context desde _compute_partial_lines ", self.env.context
            print "entra a _compute_partial_lines"
            if self._context.get('skip_full_reconcile_check'):
                #when running the manual reconciliation wizard, don't check the partials separately for full
                #reconciliation or exchange rate because it is handled manually after the whole processing
                return self
            #check if the reconcilation is full
            #first, gather all journal items involved in the reconciliation just created
            partial_rec_set = OrderedDict.fromkeys([x for x in self])
            aml_set = aml_to_balance = self.env['account.move.line']
            total_debit = 0
            total_credit = 0
            total_amount_currency = 0
            #make sure that all partial reconciliations share the same secondary currency otherwise it's not
            #possible to compute the exchange difference entry and it has to be done manually.
            currency = list(partial_rec_set)[0].currency_id
            print "esto es currency ", currency
            maxdate = None
            print "esto es partial_rec_set ", partial_rec_set
            print "esto es aml_set ", aml_set
            for partial_rec in partial_rec_set:
                print "entra al primer for"
                print "esto es partial_rec ", partial_rec.debit_move_id
                print "esto es partial_rec ", partial_rec.credit_move_id
                print "esto es partial_rec.currency_id ", partial_rec.currency_id
                if partial_rec.currency_id != currency:
                    print "entra partial_rec.currency_id != currency:"
                    #no exchange rate entry will be created
                    currency = None
                for aml in [partial_rec.debit_move_id, partial_rec.credit_move_id]:
                    print "entra sgundo for "
                    if aml not in aml_set:
                        if aml.amount_residual or aml.amount_residual_currency:
                            aml_to_balance |= aml
                        maxdate = max(aml.date, maxdate)
                        total_debit += aml.debit
                        total_credit += aml.credit
                        aml_set |= aml
                        print "esto es partial_rec.currency_id ",partial_rec.currency_id
                        if aml.currency_id and aml.currency_id == currency:
                            total_amount_currency += aml.amount_currency
                        elif partial_rec.currency_id and partial_rec.currency_id == currency:
                            #if the aml has no secondary currency but is reconciled with other journal item(s) in secondary currency, the amount
                            #currency is recorded on the partial rec and in order to check if the reconciliation is total, we need to convert the
                            #aml.balance in that foreign currency
                            total_amount_currency += aml.company_id.currency_id.with_context(date=aml.date).compute(aml.balance, partial_rec.currency_id)
                    for x in aml.matched_debit_ids | aml.matched_credit_ids:
                        partial_rec_set[x] = None
            partial_rec_ids = [x.id for x in partial_rec_set.keys()]
            aml_ids = aml_set.ids
            #then, if the total debit and credit are equal, or the total amount in currency is 0, the reconciliation is full
            digits_rounding_precision = aml_set[0].company_id.currency_id.rounding
            print "esto es currency ", currency
            print "esto es total_amount_currency ",total_amount_currency
            #print "esto es currency.rounding ",currency.rounding
            print "esto es total_debit ",total_debit
            print "esto es total_credit ",total_credit
            #print "esto es float_is_zero ", float_is_zero(total_amount_currency, precision_rounding=currency.rounding)
            print "esto es float_comparte ", float_compare(total_debit, total_credit, precision_rounding=digits_rounding_precision)

            if (currency and float_is_zero(total_amount_currency, precision_rounding=currency.rounding)) or float_compare(total_debit, total_credit, precision_rounding=digits_rounding_precision) == 0:
                print "entra a if linea 452"
                exchange_move_id = False
                exchange_partial_rec_id = False
                print "esto es currency ", currency
                print "esto es aml_to_balance ", aml_to_balance
                if currency and aml_to_balance:
                    exchange_move=False
                    rate_diff_amls, rate_diff_partial_recs = partial_rec._fix_multiple_exchange_rates_diff(aml_to_balance, abs(self.env.context.get('monto_pago')), total_amount_currency, currency, exchange_move, False)
                    aml_ids += rate_diff_amls.ids
                    partial_rec_ids += rate_diff_partial_recs.ids
                    #exchange_move.post()
                    #exchange_move_id = exchange_move.id
                    exchange_partial_rec_id = rate_diff_partial_recs[-1:].id

            else:
                print "entra al else linea 759"
                exchange_move_id = False
                exchange_partial_rec_id = False
                print "esto es currency ", currency
                print "esto es aml_to_balance ", aml_to_balance
                if currency and aml_to_balance:
                    exchange_move=False
                    rate_diff_amls, rate_diff_partial_recs = partial_rec._fix_multiple_exchange_rates_diff(aml_to_balance, abs(self.env.context.get('monto_pago')), total_amount_currency, currency, exchange_move, False)
                    aml_ids += rate_diff_amls.ids
                    partial_rec_ids += rate_diff_partial_recs.ids
                    #exchange_move.post()
                    #exchange_move_id = exchange_move.id
                    exchange_partial_rec_id = rate_diff_partial_recs[-1:].id
                #else:
                    #for partial_rec in partial_rec_set:
                        #move = self.env['account.move'].browse(partial_rec.credit_move_id.move_id.id)
                        #if move:
                            #print "esto es move ", move
                            #dif = 0
                            #for line in move.line_ids:
                                #print "esto es line ", line
                                #if line.account_id.id == line.journal_id.default_debit_account_id.id:
                                    #print "esto es line.debit ", line
                                    #print "esto es line.debit ", line.debit
                                    #dif = line.debit - partial_rec.credit_move_id.credit
                                    #print "esto es dif ", dif
