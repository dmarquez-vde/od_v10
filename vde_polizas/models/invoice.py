# -*- coding: utf-8 -*-

import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.tools import float_is_zero, float_compare
from odoo.tools.misc import formatLang

from odoo.exceptions import UserError, RedirectWarning, ValidationError

import odoo.addons.decimal_precision as dp
import logging

class AccountInvoice(models.Model):
    _inherit = "account.invoice"


    @api.multi
    def register_payment(self, payment_line, writeoff_acc_id=False, writeoff_journal_id=False):
        print "entra def register_payment "
        print "esto es payment_line ", payment_line
        print "esto es payment_line ", payment_line.reconciled
        print "esto es writeoff_acc_id ", writeoff_acc_id
        print "esto es writeoff_journal_id ", writeoff_journal_id
        """ Reconcile payable/receivable lines from the invoice with payment_line """
        line_to_reconcile = self.env['account.move.line']
        for inv in self:
            line_to_reconcile += inv.move_id.line_ids.filtered(lambda r: not r.reconciled and r.account_id.internal_type in ('payable', 'receivable'))
        print "esto es payment_line ", payment_line
        #payment_line.write({'reconciled':False})
        print "esto es line_to_reconcile ", line_to_reconcile
        return (line_to_reconcile + payment_line).reconcile(writeoff_acc_id, writeoff_journal_id)
