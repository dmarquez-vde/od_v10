# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd.
#     (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError
import datetime

class account_payment(models.Model):

    _inherit = 'account.payment'

    def _create_payment_entry(self, amount):
        if self.env.context.get('intermoneda')==False:
            ctx = self.env.context.copy()
            print "***************************** ", self.env.context
            print "entra a _create_payment_entry ss", self.env.context
            """ Create a journal entry corresponding to a payment, if the payment references invoice(s) they are reconciled.
                Return the journal entry.
            """
            if self.invoice_ids and self.line_ids:
                aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
                invoice_currency = False
                if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
                    #if all the invoices selected share the same currency, record the paiement in that currency too
                    invoice_currency = self.invoice_ids[0].currency_id
                print "esto es invoice_currency ", invoice_currency
                print "esto es self.invoice_ids ", self.invoice_ids
                move = self.env['account.move'].create(self._get_move_vals())
                print "Esto es move ", move
                for inv in self.invoice_ids:
                    amt = 0
                    if self.partner_type == 'customer':
                        print "partner_type == customer"
                        for line in self.line_ids:
                            if line.invoice_id.id == inv.id:
                            	if inv.type == 'out_invoice':
                            		amt = -(line.allocation)
                            	else:
                            		amt = line.allocation
                    else:
                        print "partner_type <> customer 1"
                        for line in self.line_ids:
                            if line.invoice_id.id == inv.id:
                            	if inv.type == 'in_invoice':
                            		amt = line.allocation
                            	else:
                            		amt = -(line.allocation)
                    print "esto es self.payment_date", self.payment_date
                    print "esto es self.currency_id", self.currency_id
                    print "esto es amt ", amt
                    #debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id, invoice_currency)
                    debit, credit, amount_currency, currency_id =\
                        aml_obj.with_context(date=self.payment_date).\
                        compute_amount_fields(amt, self.currency_id,
                                              self.company_id.currency_id,
                                              invoice_currency)
                    print "esto es debit ", debit
                    print "esto es credit ", credit
                    print "esto es amount_currency ", amount_currency
                    print "esto es currency_id ", currency_id

                    #move = self.env['account.move'].create(self._get_move_vals())
                    #VDE este move es bancos vs cliente/proveedor
                    print "Esto es move ", move
                    ctx.update({'pago_poliza_id':move.id,'pago_id':self.id, 'monto_pago':amt, 'invoice_id':inv.id ,'fecha_factura':inv.date_invoice, 'fecha_pago':self.payment_date})
                    #Write line corresponding to invoice payment
                    counterpart_aml_dict = self._get_shared_move_line_vals(debit, credit, amount_currency, move.id, False)
                    print "esto es counterpart_aml_dict 1", counterpart_aml_dict
                    counterpart_aml_dict.update(self._get_counterpart_move_line_vals(self.invoice_ids))
                    print "esto es counterpart_aml_dict 1", counterpart_aml_dict
                    counterpart_aml_dict.update({'currency_id': currency_id})
                    counterpart_aml_dict.update({'invoice_idd': inv.id})
                    counterpart_aml = aml_obj.create(counterpart_aml_dict)

                    print "esto es counterpart_aml 1", counterpart_aml
                    print "esto es counterpart_aml.debit 1", counterpart_aml.debit
                    print "esto es counterpart_aml.credit 1", counterpart_aml.credit

                    if self.partner_type == 'customer':
                        handling = 'open'  # noqa
                        for line in self.line_ids:
                            if line.invoice_id.id == inv.id:
                                payment_difference = line.balance_amount - line.allocation  # noqa
                        writeoff_account_id = self.journal_id and self.journal_id.id or False  # noqa

                        #Reconcile with the invoices
                        #if self.payment_difference_handling == 'reconcile' and self.payment_difference:
                        if handling == 'reconcile' and payment_difference:
                            writeoff_line = self._get_shared_move_line_vals(0, 0, 0, move.id, False)
                            print "esto es writeoff_line ", writeoff_line
                            #amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
                            debit_wo, credit_wo, amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)
                            print "esto es amount_currency_wo ", amount_currency_wo
                            print "esto es currency_id ", currency_id
                            print "esto es credit_wo ", credit_wo
                            print "esto es debit_wo ", debit_wo
                            # the writeoff debit and credit must be computed from the invoice residual in company currency
                            # minus the payment amount in company currency, and not from the payment difference in the payment currency
                            # to avoid loss of precision during the currency rate computations. See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a detailed example.
                            ##total_residual_company_signed = sum(invoice.residual_company_signed for invoice in self.invoice_ids)
                            ##total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(self.amount, self.company_id.currency_id)
                            ##if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
                                ##amount_wo = total_payment_company_signed - total_residual_company_signed
                            ##else:
                                ##amount_wo = total_residual_company_signed - total_payment_company_signed
                            # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
                            # amount in the company currency
                            ##if amount_wo > 0:
                                ##debit_wo = amount_wo
                                ##credit_wo = 0.0
                                ##amount_currency_wo = abs(amount_currency_wo)
                            ##else:
                                ##debit_wo = 0.0
                                ##credit_wo = -amount_wo
                                ##amount_currency_wo = -abs(amount_currency_wo)
                            writeoff_line['name'] = _('Counterpart')
                            #writeoff_line['account_id'] = self.writeoff_account_id.id
                            writeoff_line['account_id'] = writeoff_account_id.id
                            writeoff_line['debit'] = debit_wo
                            writeoff_line['credit'] = credit_wo
                            writeoff_line['amount_currency'] = amount_currency_wo
                            writeoff_line['currency_id'] = currency_id
                            writeoff_line = aml_obj.create(writeoff_line)
                            if counterpart_aml['debit']:
                                counterpart_aml['debit'] += credit_wo - debit_wo
                            if counterpart_aml['credit']:
                                counterpart_aml['credit'] += debit_wo - credit_wo
                            counterpart_aml['amount_currency'] -= amount_currency_wo
                    #algo=self.invoice_ids.with_context(ctx).register_payment(counterpart_aml)
                    algo = inv.with_context(ctx).register_payment(counterpart_aml)
                    print "esto es algo ", algo
                    print "------------"
                    #Write counterpart lines
                    if not self.currency_id != self.company_id.currency_id:
                        amount_currency = 0
                    liquidity_aml_dict = self._get_shared_move_line_vals(credit, debit, -amount_currency, move.id, False)
                    print "esto es liquidity_aml_dict ", liquidity_aml_dict
                    liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))
                    liquidity_aml_dict.update({'invoice_idd':inv.id})
                    print "esto es liquidity_aml_dict ", liquidity_aml_dict
                    aml_obj.create(liquidity_aml_dict)
                move.ajuste_cambiario(self.id)
                move.post()
                return move








        elif self.env.context.get('intermoneda')==True:
            for line in self.line_ids:
                print "aqui es donde queria que entrara"
                if (line.currency_id.name=='USD' and line.invoice_currency_id.name == 'EUR') or (line.currency_id.name=='EUR' and line.invoice_currency_id.name == 'USD'):
                    ctx = self.env.context.copy()
                    print "***************************** intermoneda"
                    print "entra a _create_payment_entry"
                    """ Create a journal entry corresponding to a payment, if the payment references invoice(s) they are reconciled.
                        Return the journal entry.
                    """
                    if self.invoice_ids and self.line_ids:
                        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
                        invoice_currency = False
                        if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
                            #if all the invoices selected share the same currency, record the paiement in that currency too
                            invoice_currency = self.invoice_ids[0].currency_id
                        print "esto es invoice_currency ", invoice_currency
                        print "esto es self.invoice_ids ", self.invoice_ids
                        move = self.env['account.move'].create(self._get_move_vals())
                        print "Esto es move ", move
                        for inv in self.invoice_ids:
                            amt = 0
                            if self.partner_type == 'customer':
                                print "partner_type == customer"
                                for line in self.line_ids:
                                    if line.invoice_id.id == inv.id:


                                        if line.currency_id.name=='USD':
                                            print "entra pago en USD"
                                            if inv.type == 'out_invoice':
                                                print "esto es line.allocation ", line.allocation
                                                print "esto es line.tipocambiop ", line.tipocambio
                                                monto_moneda_d = line.allocation
                                                print "esto es amt ", -(line.allocation * (1/line.tipocambio))
                                                amt = -(line.allocation * (1/line.tipocambio))

                                        if line.currency_id.name=='EUR':
                                            print "entra pago en EUR"
                                            if inv.type == 'out_invoice':
                                                print "esto es line.allocation ", line.allocation
                                                print "esto es line.tipocambiop ", line.tipocambio
                                                monto_moneda_d = line.allocation
                                                print "esto es amt ", -(line.allocation * (1/line.tipocambio))
                                                amt = -(line.allocation * (1/line.tipocambio))


                                        if line.invoice_currency_id.name == 'EUR':
                                            print "entra factura en EUR"
                                            if inv.type == 'out_invoice':
                                                invoice_date_datetime = datetime.datetime.strptime(line.date, '%Y-%m-%d')
                                                rate_obj = self.env['res.currency.rate']
                                                rate_rec = rate_obj.search([
                                                    ('currency_id', '=', line.currency_id.id),
                                                    ('name', '<=', str(invoice_date_datetime))
                                                    ], order='name desc', limit=1)
                                                if rate_rec:
                                                    rate = rate_obj.browse(rate_rec.id).rate
                                                else:
                                                    rate = 1.0
                                                print "esto es rate ", rate
                                                print "esto es line.allocation ", line.allocation1
                                                print "esto es line.tipocambiop ", line.tipocambio
                                                print "esto es amt ", -round((line.allocation1 * (1/rate)),2)
                                                amt = -(line.allocation1)
                                                amt1 = -round((line.allocation1 * (1/rate)),2)
                                                amount_currency = -(line.allocation1)

                                        if line.invoice_currency_id.name == 'USD':
                                            print "entra factura en USD"
                                            if inv.type == 'out_invoice':
                                                invoice_date_datetime = datetime.datetime.strptime(line.date, '%Y-%m-%d')
                                                rate_obj = self.env['res.currency.rate']
                                                rate_rec = rate_obj.search([
                                                    ('currency_id', '=', line.currency_id.id),
                                                    ('name', '<=', str(invoice_date_datetime))
                                                    ], order='name desc', limit=1)
                                                if rate_rec:
                                                    rate = rate_obj.browse(rate_rec.id).rate
                                                else:
                                                    rate = 1.0
                                                print "esto es rate ", rate
                                                print "esto es line.allocation ", line.allocation1
                                                print "esto es line.tipocambiop ", line.tipocambio
                                                print "esto es amt ", -round((line.allocation1 * (1/rate)),2)
                                                amt = -(line.allocation1)
                                                amt1 = -round((line.allocation1 * (1/rate)),2)
                                                amount_currency = -(line.allocation1)




                            else:
                                #cambio1
                                print "partner_type <> customer 2"
                                for line in self.line_ids:
                                    if line.invoice_id.id == inv.id:

                                        if line.currency_id.name=='USD':
                                            print "entra pago en USD"
                                            if inv.type == 'in_invoice':
                                                print "esto es line.allocation ", line.allocation
                                                print "esto es line.tipocambiop ", line.tipocambio
                                                monto_moneda_d = line.allocation
                                                print "esto es amt ", -(line.allocation * (1/line.tipocambio))
                                                amt = -(line.allocation * (1/line.tipocambio))

                                        if line.currency_id.name=='EUR':
                                            print "entra pago en EUR"
                                            if inv.type == 'in_invoice':
                                                print "esto es line.allocation ", line.allocation
                                                print "esto es line.tipocambiop ", line.tipocambio
                                                monto_moneda_d = line.allocation
                                                print "esto es amt ", -(line.allocation * (1/line.tipocambio))
                                                amt = -(line.allocation * (1/line.tipocambio))


                                        if line.invoice_currency_id.name == 'EUR':
                                            print "entra factura en EUR"
                                            if inv.type == 'in_invoice':
                                                invoice_date_datetime = datetime.datetime.strptime(line.date, '%Y-%m-%d')
                                                rate_obj = self.env['res.currency.rate']
                                                rate_rec = rate_obj.search([
                                                    ('currency_id', '=', line.currency_id.id),
                                                    ('name', '<=', str(invoice_date_datetime))
                                                    ], order='name desc', limit=1)
                                                if rate_rec:
                                                    rate = rate_obj.browse(rate_rec.id).rate
                                                else:
                                                    rate = 1.0
                                                print "esto es rate ", rate
                                                print "esto es line.allocation ", line.allocation1
                                                print "esto es line.tipocambiop ", line.tipocambio
                                                print "esto es amt ", -round((line.allocation1 * (1/rate)),2)
                                                amt = -(line.allocation1)
                                                amt1 = -round((line.allocation1 * (1/rate)),2)
                                                amount_currency = -(line.allocation1)

                                        if line.invoice_currency_id.name == 'USD':
                                            print "entra factura en USD"
                                            if inv.type == 'in_invoice':
                                                invoice_date_datetime = datetime.datetime.strptime(line.date, '%Y-%m-%d')
                                                rate_obj = self.env['res.currency.rate']
                                                rate_rec = rate_obj.search([
                                                    ('currency_id', '=', line.currency_id.id),
                                                    ('name', '<=', str(invoice_date_datetime))
                                                    ], order='name desc', limit=1)
                                                if rate_rec:
                                                    rate = rate_obj.browse(rate_rec.id).rate
                                                else:
                                                    rate = 1.0
                                                print "esto es rate ", rate
                                                print "esto es line.allocation ", line.allocation1
                                                print "esto es line.tipocambiop ", line.tipocambio
                                                print "esto es amt ", -round((line.allocation1 * (1/rate)),2)
                                                amt = -(line.allocation1)
                                                amt1 = -round((line.allocation1 * (1/rate)),2)
                                                amount_currency = -(line.allocation1)




                            print "esto es self.payment_date", self.payment_date
                            print "esto es self.currency_id", self.currency_id
                            print "esto es amt ", amt
                            #debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id, invoice_currency)




                            if self.currency_id.name != 'MXN':
                                for line in self.line_ids:
                                #debit, credit, amount_currency, currency_id =\
                                    #aml_obj.with_context(date=inv.date_invoice).\
                                    #compute_amount_fields(amt, self.currency_id,
                                                          #self.company_id.currency_id,
                                                          #self.currency_id)
                                    algo=-(line.allocation * (1/line.tipocambio))
                                    debit = 0
                                    credit = abs(algo)
                                    currency_id = self.journal_id.currency_id.id
                            print "esto es debit ", debit
                            print "esto es credit ", credit
                            print "esto es amount_currency ", amount_currency
                            print "esto es currency_id ", currency_id
                            #print "esto es monto_moneda_d ", monto_moneda_d

                            #move = self.env['account.move'].create(self._get_move_vals())
                            #VDE este move es bancos vs cliente/proveedor
                            print "Esto es move ", move
                            ctx.update({'pago_poliza_id':move.id,'pago_id':self.id, 'monto_pago':amt, 'invoice_id':inv.id ,'fecha_factura':inv.date_invoice, 'fecha_pago':self.payment_date})
                            #Write line corresponding to invoice payment
                            if inv.currency_id.name != 'MXN':
                                counterpart_aml_dict = self._get_shared_move_line_vals(debit, credit, -(monto_moneda_d), move.id, False)
                            else:
                                counterpart_aml_dict = self._get_shared_move_line_vals(debit, credit, amount_currency, move.id, False)
                            print "Esto es counterpart_aml_dict 2",counterpart_aml_dict
                            counterpart_aml_dict.update(self._get_counterpart_move_line_vals(self.invoice_ids))


                            if inv.currency_id.name != 'MXN':
                                counterpart_aml_dict.update({'currency_id': inv.currency_id.id, 'reconciled':False})
                            else:
                                counterpart_aml_dict.update({'currency_id': currency_id, 'reconciled':False})
                            counterpart_aml_dict.update({'invoice_idd': inv.id})
                            print "Esto es counterpart_aml_dict 2",counterpart_aml_dict
                            counterpart_aml = aml_obj.create(counterpart_aml_dict)
                            print "esto es counterpart_aml 2", counterpart_aml.credit
                            rec_move_line=self.env['account.move.line'].search([('move_id','=',move.id)])
                            print "esto es rec_move_line ", rec_move_line

                            if self.partner_type == 'customer':
                                handling = 'open'  # noqa
                                for line in self.line_ids:
                                    if line.invoice_id.id == inv.id:
                                        print "esto es balance_amount ", line.balance_amount

                                        if (line.currency_id.name=='USD' and line.invoice_currency_id.name == 'EUR') or (line.currency_id.name=='EUR' and line.invoice_currency_id.name == 'USD'):
                                            print "entra a lo que sea que haga"
                                            new_balance_amount = round(line.allocation * (1/line.tipocambiop1),4)
                                            new_balance_amount1 = round(line.allocation * (1/line.tipocambio),4)
                                            invoice_date_datetime = datetime.datetime.strptime(line.date, '%Y-%m-%d')
                                            rate_obj = self.env['res.currency.rate']
                                            rate_rec = rate_obj.search([
                                                ('currency_id', '=', line.currency_id.id),
                                                ('name', '<=', str(invoice_date_datetime))
                                                ], order='name desc', limit=1)
                                            if rate_rec:
                                                rate = rate_obj.browse(rate_rec.id).rate


                                            print "esto es rate factura ", rate
                                            print "esto es rate factura ", (1/rate)
                                            new_balance_amount = round(line.allocation / (1/rate),4)
                                            print "esto es rate factura ", line.tipocambiop
                                            print "esto es rate factura ", (1/line.tipocambiop)
                                            new_balance_amount1 = round(line.allocation / (1/line.tipocambiop),4)
                                        print "esto es new_balance_amount ", new_balance_amount
                                        print "esto es new_balance_amount1 ", new_balance_amount1
                                        print "esto es allocation ", line.allocation
                                        payment_difference = round(new_balance_amount - new_balance_amount1, 1)  # noqa

                                writeoff_account_id = self.journal_id and self.journal_id.id or False  # noqa
                                print "esto es payment_differente ", payment_difference
                                #Reconcile with the invoices
                                #if self.payment_difference_handling == 'reconcile' and self.payment_difference:
                                if handling == 'reconcile' and payment_difference:
                                    print "entra a if 270"
                                    writeoff_line = self._get_shared_move_line_vals(0, 0, 0, move.id, False)
                                    print "esto es writeoff_line ", writeoff_line
                                    #amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
                                    debit_wo, credit_wo, amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)
                                    print "esto es amount_currency_wo ", amount_currency_wo
                                    print "esto es currency_id ", currency_id
                                    print "esto es credit_wo ", credit_wo
                                    print "esto es debit_wo ", debit_wo
                                    # the writeoff debit and credit must be computed from the invoice residual in company currency
                                    # minus the payment amount in company currency, and not from the payment difference in the payment currency
                                    # to avoid loss of precision during the currency rate computations. See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a detailed example.
                                    ##total_residual_company_signed = sum(invoice.residual_company_signed for invoice in self.invoice_ids)
                                    ##total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(self.amount, self.company_id.currency_id)
                                    ##if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
                                        ##amount_wo = total_payment_company_signed - total_residual_company_signed
                                    ##else:
                                        ##amount_wo = total_residual_company_signed - total_payment_company_signed
                                    # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
                                    # amount in the company currency
                                    ##if amount_wo > 0:
                                        ##debit_wo = amount_wo
                                        ##credit_wo = 0.0
                                        ##amount_currency_wo = abs(amount_currency_wo)
                                    ##else:
                                        ##debit_wo = 0.0
                                        ##credit_wo = -amount_wo
                                        ##amount_currency_wo = -abs(amount_currency_wo)
                                    writeoff_line['name'] = _('Counterpart')
                                    #writeoff_line['account_id'] = self.writeoff_account_id.id
                                    writeoff_line['account_id'] = writeoff_account_id.id
                                    writeoff_line['debit'] = debit_wo
                                    writeoff_line['credit'] = credit_wo
                                    writeoff_line['amount_currency'] = amount_currency_wo
                                    writeoff_line['currency_id'] = currency_id
                                    writeoff_line = aml_obj.create(writeoff_line)
                                    if counterpart_aml['debit']:
                                        counterpart_aml['debit'] += credit_wo - debit_wo
                                    if counterpart_aml['credit']:
                                        counterpart_aml['credit'] += debit_wo - credit_wo
                                    counterpart_aml['amount_currency'] -= amount_currency_wo
                            #algo=self.invoice_ids.with_context(ctx).register_payment(counterpart_aml)

                            else:
                                print "print de busqueda"
                                handling = 'open'  # noqa
                                for line in self.line_ids:
                                    if line.invoice_id.id == inv.id:
                                        print "esto es balance_amount ", line.balance_amount

                                        if (line.currency_id.name=='USD' and line.invoice_currency_id.name == 'EUR') or (line.currency_id.name=='EUR' and line.invoice_currency_id.name == 'USD'):
                                            print "entra a lo que sea que haga"
                                            new_balance_amount = round(line.allocation * (1/line.tipocambiop1),4)
                                            new_balance_amount1 = round(line.allocation * (1/line.tipocambio),4)
                                            invoice_date_datetime = datetime.datetime.strptime(line.date, '%Y-%m-%d')
                                            rate_obj = self.env['res.currency.rate']
                                            rate_rec = rate_obj.search([
                                                ('currency_id', '=', line.currency_id.id),
                                                ('name', '<=', str(invoice_date_datetime))
                                                ], order='name desc', limit=1)
                                            if rate_rec:
                                                rate = rate_obj.browse(rate_rec.id).rate


                                            print "esto es rate factura ", rate
                                            print "esto es rate factura ", (1/rate)
                                            new_balance_amount = round(line.allocation / (1/rate),4)
                                            print "esto es rate factura ", line.tipocambiop
                                            print "esto es rate factura ", (1/line.tipocambiop)
                                            new_balance_amount1 = round(line.allocation / (1/line.tipocambiop),4)
                                        print "esto es new_balance_amount ", new_balance_amount
                                        print "esto es new_balance_amount1 ", new_balance_amount1
                                        print "esto es allocation ", line.allocation
                                        payment_difference = round(new_balance_amount - new_balance_amount1, 1)  # noqa

                                writeoff_account_id = self.journal_id and self.journal_id.id or False  # noqa
                                print "esto es payment_differente ", payment_difference
                                #Reconcile with the invoices
                                #if self.payment_difference_handling == 'reconcile' and self.payment_difference:
                                if handling == 'reconcile' and payment_difference:
                                    print "entra a if 270"
                                    writeoff_line = self._get_shared_move_line_vals(0, 0, 0, move.id, False)
                                    print "esto es writeoff_line ", writeoff_line
                                    #amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
                                    debit_wo, credit_wo, amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)
                                    print "esto es amount_currency_wo ", amount_currency_wo
                                    print "esto es currency_id ", currency_id
                                    print "esto es credit_wo ", credit_wo
                                    print "esto es debit_wo ", debit_wo
                                    # the writeoff debit and credit must be computed from the invoice residual in company currency
                                    # minus the payment amount in company currency, and not from the payment difference in the payment currency
                                    # to avoid loss of precision during the currency rate computations. See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a detailed example.
                                    ##total_residual_company_signed = sum(invoice.residual_company_signed for invoice in self.invoice_ids)
                                    ##total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(self.amount, self.company_id.currency_id)
                                    ##if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
                                        ##amount_wo = total_payment_company_signed - total_residual_company_signed
                                    ##else:
                                        ##amount_wo = total_residual_company_signed - total_payment_company_signed
                                    # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
                                    # amount in the company currency
                                    ##if amount_wo > 0:
                                        ##debit_wo = amount_wo
                                        ##credit_wo = 0.0
                                        ##amount_currency_wo = abs(amount_currency_wo)
                                    ##else:
                                        ##debit_wo = 0.0
                                        ##credit_wo = -amount_wo
                                        ##amount_currency_wo = -abs(amount_currency_wo)
                                    writeoff_line['name'] = _('Counterpart')
                                    #writeoff_line['account_id'] = self.writeoff_account_id.id
                                    writeoff_line['account_id'] = writeoff_account_id.id
                                    writeoff_line['debit'] = debit_wo
                                    writeoff_line['credit'] = credit_wo
                                    writeoff_line['amount_currency'] = amount_currency_wo
                                    writeoff_line['currency_id'] = currency_id
                                    writeoff_line = aml_obj.create(writeoff_line)
                                    if counterpart_aml['debit']:
                                        counterpart_aml['debit'] += credit_wo - debit_wo
                                    if counterpart_aml['credit']:
                                        counterpart_aml['credit'] += debit_wo - credit_wo
                                    counterpart_aml['amount_currency'] -= amount_currency_wo
                            #algo=self.invoice_ids.with_context(ctx).register_payment(counterpart_aml)















                            print "antes de algo "
                            algo = inv.with_context(ctx).register_payment(counterpart_aml)
                            print "esto es algopppppp 1", algo


                            if self.currency_id.name != 'MXN':
                                print "esto es context A", self.env.context
                                pay_amount = amt
                                print "esto es pay_amount ", pay_amount
                                pay_date = self.payment_date
                                print "esto es pay_date ", pay_date
                                invoice_date_datetime = datetime.datetime.strptime(inv.date_invoice, '%Y-%m-%d')
                                rate_obj = self.env['res.currency.rate']
                                rate_rec = rate_obj.search([
                                    ('currency_id', '=', self.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                if rate_rec:
                                    rate = rate_obj.browse(rate_rec.id).rate
                                else:
                                    rate = 1.0
                                print "esto es reate ", rate
                                pay_ori_amount = pay_amount * (1/rate)
                                print "esto es pay_ori_amount ", pay_ori_amount
                                invoice_date_datetime = datetime.datetime.strptime(pay_date, '%Y-%m-%d')
                                rate_obj = self.env['res.currency.rate']
                                rate_rec = rate_obj.search([
                                    ('currency_id', '=', self.currency_id.id),
                                    ('name', '<=', str(invoice_date_datetime))
                                    ], order='name desc', limit=1)
                                if rate_rec:
                                    rate = rate_obj.browse(rate_rec.id).rate
                                else:
                                    rate = 1.0
                                print "esto es rate ", rate
                                pay_amount_new = abs(round(pay_amount * (1/rate),2))
                                print "esto es pay_amount_new ", pay_amount_new

                                if self.payment_type == 'inbound':
                                    liquidity_aml_dict = self._get_shared_move_line_vals(pay_amount_new, 0, self.amount, move.id, False)
                                    print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                    liquidity_aml_dict.update(self._get_liquidity_move_line_vals(pay_amount_new))
                                    liquidity_aml_dict.update({'invoice_idd':inv.id})
                                    print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                    banco_id = aml_obj.create(liquidity_aml_dict)
                                    print "esto es banco_id ", banco_id


                                if self.payment_type == 'outbound':
                                    liquidity_aml_dict = self._get_shared_move_line_vals(pay_amount_new, 0, self.amount, move.id, False)
                                    print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                    liquidity_aml_dict.update(self._get_liquidity_move_line_vals(pay_amount_new))
                                    liquidity_aml_dict.update({'invoice_idd':inv.id})
                                    print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                    banco_id = aml_obj.create(liquidity_aml_dict)
                                    print "esto es banco_id ", banco_id



                                #Write counterpart lines
                                #if not self.currency_id != self.company_id.currency_id:
                                    #amount_currency = 0
                                #credit = self.amount
                                #liquidity_aml_dict = self._get_shared_move_line_vals(credit, debit, -amount_currency, move.id, False)
                                #print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                #liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))
                                #print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                #banco_id = aml_obj.create(liquidity_aml_dict)

                        move.ajuste_cambiario(self.id)
                        move.post()
                        return move







                else:
                    ctx = self.env.context.copy()
                    print "***************************** intermoneda"
                    print "entra a _create_payment_entry"
                    """ Create a journal entry corresponding to a payment, if the payment references invoice(s) they are reconciled.
                        Return the journal entry.
                    """
                    if self.invoice_ids and self.line_ids:
                        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
                        invoice_currency = False
                        if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
                            #if all the invoices selected share the same currency, record the paiement in that currency too
                            invoice_currency = self.invoice_ids[0].currency_id
                        print "esto es invoice_currency ", invoice_currency
                        print "esto es self.invoice_ids ", self.invoice_ids
                        move = self.env['account.move'].create(self._get_move_vals())
                        print "Esto es move ", move
                        for inv in self.invoice_ids:
                            amt = 0
                            if self.partner_type == 'customer':
                                print "partner_type == customer"
                                for line in self.line_ids:
                                    if line.invoice_id.id == inv.id:


                                        if line.currency_id.name=='MXN':
                                            print "entra pago en MXN"
                                            if inv.type == 'out_invoice':
                                                print "esto es line.allocation ", line.allocation
                                                print "esto es line.tipocambiop ", line.tipocambio
                                                monto_moneda_d = line.allocation
                                                print "esto es amt ", -(line.allocation * (1/line.tipocambio))
                                                amt = -(line.allocation * (1/line.tipocambio))


                                        if line.invoice_currency_id.name == 'MXN':
                                            print "entra factura en MXN"
                                            if inv.type == 'out_invoice':
                                                invoice_date_datetime = datetime.datetime.strptime(line.date, '%Y-%m-%d')
                                                rate_obj = self.env['res.currency.rate']
                                                rate_rec = rate_obj.search([
                                                    ('currency_id', '=', line.currency_id.id),
                                                    ('name', '<=', str(invoice_date_datetime))
                                                    ], order='name desc', limit=1)
                                                if rate_rec:
                                                    rate = rate_obj.browse(rate_rec.id).rate
                                                else:
                                                    rate = 1.0
                                                print "esto es rate ", rate
                                                print "esto es line.allocation ", line.allocation1
                                                print "esto es line.tipocambiop ", line.tipocambio
                                                print "esto es amt ", -round((line.allocation1 * (1/rate)),2)
                                                amt = -(line.allocation1)
                                                amt1 = -round((line.allocation1 * (1/rate)),2)
                                                amount_currency = -(line.allocation1)

                                        if self.currency_id.name == 'MXN':
                                            debit, credit, amount_currency, currency_id =\
                                                aml_obj.with_context(date=self.payment_date).\
                                                compute_amount_fields(amt, self.currency_id,
                                                                      self.company_id.currency_id,
                                                                      self.currency_id)

                                        elif self.currency_id.name != 'MXN':
                                            #debit, credit, amount_currency, currency_id =\
                                                #aml_obj.with_context(date=inv.date_invoice).\
                                                #compute_amount_fields(amt, self.currency_id,
                                                                      #self.company_id.currency_id,
                                                                      #self.currency_id)
                                            debit = 0
                                            credit = abs(amt1)
                                            currency_id = self.journal_id.currency_id.id


                                        print "esto es debit ", debit
                                        print "esto es credit ", credit
                                        print "esto es amount_currency ", amount_currency
                                        print "esto es currency_id ", currency_id
                                        #print "esto es monto_moneda_d ", monto_moneda_d



                                        print "esto es self.payment_date", self.payment_date
                                        print "esto es self.currency_id", self.currency_id
                                        print "esto es amt ", amt
                            #debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id, invoice_currency)
                                        #move = self.env['account.move'].create(self._get_move_vals())
                                        #VDE este move es bancos vs cliente/proveedor
                                        print "Esto es move ", move
                                        ctx.update({'pago_poliza_id':move.id,'pago_id':self.id, 'monto_pago':amt, 'invoice_id':inv.id ,'fecha_factura':inv.date_invoice, 'fecha_pago':self.payment_date})
                                        #Write line corresponding to invoice payment
                                        if inv.currency_id.name != 'MXN':
                                            counterpart_aml_dict = self._get_shared_move_line_vals(debit, credit, -(monto_moneda_d), move.id, False)
                                        else:
                                            counterpart_aml_dict = self._get_shared_move_line_vals(debit, credit, amount_currency, move.id, False)
                                        print "Esto es counterpart_aml_dict 3",counterpart_aml_dict
                                        counterpart_aml_dict.update(self._get_counterpart_move_line_vals(self.invoice_ids))
                                        if inv.currency_id.name != 'MXN':
                                            counterpart_aml_dict.update({'currency_id': inv.currency_id.id, 'reconciled':False})
                                        else:
                                            counterpart_aml_dict.update({'currency_id': currency_id, 'reconciled':False})
                                        counterpart_aml_dict.update({'invoice_idd': inv.id})
                                        print "Esto es counterpart_aml_dict 3",counterpart_aml_dict
                                        counterpart_aml = aml_obj.create(counterpart_aml_dict)
                                        print "esto es counterpart_aml  3", counterpart_aml.credit
                                        rec_move_line=self.env['account.move.line'].search([('move_id','=',move.id)])
                                        print "esto es rec_move_line ", rec_move_line

                                        if self.partner_type == 'customer':
                                            handling = 'open'  # noqa
                                            for line in self.line_ids:
                                                if line.invoice_id.id == inv.id:
                                                    print "esto es balance_amount ", line.balance_amount

                                                    if line.currency_id.name == 'MXN':
                                                        print "entra a mxn 1"
                                                        new_balance_amount = round(line.allocation * (1/line.tipocambiop1),4)
                                                        new_balance_amount1 = round(line.allocation * (1/line.tipocambio),4)
                                                    if line.invoice_currency_id.name=='MXN':
                                                        invoice_date_datetime = datetime.datetime.strptime(line.date, '%Y-%m-%d')
                                                        rate_obj = self.env['res.currency.rate']
                                                        rate_rec = rate_obj.search([
                                                            ('currency_id', '=', line.currency_id.id),
                                                            ('name', '<=', str(invoice_date_datetime))
                                                            ], order='name desc', limit=1)
                                                        if rate_rec:
                                                            rate = rate_obj.browse(rate_rec.id).rate
                                                        else:
                                                            rate = 1.0
                                                        print "esto es rate factura ", rate
                                                        print "esto es rate factura ", (1/rate)
                                                        new_balance_amount = round(line.allocation / (1/rate),4)
                                                        print "esto es rate factura ", line.tipocambiop
                                                        print "esto es rate factura ", (1/line.tipocambiop)
                                                        new_balance_amount1 = round(line.allocation / (1/line.tipocambiop),4)
                                                    print "esto es new_balance_amount ", new_balance_amount
                                                    print "esto es new_balance_amount1 ", new_balance_amount1
                                                    print "esto es allocation ", line.allocation
                                                    payment_difference = round(new_balance_amount - new_balance_amount1, 1)  # noqa
                                            writeoff_account_id = self.journal_id and self.journal_id.id or False  # noqa
                                            print "esto es payment_differente ", payment_difference
                                            #Reconcile with the invoices
                                            #if self.payment_difference_handling == 'reconcile' and self.payment_difference:
                                            if handling == 'reconcile' and payment_difference:
                                                print "entra a if 270"
                                                writeoff_line = self._get_shared_move_line_vals(0, 0, 0, move.id, False)
                                                print "esto es writeoff_line ", writeoff_line
                                                #amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
                                                debit_wo, credit_wo, amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)
                                                print "esto es amount_currency_wo ", amount_currency_wo
                                                print "esto es currency_id ", currency_id
                                                print "esto es credit_wo ", credit_wo
                                                print "esto es debit_wo ", debit_wo
                                                # the writeoff debit and credit must be computed from the invoice residual in company currency
                                                # minus the payment amount in company currency, and not from the payment difference in the payment currency
                                                # to avoid loss of precision during the currency rate computations. See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a detailed example.
                                                ##total_residual_company_signed = sum(invoice.residual_company_signed for invoice in self.invoice_ids)
                                                ##total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(self.amount, self.company_id.currency_id)
                                                ##if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
                                                    ##amount_wo = total_payment_company_signed - total_residual_company_signed
                                                ##else:
                                                    ##amount_wo = total_residual_company_signed - total_payment_company_signed
                                                # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
                                                # amount in the company currency
                                                ##if amount_wo > 0:
                                                    ##debit_wo = amount_wo
                                                    ##credit_wo = 0.0
                                                    ##amount_currency_wo = abs(amount_currency_wo)
                                                ##else:
                                                    ##debit_wo = 0.0
                                                    ##credit_wo = -amount_wo
                                                    ##amount_currency_wo = -abs(amount_currency_wo)
                                                writeoff_line['name'] = _('Counterpart')
                                                #writeoff_line['account_id'] = self.writeoff_account_id.id
                                                writeoff_line['account_id'] = writeoff_account_id.id
                                                writeoff_line['debit'] = debit_wo
                                                writeoff_line['credit'] = credit_wo
                                                writeoff_line['amount_currency'] = amount_currency_wo
                                                writeoff_line['currency_id'] = currency_id
                                                writeoff_line = aml_obj.create(writeoff_line)
                                                if counterpart_aml['debit']:
                                                    counterpart_aml['debit'] += credit_wo - debit_wo
                                                if counterpart_aml['credit']:
                                                    counterpart_aml['credit'] += debit_wo - credit_wo
                                                counterpart_aml['amount_currency'] -= amount_currency_wo
                                        #algo=self.invoice_ids.with_context(ctx).register_payment(counterpart_aml)

                                        else:
                                            handling = 'open'  # noqa
                                            for line in self.line_ids:
                                                if line.invoice_id.id == inv.id:
                                                    print "esto es balance_amount ", line.balance_amount

                                                    if line.currency_id.name == 'MXN':
                                                        print "entra a mxn 2"
                                                        new_balance_amount = round(line.allocation * (1/line.tipocambiop1),4)
                                                        new_balance_amount1 = round(line.allocation * (1/line.tipocambio),4)
                                                    if line.invoice_currency_id.name=='MXN':
                                                        invoice_date_datetime = datetime.datetime.strptime(line.date, '%Y-%m-%d')
                                                        rate_obj = self.env['res.currency.rate']
                                                        rate_rec = rate_obj.search([
                                                            ('currency_id', '=', line.currency_id.id),
                                                            ('name', '<=', str(invoice_date_datetime))
                                                            ], order='name desc', limit=1)
                                                        if rate_rec:
                                                            rate = rate_obj.browse(rate_rec.id).rate
                                                        else:
                                                            rate = 1.0
                                                        print "esto es rate factura ", rate
                                                        print "esto es rate factura ", (1/rate)
                                                        new_balance_amount = round(line.allocation / (1/rate),4)
                                                        print "esto es rate factura ", line.tipocambiop
                                                        print "esto es rate factura ", (1/line.tipocambiop)
                                                        new_balance_amount1 = round(line.allocation / (1/line.tipocambiop),4)
                                                    print "esto es new_balance_amount ", new_balance_amount
                                                    print "esto es new_balance_amount1 ", new_balance_amount1
                                                    print "esto es allocation ", line.allocation
                                                    payment_difference = round(new_balance_amount - new_balance_amount1, 1)  # noqa
                                            writeoff_account_id = self.journal_id and self.journal_id.id or False  # noqa
                                            print "esto es payment_differente ", payment_difference
                                            #Reconcile with the invoices
                                            #if self.payment_difference_handling == 'reconcile' and self.payment_difference:
                                            if handling == 'reconcile' and payment_difference:
                                                print "entra a if 270"
                                                writeoff_line = self._get_shared_move_line_vals(0, 0, 0, move.id, False)
                                                print "esto es writeoff_line ", writeoff_line
                                                #amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
                                                debit_wo, credit_wo, amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)
                                                print "esto es amount_currency_wo ", amount_currency_wo
                                                print "esto es currency_id ", currency_id
                                                print "esto es credit_wo ", credit_wo
                                                print "esto es debit_wo ", debit_wo
                                                # the writeoff debit and credit must be computed from the invoice residual in company currency
                                                # minus the payment amount in company currency, and not from the payment difference in the payment currency
                                                # to avoid loss of precision during the currency rate computations. See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a detailed example.
                                                ##total_residual_company_signed = sum(invoice.residual_company_signed for invoice in self.invoice_ids)
                                                ##total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(self.amount, self.company_id.currency_id)
                                                ##if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
                                                    ##amount_wo = total_payment_company_signed - total_residual_company_signed
                                                ##else:
                                                    ##amount_wo = total_residual_company_signed - total_payment_company_signed
                                                # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
                                                # amount in the company currency
                                                ##if amount_wo > 0:
                                                    ##debit_wo = amount_wo
                                                    ##credit_wo = 0.0
                                                    ##amount_currency_wo = abs(amount_currency_wo)
                                                ##else:
                                                    ##debit_wo = 0.0
                                                    ##credit_wo = -amount_wo
                                                    ##amount_currency_wo = -abs(amount_currency_wo)
                                                writeoff_line['name'] = _('Counterpart')
                                                #writeoff_line['account_id'] = self.writeoff_account_id.id
                                                writeoff_line['account_id'] = writeoff_account_id.id
                                                writeoff_line['debit'] = debit_wo
                                                writeoff_line['credit'] = credit_wo
                                                writeoff_line['amount_currency'] = amount_currency_wo
                                                writeoff_line['currency_id'] = currency_id
                                                writeoff_line = aml_obj.create(writeoff_line)
                                                if counterpart_aml['debit']:
                                                    counterpart_aml['debit'] += credit_wo - debit_wo
                                                if counterpart_aml['credit']:
                                                    counterpart_aml['credit'] += debit_wo - credit_wo
                                                counterpart_aml['amount_currency'] -= amount_currency_wo
                                        #algo=self.invoice_ids.with_context(ctx).register_payment(counterpart_aml)
                                        print "antes de algo "
                                        algo = inv.with_context(ctx).register_payment(counterpart_aml)
                                        print "esto es algopppppp 2", algo

                                        if self.currency_id.name == 'MXN':
                                            print "esto es context B", self.env.context
                                            pay_amount = amt
                                            print "esto es pay_amount ", pay_amount
                                            pay_date = self.payment_date
                                            print "esto es pay_date ", pay_date


                                            invoice_date_datetime = datetime.datetime.strptime(inv.date_invoice, '%Y-%m-%d')
                                            print "Esto es la fecha factura",invoice_date_datetime
                                            rate_obj = self.env['res.currency.rate']

                                            rate_rec = rate_obj.search([
                                                ('currency_id', '=', inv.currency_id.id),
                                                ('name', '<=', str(invoice_date_datetime))
                                                ], order='name desc', limit=1)
                                            if rate_rec:
                                                rate = rate_obj.browse(rate_rec.id).rate
                                            else:
                                                rate = 1.0
                                            print "esto es reate_ ", rate_obj.browse(rate_rec.id)
                                            print "esto es reate ", rate
                                            pay_ori_amount = pay_amount / (1/rate)
                                            print "esto es pay_ori_amount ", pay_ori_amount
                                            invoice_date_datetime = datetime.datetime.strptime(pay_date, '%Y-%m-%d')
                                            rate_obj = self.env['res.currency.rate']
                                            rate_rec = rate_obj.search([
                                                ('currency_id', '=', inv.currency_id.id),
                                                ('name', '<=', str(invoice_date_datetime))
                                                ], order='name desc', limit=1)
                                            if rate_rec:
                                                rate = rate_obj.browse(rate_rec.id).rate
                                            else:
                                                rate = 1.0
                                            print "esto es rate ", rate
                                            pay_amount_new = abs(round(pay_ori_amount * (1/rate),2))
                                            print "esto es pay_amount_new ", pay_amount_new

                                            if self.payment_type == 'inbound':
                                                if inv.currency_id.name != 'MXN':
                                                    liquidity_aml_dict = self._get_shared_move_line_vals(pay_amount_new, 0, abs(pay_ori_amount), move.id, False)
                                                else:
                                                    liquidity_aml_dict = self._get_shared_move_line_vals(pay_amount_new, 0, 0, move.id, False)
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                liquidity_aml_dict.update(self._get_liquidity_move_line_vals(pay_amount_new))
                                                if inv.currency_id.name != 'MXN':
                                                    liquidity_aml_dict.update({'currency_id':inv.currency_id.id})
                                                liquidity_aml_dict.update({'invoice_idd':inv.id})
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                banco_id = aml_obj.create(liquidity_aml_dict)
                                                print "esto es banco_id ", banco_id

                                            if self.payment_type == 'outbound':
                                                if inv.currency_id.name != 'MXN':
                                                    liquidity_aml_dict = self._get_shared_move_line_vals(pay_amount_new, 0, abs(pay_ori_amount), move.id, False)
                                                else:
                                                    liquidity_aml_dict = self._get_shared_move_line_vals(pay_amount_new, 0, 0, move.id, False)
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                liquidity_aml_dict.update(self._get_liquidity_move_line_vals(pay_amount_new))
                                                if inv.currency_id.name != 'MXN':
                                                    liquidity_aml_dict.update({'currency_id':inv.currency_id.id})
                                                liquidity_aml_dict.update({'invoice_idd':inv.id})
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                banco_id = aml_obj.create(liquidity_aml_dict)
                                                print "esto es banco_id ", banco_id




                                        if self.currency_id.name != 'MXN':
                                            print "esto es context ", self.env.context
                                            pay_amount = amt
                                            print "esto es pay_amount ", pay_amount
                                            pay_date = self.payment_date
                                            print "esto es pay_date ", pay_date
                                            invoice_date_datetime = datetime.datetime.strptime(inv.date_invoice, '%Y-%m-%d')
                                            rate_obj = self.env['res.currency.rate']
                                            rate_rec = rate_obj.search([
                                                ('currency_id', '=', self.currency_id.id),
                                                ('name', '<=', str(invoice_date_datetime))
                                                ], order='name desc', limit=1)
                                            if rate_rec:
                                                rate = rate_obj.browse(rate_rec.id).rate
                                            else:
                                                rate = 1.0
                                            print "esto es reate ", rate
                                            pay_ori_amount = pay_amount * (1/rate)
                                            print "esto es pay_ori_amount ", pay_ori_amount
                                            invoice_date_datetime = datetime.datetime.strptime(pay_date, '%Y-%m-%d')
                                            rate_obj = self.env['res.currency.rate']
                                            rate_rec = rate_obj.search([
                                                ('currency_id', '=', self.currency_id.id),
                                                ('name', '<=', str(invoice_date_datetime))
                                                ], order='name desc', limit=1)
                                            if rate_rec:
                                                rate = rate_obj.browse(rate_rec.id).rate
                                            else:
                                                rate = 1.0
                                            print "esto es rate ", rate
                                            pay_amount_new = abs(round(pay_amount * (1/rate),2))
                                            print "esto es pay_amount_new ", pay_amount_new

                                            if self.payment_type == 'inbound':
                                                liquidity_aml_dict = self._get_shared_move_line_vals(pay_amount_new, 0, self.amount, move.id, False)
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                liquidity_aml_dict.update(self._get_liquidity_move_line_vals(pay_amount_new))
                                                liquidity_aml_dict.update({'invoice_idd':inv.id})
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                banco_id = aml_obj.create(liquidity_aml_dict)
                                                print "esto es banco_id ", banco_id

                                            if self.payment_type == 'outbound':
                                                liquidity_aml_dict = self._get_shared_move_line_vals(pay_amount_new, 0, self.amount, move.id, False)
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                liquidity_aml_dict.update(self._get_liquidity_move_line_vals(pay_amount_new))
                                                liquidity_aml_dict.update({'invoice_idd':inv.id})
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                banco_id = aml_obj.create(liquidity_aml_dict)
                                                print "esto es banco_id ", banco_id



                                            #Write counterpart lines
                                            #if not self.currency_id != self.company_id.currency_id:
                                                #amount_currency = 0
                                            #credit = self.amount
                                            #liquidity_aml_dict = self._get_shared_move_line_vals(credit, debit, -amount_currency, move.id, False)
                                            #print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                            #liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))
                                            #print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                            #banco_id = aml_obj.create(liquidity_aml_dict)
                            else:
                                #cambio2
                                print "partner_type == proveedor"
                                for line in self.line_ids:
                                    if line.invoice_id.id == inv.id:
                                    	if line.currency_id.name=='MXN':
                                            print "entra pago en MXN"
                                            if inv.type == 'in_invoice':
                                                print "esto es line.allocation ", line.allocation
                                                print "esto es line.tipocambiop ", line.tipocambio
                                                monto_moneda_d = line.allocation
                                                print "esto es amt ", (line.allocation * (1/line.tipocambio))
                                                amt = (line.allocation * (1/line.tipocambio))
                                        if line.invoice_currency_id.name == 'MXN':
                                            print "entra factura en MXN"
                                            if inv.type == 'in_invoice':
                                                invoice_date_datetime = datetime.datetime.strptime(line.date, '%Y-%m-%d')
                                                rate_obj = self.env['res.currency.rate']
                                                rate_rec = rate_obj.search([
                                                    ('currency_id', '=', line.currency_id.id),
                                                    ('name', '<=', str(invoice_date_datetime))
                                                    ], order='name desc', limit=1)
                                                if rate_rec:
                                                    rate = rate_obj.browse(rate_rec.id).rate
                                                else:
                                                    rate = 1.0
                                                print "esto es rate ", rate
                                                print "esto es line.allocation ", line.allocation1
                                                print "esto es line.tipocambiop ", line.tipocambio
                                                print "esto es amt ", -round((line.allocation1 * (1/rate)),2)
                                                amt = -(line.allocation1)
                                                amt1 = -round((line.allocation1 * (1/rate)),2)
                                                amount_currency = -(line.allocation1)
                                        if self.currency_id.name == 'MXN':
                                            credit, debit, amount_currency, currency_id =\
                                                aml_obj.with_context(date=self.payment_date).\
                                                compute_amount_fields(amt, self.currency_id,
                                                                      self.company_id.currency_id,
                                                                      self.currency_id)
                                        elif self.currency_id.name != 'MXN':
                                            #debit, credit, amount_currency, currency_id =\
                                                #aml_obj.with_context(date=inv.date_invoice).\
                                                #compute_amount_fields(amt, self.currency_id,
                                                                      #self.company_id.currency_id,
                                                                      #self.currency_id)
                                            debit = 0
                                            credit = abs(amt1)
                                            currency_id = self.journal_id.currency_id.id
                                        print "esto es debit ", debit
                                        print "esto es credit ", credit
                                        print "esto es amount_currency ", amount_currency
                                        print "esto es currency_id ", currency_id
                                        #print "esto es monto_moneda_d ", monto_moneda_d
                                        print "esto es self.payment_date", self.payment_date
                                        print "esto es self.currency_id", self.currency_id
                                        print "esto es amt ", amt
                                        #move = self.env['account.move'].create(self._get_move_vals())
                                        #VDE este move es bancos vs cliente/proveedor
                                        print "Esto es move ", move
                                        ctx.update({'pago_poliza_id':move.id,'pago_id':self.id, 'monto_pago':amt, 'invoice_id':inv.id ,'fecha_factura':inv.date_invoice, 'fecha_pago':self.payment_date})
                                        #Write line corresponding to invoice payment
                                        if inv.currency_id.name != 'MXN':
                                            counterpart_aml_dict = self._get_shared_move_line_vals(credit, debit, abs(monto_moneda_d), move.id, False)
                                        else:
                                            counterpart_aml_dict = self._get_shared_move_line_vals(credit, debit,  abs(amount_currency), move.id, False)
                                        print "Esto es counterpart_aml_dict 4",counterpart_aml_dict
                                        counterpart_aml_dict.update(self._get_counterpart_move_line_vals(self.invoice_ids))
                                        if inv.currency_id.name != 'MXN':
                                            counterpart_aml_dict.update({'currency_id': inv.currency_id.id, 'reconciled':False})
                                        else:
                                            counterpart_aml_dict.update({'currency_id': currency_id, 'reconciled':False})
                                        counterpart_aml_dict.update({'invoice_idd': inv.id})
                                        print "Esto es counterpart_aml_dict 4",counterpart_aml_dict
                                        counterpart_aml = aml_obj.create(counterpart_aml_dict)
                                        print "esto es counterpart_aml 4", counterpart_aml.credit
                                        print "esto es counterpart_aml 4", counterpart_aml.debit
                                        rec_move_line=self.env['account.move.line'].search([('move_id','=',move.id)])
                                        print "esto es rec_move_line ", rec_move_line

                                        if self.partner_type == 'customer':
                                            handling = 'open'  # noqa
                                            for line in self.line_ids:
                                                if line.invoice_id.id == inv.id:
                                                    print "esto es balance_amount ", line.balance_amount

                                                    if line.currency_id.name == 'MXN':
                                                        print "entra a mxn 3"
                                                        new_balance_amount = round(line.allocation * (1/line.tipocambiop1),4)
                                                        new_balance_amount1 = round(line.allocation * (1/line.tipocambio),4)
                                                    if line.invoice_currency_id.name=='MXN':
                                                        invoice_date_datetime = datetime.datetime.strptime(line.date, '%Y-%m-%d')
                                                        rate_obj = self.env['res.currency.rate']
                                                        rate_rec = rate_obj.search([
                                                            ('currency_id', '=', line.currency_id.id),
                                                            ('name', '<=', str(invoice_date_datetime))
                                                            ], order='name desc', limit=1)
                                                        if rate_rec:
                                                            rate = rate_obj.browse(rate_rec.id).rate
                                                        else:
                                                            rate = 1.0
                                                        print "esto es rate factura ", rate
                                                        print "esto es rate factura ", (1/rate)
                                                        new_balance_amount = round(line.allocation / (1/rate),4)
                                                        print "esto es rate factura ", line.tipocambiop
                                                        print "esto es rate factura ", (1/line.tipocambiop)
                                                        new_balance_amount1 = round(line.allocation / (1/line.tipocambiop),4)
                                                    print "esto es new_balance_amount ", new_balance_amount
                                                    print "esto es new_balance_amount1 ", new_balance_amount1
                                                    print "esto es allocation ", line.allocation
                                                    payment_difference = round(new_balance_amount - new_balance_amount1, 1)  # noqa
                                            writeoff_account_id = self.journal_id and self.journal_id.id or False  # noqa
                                            print "esto es payment_differente ", payment_difference
                                            #Reconcile with the invoices
                                            #if self.payment_difference_handling == 'reconcile' and self.payment_difference:
                                            if handling == 'reconcile' and payment_difference:
                                                print "entra a if 270"
                                                writeoff_line = self._get_shared_move_line_vals(0, 0, 0, move.id, False)
                                                print "esto es writeoff_line ", writeoff_line
                                                #amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
                                                debit_wo, credit_wo, amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)
                                                print "esto es amount_currency_wo ", amount_currency_wo
                                                print "esto es currency_id ", currency_id
                                                print "esto es credit_wo ", credit_wo
                                                print "esto es debit_wo ", debit_wo
                                                # the writeoff debit and credit must be computed from the invoice residual in company currency
                                                # minus the payment amount in company currency, and not from the payment difference in the payment currency
                                                # to avoid loss of precision during the currency rate computations. See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a detailed example.
                                                ##total_residual_company_signed = sum(invoice.residual_company_signed for invoice in self.invoice_ids)
                                                ##total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(self.amount, self.company_id.currency_id)
                                                ##if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
                                                    ##amount_wo = total_payment_company_signed - total_residual_company_signed
                                                ##else:
                                                    ##amount_wo = total_residual_company_signed - total_payment_company_signed
                                                # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
                                                # amount in the company currency
                                                ##if amount_wo > 0:
                                                    ##debit_wo = amount_wo
                                                    ##credit_wo = 0.0
                                                    ##amount_currency_wo = abs(amount_currency_wo)
                                                ##else:
                                                    ##debit_wo = 0.0
                                                    ##credit_wo = -amount_wo
                                                    ##amount_currency_wo = -abs(amount_currency_wo)
                                                writeoff_line['name'] = _('Counterpart')
                                                #writeoff_line['account_id'] = self.writeoff_account_id.id
                                                writeoff_line['account_id'] = writeoff_account_id.id
                                                writeoff_line['debit'] = debit_wo
                                                writeoff_line['credit'] = credit_wo
                                                writeoff_line['amount_currency'] = amount_currency_wo
                                                writeoff_line['currency_id'] = currency_id
                                                writeoff_line = aml_obj.create(writeoff_line)
                                                if counterpart_aml['debit']:
                                                    counterpart_aml['debit'] += credit_wo - debit_wo
                                                if counterpart_aml['credit']:
                                                    counterpart_aml['credit'] += debit_wo - credit_wo
                                                counterpart_aml['amount_currency'] -= amount_currency_wo
                                        #algo=self.invoice_ids.with_context(ctx).register_payment(counterpart_aml)

                                        else:
                                            handling = 'open'  # noqa
                                            for line in self.line_ids:
                                                if line.invoice_id.id == inv.id:
                                                    print "esto es balance_amount ", line.balance_amount

                                                    if line.currency_id.name == 'MXN':
                                                        print "entra a mxn 4"
                                                        new_balance_amount = round(line.allocation * (1/line.tipocambiop1),4)
                                                        new_balance_amount1 = round(line.allocation * (1/line.tipocambio),4)
                                                    if line.invoice_currency_id.name=='MXN':
                                                        invoice_date_datetime = datetime.datetime.strptime(line.date, '%Y-%m-%d')
                                                        rate_obj = self.env['res.currency.rate']
                                                        rate_rec = rate_obj.search([
                                                            ('currency_id', '=', line.currency_id.id),
                                                            ('name', '<=', str(invoice_date_datetime))
                                                            ], order='name desc', limit=1)
                                                        if rate_rec:
                                                            rate = rate_obj.browse(rate_rec.id).rate
                                                        else:
                                                            rate = 1.0
                                                        print "esto es rate factura ", rate
                                                        print "esto es rate factura ", (1/rate)
                                                        new_balance_amount = round(line.allocation / (1/rate),4)
                                                        print "esto es rate factura ", line.tipocambiop
                                                        print "esto es rate factura ", (1/line.tipocambiop)
                                                        new_balance_amount1 = round(line.allocation / (1/line.tipocambiop),4)
                                                    print "esto es new_balance_amount ", new_balance_amount
                                                    print "esto es new_balance_amount1 ", new_balance_amount1
                                                    print "esto es allocation ", line.allocation
                                                    payment_difference = round(new_balance_amount - new_balance_amount1, 2)  # noqa
                                            writeoff_account_id = self.journal_id and self.journal_id.id or False  # noqa
                                            print "esto es payment_differente ", payment_difference
                                            #Reconcile with the invoices
                                            #if self.payment_difference_handling == 'reconcile' and self.payment_difference:
                                            if handling == 'reconcile' and payment_difference:
                                                print "entra a if 270"
                                                writeoff_line = self._get_shared_move_line_vals(0, 0, 0, move.id, False)
                                                print "esto es writeoff_line ", writeoff_line
                                                #amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
                                                debit_wo, credit_wo, amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)
                                                print "esto es amount_currency_wo ", amount_currency_wo
                                                print "esto es currency_id ", currency_id
                                                print "esto es credit_wo ", credit_wo
                                                print "esto es debit_wo ", debit_wo
                                                # the writeoff debit and credit must be computed from the invoice residual in company currency
                                                # minus the payment amount in company currency, and not from the payment difference in the payment currency
                                                # to avoid loss of precision during the currency rate computations. See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a detailed example.
                                                ##total_residual_company_signed = sum(invoice.residual_company_signed for invoice in self.invoice_ids)
                                                ##total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(self.amount, self.company_id.currency_id)
                                                ##if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
                                                    ##amount_wo = total_payment_company_signed - total_residual_company_signed
                                                ##else:
                                                    ##amount_wo = total_residual_company_signed - total_payment_company_signed
                                                # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
                                                # amount in the company currency
                                                ##if amount_wo > 0:
                                                    ##debit_wo = amount_wo
                                                    ##credit_wo = 0.0
                                                    ##amount_currency_wo = abs(amount_currency_wo)
                                                ##else:
                                                    ##debit_wo = 0.0
                                                    ##credit_wo = -amount_wo
                                                    ##amount_currency_wo = -abs(amount_currency_wo)
                                                writeoff_line['name'] = _('Counterpart')
                                                #writeoff_line['account_id'] = self.writeoff_account_id.id
                                                writeoff_line['account_id'] = writeoff_account_id.id
                                                writeoff_line['debit'] = debit_wo
                                                writeoff_line['credit'] = credit_wo
                                                writeoff_line['amount_currency'] = amount_currency_wo
                                                writeoff_line['currency_id'] = currency_id
                                                writeoff_line = aml_obj.create(writeoff_line)
                                                if counterpart_aml['debit']:
                                                    counterpart_aml['debit'] += credit_wo - debit_wo
                                                if counterpart_aml['credit']:
                                                    counterpart_aml['credit'] += debit_wo - credit_wo
                                                counterpart_aml['amount_currency'] -= amount_currency_wo
                                        #algo=self.invoice_ids.with_context(ctx).register_payment(counterpart_aml)










                                        print "antes de algo "
                                        algo = inv.with_context(ctx).register_payment(counterpart_aml)
                                        print "esto es algopppppp 3", algo

                                        if self.currency_id.name == 'MXN':
                                            print "esto es context C", self.env.context
                                            pay_amount = amt
                                            print "esto es pay_amount ", pay_amount
                                            pay_date = self.payment_date
                                            print "esto es pay_date ", pay_date
                                            invoice_date_datetime = datetime.datetime.strptime(inv.date_invoice, '%Y-%m-%d')
                                            rate_obj = self.env['res.currency.rate']
                                            rate_rec = rate_obj.search([
                                                ('currency_id', '=', inv.currency_id.id),
                                                ('name', '<=', str(invoice_date_datetime))
                                                ], order='name desc', limit=1)
                                            if rate_rec:
                                                rate = rate_obj.browse(rate_rec.id).rate
                                            else:
                                                rate = 1.0
                                            print "esto es reate ", rate
                                            pay_ori_amount = pay_amount / (1/rate)
                                            print "esto es pay_ori_amount ", pay_ori_amount
                                            invoice_date_datetime = datetime.datetime.strptime(pay_date, '%Y-%m-%d')
                                            rate_obj = self.env['res.currency.rate']
                                            rate_rec = rate_obj.search([
                                                ('currency_id', '=', inv.currency_id.id),
                                                ('name', '<=', str(invoice_date_datetime))
                                                ], order='name desc', limit=1)
                                            if rate_rec:
                                                rate = rate_obj.browse(rate_rec.id).rate
                                            else:
                                                rate = 1.0
                                            print "esto es rate ", rate
                                            pay_amount_new = abs(round(pay_ori_amount * (1/rate),2))
                                            print "esto es pay_amount_new ", pay_amount_new

                                            if self.payment_type == 'inbound':
                                                print"esta aqui arriba en inbound"
                                                if inv.currency_id.name != 'MXN':
                                                    liquidity_aml_dict = self._get_shared_move_line_vals(pay_amount_new, 0, abs(pay_ori_amount), move.id, False)
                                                else:
                                                    liquidity_aml_dict = self._get_shared_move_line_vals(pay_amount_new, 0, 0, move.id, False)
                                                print"esta aqui arriba en donde entro inbound"
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                liquidity_aml_dict.update(self._get_liquidity_move_line_vals(pay_amount_new))
                                                if inv.currency_id.name != 'MXN':
                                                    liquidity_aml_dict.update({'currency_id':inv.currency_id.id})
                                                liquidity_aml_dict.update({'invoice_idd':inv.id})
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                banco_id = aml_obj.create(liquidity_aml_dict)
                                                print "esto es banco_id ", banco_id

                                            if self.payment_type == 'outbound':
                                                print"esta aqui arriba en outbound"
                                                if inv.currency_id.name != 'MXN':
                                                    liquidity_aml_dict = self._get_shared_move_line_vals(0, pay_amount_new, -(pay_ori_amount), move.id, False)
                                                else:
                                                    liquidity_aml_dict = self._get_shared_move_line_vals(0, pay_amount_new, 0, move.id, False)
                                                print"esta aqui arriba en outbound"
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                liquidity_aml_dict.update(self._get_liquidity_move_line_vals(pay_amount_new))
                                                if inv.currency_id.name != 'MXN':
                                                    liquidity_aml_dict.update({'currency_id':inv.currency_id.id})
                                                liquidity_aml_dict.update({'invoice_idd':inv.id})
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                banco_id = aml_obj.create(liquidity_aml_dict)
                                                print "esto es banco_id ", banco_id




                                        if self.currency_id.name != 'MXN':
                                            print "esto es context ", self.env.context
                                            pay_amount = amt
                                            print "esto es pay_amount ", pay_amount
                                            pay_date = self.payment_date
                                            print "esto es pay_date ", pay_date
                                            invoice_date_datetime = datetime.datetime.strptime(inv.date_invoice, '%Y-%m-%d')
                                            rate_obj = self.env['res.currency.rate']
                                            rate_rec = rate_obj.search([
                                                ('currency_id', '=', self.currency_id.id),
                                                ('name', '<=', str(invoice_date_datetime))
                                                ], order='name desc', limit=1)
                                            if rate_rec:
                                                rate = rate_obj.browse(rate_rec.id).rate
                                            else:
                                                rate = 1.0
                                            print "esto es reate ", rate
                                            pay_ori_amount = pay_amount * (1/rate)
                                            print "esto es pay_ori_amount ", pay_ori_amount
                                            invoice_date_datetime = datetime.datetime.strptime(pay_date, '%Y-%m-%d')
                                            rate_obj = self.env['res.currency.rate']
                                            rate_rec = rate_obj.search([
                                                ('currency_id', '=', self.currency_id.id),
                                                ('name', '<=', str(invoice_date_datetime))
                                                ], order='name desc', limit=1)
                                            if rate_rec:
                                                rate = rate_obj.browse(rate_rec.id).rate
                                            else:
                                                rate = 1.0
                                            print "esto es rate ", rate
                                            pay_amount_new = abs(round(pay_amount * (1/rate),2))
                                            print "esto es pay_amount_new ", pay_amount_new

                                            if self.payment_type == 'inbound':
                                                liquidity_aml_dict = self._get_shared_move_line_vals(pay_amount_new, 0, self.amount, move.id, False)
                                                print"numero one en inbound"
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                liquidity_aml_dict.update(self._get_liquidity_move_line_vals(pay_amount_new))
                                                liquidity_aml_dict.update({'invoice_idd':inv.id})
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                banco_id = aml_obj.create(liquidity_aml_dict)
                                                print "esto es banco_id ", banco_id

                                            if self.payment_type == 'outbound':
                                                print"numero one en outbound"
                                                liquidity_aml_dict = self._get_shared_move_line_vals(0, pay_amount_new, -self.amount, move.id, False)
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                liquidity_aml_dict.update(self._get_liquidity_move_line_vals(pay_amount_new))
                                                liquidity_aml_dict.update({'invoice_idd':inv.id})
                                                print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                                banco_id = aml_obj.create(liquidity_aml_dict)
                                                print "esto es banco_id ", banco_id



                                            #Write counterpart lines
                                            #if not self.currency_id != self.company_id.currency_id:
                                                #amount_currency = 0
                                            #credit = self.amount
                                            #liquidity_aml_dict = self._get_shared_move_line_vals(credit, debit, -amount_currency, move.id, False)
                                            #print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                            #liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))
                                            #print "esto es liquidity_aml_dict ", liquidity_aml_dict
                                            #banco_id = aml_obj.create(liquidity_aml_dict)

                        move.ajuste_cambiario(self.id)
                        move.post()
                        return move


################### FUNCIONES ORIGINALES NO MODIFICADAS

    def _get_move_vals(self, journal=None):
        """ Return dict to create the payment move
        """
        journal = journal or self.journal_id
        if not journal.sequence_id:
            raise UserError(_('Configuration Error !'), _('The journal %s does not have a sequence, please specify one.') % journal.name)
        if not journal.sequence_id.active:
            raise UserError(_('Configuration Error !'), _('The sequence of journal %s is deactivated.') % journal.name)
        name = self.move_name or journal.with_context(ir_sequence_date=self.payment_date).sequence_id.next_by_id()
        lista ={
            'name': name,
            'date': self.payment_date,
            'ref': self.communication or '',
            'company_id': self.company_id.id,
            'journal_id': journal.id,
        }
        print "esto es lista ", lista
        return {
            'name': name,
            'date': self.payment_date,
            'ref': self.communication or '',
            'company_id': self.company_id.id,
            'journal_id': journal.id,
        }
