# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd.
#     (<http://acespritech.com>).
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': "VDE Polizas",
    'version': '10.1',
    'category': 'General',
    'description': """Solves journal entries issue """,
    'author': 'VDE Suite',
    'website': 'http://www.vde-suite.com',
    "depends": ['account_accountant',
    #'global_discount'
    ],
    "init_xml": [],
    "update_xml": [
        'views/poliza.xml',
        #'views/res_company_view.xml',
        #'views/invoice_data.xml',
        #'models/wizard/wizard_import_invoices_view.xml',
        ],
    "demo_xml": [],
    "active": False,
    "installable": True,
}
